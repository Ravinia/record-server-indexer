package servertester.controllers;

import java.util.*;

import client.communication.ClientCommunicator;
import servertester.views.*;
import shared.communication.*;

public class Controller implements IController {

	private IView _view;
	
	public Controller() {
		return;
	}
	
	public IView getView() {
		return _view;
	}
	
	public void setView(IView value) {
		_view = value;
	}
	
	// IController methods
	//
	
	@Override
	public void initialize() {
		getView().setHost("localhost");
		getView().setPort("8881");
		operationSelected();
	}

	@Override
	public void operationSelected() {
		ArrayList<String> paramNames = new ArrayList<String>();
		paramNames.add("User");
		paramNames.add("Password");
		
		switch (getView().getOperation()) {
		case VALIDATE_USER:
			break;
		case GET_PROJECTS:
			break;
		case GET_SAMPLE_IMAGE:
			paramNames.add("Project");
			break;
		case DOWNLOAD_BATCH:
			paramNames.add("Project");
			break;
		case GET_FIELDS:
			paramNames.add("Project");
			break;
		case SUBMIT_BATCH:
			paramNames.add("Batch");
			paramNames.add("Record Values");
			break;
		case SEARCH:
			paramNames.add("Fields");
			paramNames.add("Search Values");
			break;
		default:
			assert false;
			break;
		}
		
		getView().setRequest("");
		getView().setResponse("");
		getView().setParameterNames(paramNames.toArray(new String[paramNames.size()]));
	}

	@Override
	public void executeOperation() {
		switch (getView().getOperation()) {
		case VALIDATE_USER:
			validateUser();
			break;
		case GET_PROJECTS:
			getProjects();
			break;
		case GET_SAMPLE_IMAGE:
			getSampleImage();
			break;
		case DOWNLOAD_BATCH:
			downloadBatch();
			break;
		case GET_FIELDS:
			getFields();
			break;
		case SUBMIT_BATCH:
			submitBatch();
			break;
		case SEARCH:
			search();
			break;
		default:
			assert false;
			break;
		}
	}
	
	private void validateUser() {
		int port = 8881;
		if (getView().getPort().length() > 0)
		{
			port = Integer.parseInt(getView().getPort());
			System.out.println("port set: "+ port);
		}
		else
			System.out.println("port default: "+ port);
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		ValidateUser_Params params = new ValidateUser_Params(strings[0], strings[1]);
		ValidateUser_Return ret = cc.validateUser(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}
	
	private void getProjects() {
		int port = 8881;
		if (getView().getPort().length() > 0)
			port = Integer.parseInt(getView().getPort());
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		GetProjects_Params params = new GetProjects_Params(strings[0], strings[1]);
		GetProjects_Return ret = cc.getProjects(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}
	
	private void getSampleImage() {
		int port = 8881;
		if (getView().getPort().length() > 0)
			port = Integer.parseInt(getView().getPort());
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		int second = -1;
		if (strings[2].length() > 0)
			second = Integer.parseInt(strings[2]);
		GetSampleImage_Params params = new GetSampleImage_Params(strings[0], strings[1], second);
		GetSampleImage_Return ret = cc.getSampleImage(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}
	
	private void downloadBatch() {
		int port = 8881;
		if (getView().getPort().length() > 0)
			port = Integer.parseInt(getView().getPort());
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		int second = -1;
		if (strings[2].length() > 0)
			second = Integer.parseInt(strings[2]);
		DownloadBatch_Params params = new DownloadBatch_Params(strings[0], strings[1], second);
		DownloadBatch_Return ret = cc.downloadBatch(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}
	
	private void getFields() {
		int port = 8881;
		if (getView().getPort().length() > 0)
			port = Integer.parseInt(getView().getPort());
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		int second = -1;
		if (strings[2].length() > 0)
		{
			second = Integer.parseInt(strings[2]);
			if (second == -1)
				second = 0;
		}
		GetFields_Params params = new GetFields_Params(strings[0], strings[1], second);
		GetFields_Return ret = cc.getFields(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}
	
	private void submitBatch() {
		int port = 8881;
		if (getView().getPort().length() > 0)
			port = Integer.parseInt(getView().getPort());
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		
		ArrayList<ArrayList<String>> field_values = new ArrayList<ArrayList<String>>();
		
		Scanner scanner = new Scanner(strings[3]);
		Scanner scannerOuter = scanner.useDelimiter(";");
		ArrayList<String> outers = new ArrayList<String>();
		while(scannerOuter.hasNext())
			outers.add(scannerOuter.next());
		scanner.close();
		
		for (String focus : outers)
		{
			Scanner scanner2 = new Scanner(focus);
			Scanner scannerInner = scanner2.useDelimiter(",");
			ArrayList<String> inners = new ArrayList<String>();
			while(scannerInner.hasNext())
				inners.add(scannerInner.next());
			field_values.add(inners);
			scanner2.close();
		}
		
		if (field_values.size() > 0)
			System.out.println("submit batch: " + field_values.size() + ", " + field_values.get(0).size());
		
		int second = -1;
		if (strings[2].length() > 0)
			second = Integer.parseInt(strings[2]);
		SubmitBatch_Params params = new SubmitBatch_Params(strings[0], strings[1], second, field_values);
		SubmitBatch_Return ret = cc.submitBatch(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}
	
	private void search() {
		int port = 8881;
		if (getView().getPort().length() > 0)
			port = Integer.parseInt(getView().getPort());
		ClientCommunicator cc = new ClientCommunicator(getView().getHost(), port);
		String strings[] = getView().getParameterValues();
		
		ArrayList<Integer> fields = new ArrayList<Integer>();
		ArrayList<String> search_values = new ArrayList<String>();
		
		Scanner scanner = new Scanner(strings[2]);
		Scanner scannerFields = scanner.useDelimiter(",");
		while(scannerFields.hasNext())
			fields.add(Integer.parseInt(scannerFields.next()));
		scanner.close();
		
		Scanner scanner2 = new Scanner(strings[3]);
		Scanner scannerSearchValues = scanner2.useDelimiter(",");
		while(scannerSearchValues.hasNext())
			search_values.add(scannerSearchValues.next());
		scanner2.close();
				
		Search_Params params = new Search_Params(strings[0], strings[1], fields, search_values);
		Search_Return ret = cc.search(params);
		getView().setRequest(params.toString());
		if (ret != null)
			getView().setResponse(ret.toString());
		else
			getView().setResponse("FAILED\n");
	}

}

