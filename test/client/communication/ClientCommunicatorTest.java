package client.communication;

import static org.junit.Assert.*;
import importer.DataImporter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import server.dao.*;
import shared.communication.*;
import shared.communication.Search_Return.SearchResult;
import shared.model.*;

public class ClientCommunicatorTest {

	private static final Logger logger = Logger.getLogger("logger");
	ClientCommunicator cc;
	Database db;
	
	@Before
	public void setUp() throws Exception {

		db = new Database();
		logger.setLevel(Level.ALL); 
		db.wipeDatabase();
		cc = new ClientCommunicator("localhost", 8881);
		logger.info("Prepare database for test case");
		String args[] = {"demo/indexer_data/Records/Records.xml"};
		
		DataImporter.main(args);
	}

	@Test
	public void testValidateUser() {
		logger.info("test validate user");
		
		ValidateUser_Params params = new ValidateUser_Params("sheila", "parker");
		ValidateUser_Params params2 = new ValidateUser_Params("goofy", "parker");
		ValidateUser_Params params3 = new ValidateUser_Params("sheila", "garbage");
		ValidateUser_Return ret = cc.validateUser(params);
		ValidateUser_Return ret2 = cc.validateUser(params2);
		ValidateUser_Return ret3 = cc.validateUser(params3);
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(false, ret3.getSucceeds());
		
		logger.info("finished test validate user");
	}
	
	@Test
	public void testGetProjects() {
		logger.info("test get projects");
		
		GetProjects_Params params = new GetProjects_Params("sheila", "parker");
		GetProjects_Params params2 = new GetProjects_Params("goofy", "parker");
		GetProjects_Params params3 = new GetProjects_Params("sheila", "garbage");
		GetProjects_Return ret = cc.getProjects(params);
		GetProjects_Return ret2 = cc.getProjects(params2);
		GetProjects_Return ret3 = cc.getProjects(params3);
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(false, ret3.getSucceeds());

		
		ArrayList<Project> projects = ret.getProjects();
		assertEquals(3, projects.size());
		
		assertEquals(1, projects.get(0).getId());
		assertEquals("1890 Census", projects.get(0).getTitle());
		
		assertEquals(2, projects.get(1).getId());
		assertEquals("1900 Census", projects.get(1).getTitle());
		
		assertEquals(3, projects.get(2).getId());
		assertEquals("Draft Records", projects.get(2).getTitle());
		
		logger.info("finished test get projects");
	}
	
	@Test
	public void testGetSampleImage() {
		logger.info("test get sample image");
		
		GetSampleImage_Params params = new GetSampleImage_Params("sheila", "parker", 2);
		GetSampleImage_Params params2 = new GetSampleImage_Params("goofy", "parker",3);
		GetSampleImage_Params params3 = new GetSampleImage_Params("sheila", "garbage", 2);
		GetSampleImage_Return ret = cc.getSampleImage(params);
		GetSampleImage_Return ret2 = cc.getSampleImage(params2);
		GetSampleImage_Return ret3 = cc.getSampleImage(params3);
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(cc.makeLocalUrlFromPath("images/1900_image0.png"), ret.getImage_url());;
		assertEquals(false, ret2.getSucceeds());
		assertEquals(false, ret3.getSucceeds());
		
//		public GetSampleImage_Return(boolean succeeds, URL image_url)
		
		logger.info("finished test get sample image");
	}
	
	@Test
	public void testDownloadBatch() {
		logger.info("test download batch");
		
		DownloadBatch_Params params = new DownloadBatch_Params("sheila", "parker", 2);
		DownloadBatch_Params params2 = new DownloadBatch_Params("test1", "test1", 2);
		DownloadBatch_Return ret = cc.downloadBatch(params);
		DownloadBatch_Return ret2 = cc.downloadBatch(params);
		DownloadBatch_Return ret3 = cc.downloadBatch(params2);
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(true, ret3.getSucceeds());
		assertEquals(cc.makeLocalUrlFromPath("images/1900_image0.png"), ret.getImage_url());
		assertEquals(cc.makeLocalUrlFromPath("images/1900_image1.png"), ret3.getImage_url());
		
		logger.info("finished test download ");
	}
	
	@Test
	public void testSubmitBatch() {
		logger.info("test submit batch");
		
//		public SubmitBatch_Params(String user, String password, int batch, ArrayList<ArrayList<String>> field_values)
		ArrayList<ArrayList<String>> field_values = new ArrayList<ArrayList<String>>();
		
		DownloadBatch_Params params0 = new DownloadBatch_Params("test1", "test1", 1);
		DownloadBatch_Return ret0 = cc.downloadBatch(params0);
		assertEquals(true, ret0.getSucceeds());
		
		ArrayList<String> one = new ArrayList<String>();
		one.add("great");one.add("scott");one.add("potato");one.add("13");
		ArrayList<String> two = new ArrayList<String>();
		two.add("goocha");two.add("gonga");two.add("greek");two.add("133");
		
		field_values.add(one);
		field_values.add(two);
		field_values.add(two);
		field_values.add(two);
		field_values.add(two);
		field_values.add(two);
		field_values.add(two);
		field_values.add(two);
		
		logger.info("field_values size: " + field_values.size());
		
		SubmitBatch_Params params = new SubmitBatch_Params("test1", "test1", 1, field_values);
		SubmitBatch_Return ret = cc.submitBatch(params);
		
		assertEquals(true, ret.getSucceeds());
		db.openDatabaseConnection();
		UserAccess cv = new UserAccess();
		try {
			logger.info("trying to get all users");
			ArrayList<User> ul = cv.getAll();
			logger.info("Size: "+ul.size());
			logger.info("Username: "+ul.get(0).getUserName());
			assertEquals(field_values.size(), ul.get(0).getIndexedRecords());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			db.closeDatabaseConnection(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		logger.info("finished test submit batch");
	}
	
	@Test
	public void testGetFields() {
		logger.info("test get fields");
		
		GetFields_Params params = new GetFields_Params("sheila", "parker", 2);
		GetFields_Params params2 = new GetFields_Params("great", "test1", 2);
		GetFields_Return ret = cc.getFields(params);
		GetFields_Return ret2 = cc.getFields(params2);
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		
		ArrayList<Field> fields = ret.getFields();
		ArrayList<Field> fields2 = new ArrayList<Field>();
		ArrayList<Field> fields3 = null;
		
		logger.info("test get fields");
		
		db.openDatabaseConnection();
		FieldAccess fieldAccess = new FieldAccess();
		try {
			fields3 = fieldAccess.getAll();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		for (Field field : fields3)
		{
			logger.info(field.getProject_id() + " ?= "+params.getProject());
			if (field.getProject_id() == params.getProject())
				fields2.add(field);
		}
		
		logger.info("1 size: "+fields.size());
		logger.info("2 size: "+fields2.size());
		assertEquals(fields2.size(), fields.size());
		
		for (int i=0; i<fields.size(); i++)
		{
			assertEquals(fields2.get(i).getId(), fields.get(i).getId());
			assertEquals(fields2.get(i).getProject_id(), fields.get(i).getProject_id());
			assertEquals(fields2.get(i).getTitle(), fields.get(i).getTitle());
		}
		
		try {
			db.closeDatabaseConnection(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		logger.info("finished test get fields");
	}
	
	@Test
	public void testSearch() {
		logger.info("test get fields");
		
//		public Search_Params(String user, String password, ArrayList<Integer> fields, ArrayList<String> search_values)
		ArrayList<Integer> fields = new ArrayList<Integer>();
		fields.add(10);fields.add(11);
		ArrayList<String> search_values = new ArrayList<String>();
		ArrayList<String> search_values2 = new ArrayList<String>();
		search_values.add("fox");search_values.add("leroy");search_values.add("20");
		
		ArrayList<Integer> fields3 = new ArrayList<Integer>();
		fields3.add(13);fields3.add(11);
		ArrayList<String> search_values3 = new ArrayList<String>();
		search_values3.add("alaska native");search_values3.add("american indian");search_values3.add("charlie");
		
		
		Search_Params params = new Search_Params("test1", "test1", fields, search_values);
		Search_Params params2 = new Search_Params("sheila", "parker", fields, search_values2);
		Search_Params params3 = new Search_Params("garfield", "Matrix", fields, search_values);
		Search_Params params4 = new Search_Params("sheila", "parker", fields3, search_values3);
		
		Search_Return ret = cc.search(params);
		Search_Return ret2 = cc.search(params2);
		Search_Return ret3 = cc.search(params3);
		Search_Return ret4 = cc.search(params4);
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(false, ret3.getSucceeds());
		assertEquals(true, ret4.getSucceeds());
		
		//-----------------------------------------------------------
		
		ArrayList<SearchResult> searchResults = ret.getSearchResults();
		assertEquals(2, searchResults.size());
		
		assertEquals(1, searchResults.get(0).getBatch_id());
		assertEquals(10, searchResults.get(0).getField_id());
		assertEquals(cc.makeLocalUrlFromPath("images/draft_image0.png"), searchResults.get(0).getImage_url());
		assertEquals(1, searchResults.get(0).getRecord_num());
		
		assertEquals(1, searchResults.get(1).getBatch_id());
		assertEquals(11, searchResults.get(1).getField_id());
		assertEquals(cc.makeLocalUrlFromPath("images/draft_image0.png"), searchResults.get(1).getImage_url());
		assertEquals(6, searchResults.get(1).getRecord_num());
		
		//-----------------------------------------------------------------
		
		searchResults = ret4.getSearchResults();
		assertEquals(45, searchResults.size());
		
		int count = 0;
		for (SearchResult sr : searchResults)
		{
			if (sr.getField_id() == 11)
			{
				count++;
				if (count == 1)
					assertEquals(sr.getImage_url(), cc.makeLocalUrlFromPath("images/draft_image3.png"));
				if (count == 2)
					assertEquals(sr.getImage_url(), cc.makeLocalUrlFromPath("images/draft_image8.png"));
				if (count == 3)
					assertEquals(sr.getImage_url(), cc.makeLocalUrlFromPath("images/draft_image11.png"));
			}
		}
		assertEquals(3,count);
		
		logger.info("finished test search");
	}
	
	@Test
	public void testDownloadFile() {
		logger.info("test download file");
		
		byte[] bytes2 = cc.downloadFile("knowndata/1890_first_names.txt");
		
		Path path = Paths.get("database/knowndata/1890_first_names.txt");
		
		byte[] bytes;
		try {
			bytes = Files.readAllBytes(path);
			logger.info("FIRST BYTE ARRAY: " + bytes2.length);
			logger.info("SECOND BYTE ARRAY: " + bytes.length);
			assertEquals(true, Arrays.equals(bytes, bytes2));
			assertEquals(null, cc.downloadFile("blah"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		logger.info("finished download file");
	}
}
