package client.gui.left;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import shared.model.Field;
import client.communication.ClientCommunicator;
import client.gui.BatchState;
import client.gui.left.spell.SpellCorrector;

@SuppressWarnings("serial")
public class TableEntryPanel extends JPanel implements BatchState.BatchStateListener, ActionListener, SuggestionsDialog.Context{

	private JTable table;
	private TableModel tableModel;
	private ListSelectionModel listSelectionModel;
	private BatchState batchState;
	
	private JPopupMenu popup;
	private JMenuItem menuItem;
	private PopupListener popupListener;
	
	private Point clickedPoint;
	
	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public TableEntryPanel()
	{
		table = new JTable();
		add(table);
		add(Box.createHorizontalGlue());
		
		popup = new JPopupMenu();
		menuItem = new JMenuItem("See suggestions");
		menuItem.addActionListener(this);
		popup.add(menuItem);
	}
	
	public void fillTable(final BatchState batchState, ClientCommunicator cc)
	{
		tableModel = new TableModel(batchState);
		this.batchState = batchState;
		
		table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setCellSelectionEnabled(true);
		table.getTableHeader().setReorderingAllowed(false);
		table.setGridColor(Color.black); // black
		table.setShowGrid(true);
		table.setSelectionForeground(Color.black);
		popupListener = new PopupListener();
		table.addMouseListener(popupListener);
		
		TableColumnModel columnModel = table.getColumnModel();
		ArrayList<Field> fields = batchState.getDbr().getFields();
		for (int i = 0; i<fields.size()+1; i++)
		{
			TableColumn column = columnModel.getColumn(i);
			column.setPreferredWidth(100);
//			column.setCellRenderer(new ColorCellRenderer());
			column.setCellRenderer(new StatusColumnCellRenderer());
			
			if (i>0)
			{
				int k = i-1;
				String pathString = fields.get(k).getKnownData();
				System.out.println("pathString:" + pathString);
				if (!pathString.equals("null"))
				{
					int index = pathString.indexOf("knowndata");
					pathString = pathString.substring(index);
					byte[] bytes = cc.downloadFile(pathString);
					batchState.getKnownDataStrings().add(new String(bytes));
					System.out.println("new known data: "+pathString);
				}
				else
				{
					batchState.getKnownDataStrings().add("");
					System.out.println("new known data: null");
				}
			}
			table.requestFocus();
		}	
		
		// Spelling Corrector objects
		for (int i=0; i<batchState.getDbr().getNum_fields(); i++)
		{
			SpellCorrector sci = new SpellCorrector();
			try {
					sci.useString(batchState.getKnownDataStrings().get(i));
					System.out.println("new sci: "+batchState.getKnownDataStrings().get(i));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			batchState.getScis().add(sci);
		}
		System.out.println("scis size: "+batchState.getScis().size());
		
		listSelectionModel = table.getSelectionModel();
        listSelectionModel.addListSelectionListener(new SharedListSelectionHandler());
        table.setSelectionModel(listSelectionModel);
        columnModel.getSelectionModel().addListSelectionListener(new SharedListSelectionHandler());
		
		JPanel rootPanel = new JPanel(new BorderLayout());
		
//		table.changeSelection(0, 1, false, false);
		rootPanel.add(table.getTableHeader(), BorderLayout.NORTH);
		rootPanel.add(table, BorderLayout.CENTER);
		
        this.add(rootPanel);
	}
	
	public void wipe()
	{
		this.removeAll();
		repaint();
	}
	
	class SharedListSelectionHandler implements ListSelectionListener {
	    public void valueChanged(ListSelectionEvent e) {
	    	if (batchState.isEnabled() == false)
	    		return;
	    	
            int row = table.getSelectedRow();
            int col = table.getSelectedColumn();
            
//            System.out.println("setting selected: "+row+","+col);
            batchState.setSelectedCell(row, col);
		}
	}
	
	class PopupListener extends MouseAdapter {
		private int row;
		private int col;
		
		public PopupListener()
		{
			row = 0;
			col = 0;
		}
		
		public int getRow() {
			return row;
		}

		public void setRow(int row) {
			this.row = row;
		}

		public int getCol() {
			return col;
		}

		public void setCol(int col) {
			this.col = col;
		}
		
	    public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	Point p = e.getPoint();
	        	clickedPoint = p;
	        	int row = table.rowAtPoint(p);
	        	int col = table.columnAtPoint(p);
	        	
	        	System.out.println("maybeShowPopup.setRow: "+row);
	        	System.out.println("maybeShowPopup.setCol: "+col);
				
	        	if (batchState.getStatus(row, col) == BatchState.DENIED)
	        		popup.show(e.getComponent(),e.getX(), e.getY());
	        }
	    }
	}
	
	@Override
	public void valueChanged(int row, int col, String newValue) {
		if (batchState.isEnabled() == false)
    		return;
		if (col == 0)
			return;
		
		System.out.println("value changed");
		table.setValueAt(newValue, row, col);
	}

	@Override
	public void selectedCellChanged(int row, int col) {
		if (batchState.isEnabled() == false)
    		return;
		
//		System.out.println("changing table selection: " + row+","+col);
		table.changeSelection(row, col, false, false);
		table.requestFocus();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == menuItem)
		{
			int row = table.rowAtPoint(clickedPoint);
        	int col = table.columnAtPoint(clickedPoint);
			
			Set<String> suggestions = batchState.getSuggestions(row, col);
			System.out.println(suggestions);
			
//			System.out.println("suggestion dialog row: "+row);
//        	System.out.println("suggestion dialog col: "+col);
			
			new SuggestionsDialog(suggestions, this, row, col);
		}
	}

	@Override
	public void useSuggestion(String suggestion, int row, int col) {
		batchState.setValue(row, col, suggestion, this);
		table.repaint();
	}
}

