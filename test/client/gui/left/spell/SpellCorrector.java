package client.gui.left.spell;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class SpellCorrector{

	Map<String, Integer> words;
	Set<String> possibilities;
	Set<String> suggestions;
	
	public SpellCorrector()
	{
		words = new TreeMap<String, Integer>();
		possibilities = new TreeSet<String>();
		suggestions = new TreeSet<String>();
	}
	
	public Map<String, Integer> getWords()
	{
		return words;
	}
	
	public void useString(String str) throws IOException {
		Scanner dictionary = new Scanner(str);
		dictionary.useDelimiter(",");
		
		words = new TreeMap<String, Integer>();
		
		while(dictionary.hasNext())
		{
			String item = dictionary.next().toLowerCase();
			
			int frequency = 0;
			Set<String> values = words.keySet();
			for (String value : values)
			{
				if (value.toLowerCase().equals(item))
					frequency++;
			}
			if (item != null)
				words.put(item,frequency);
//			System.out.println("added: "+ frequency+ " " +item );
		}
		
		dictionary.close();
		
//		System.out.println("Initialized words: "+words.size());
	}
	
	public void calculate(String inputWord, boolean addPossibilities)
	{
		// Deletion distance
		for (int i=0; i<inputWord.length(); i++)
		{
			String variation = inputWord.substring(0,i) + inputWord.substring(i+1,inputWord.length());
			//System.out.print(variation + " ");
			if (addPossibilities)
				possibilities.add(variation);
			boolean contains = words.containsKey(variation);
			if (contains)
			{
				suggestions.add(variation);
			}
		}
		
		// Transposition Distance
		for (int i=1; i<inputWord.length(); i++)
		{
			String variation = inputWord.substring(0,i-1) + inputWord.charAt(i) + inputWord.charAt(i-1) +
					inputWord.substring(i+1,inputWord.length());
			//System.out.print(variation + " ");
			if (addPossibilities)
				possibilities.add(variation);
			boolean contains = words.containsKey(variation);
			if (contains)
			{
				suggestions.add(variation);
			}
		}
		
		// Alteration Distance
		for (int i=0; i<inputWord.length(); i++)
		{
			for (int j=0; j<26; j++)
			{
				char myChar = 'a';
				myChar += j;
				if (myChar != inputWord.charAt(i))
				{	
					String variation = inputWord.substring(0,i) + myChar + inputWord.substring(i+1,inputWord.length());
					//System.out.println(variation);
					if (addPossibilities)
						possibilities.add(variation);
					boolean contains = words.containsKey(variation);
					if (contains)
					{
						suggestions.add(variation);
					}
				}
			}
		}
		
		// Insertion Distance
		for (int i=0; i<inputWord.length()+1; i++)
		{
			for (int j=0; j<26; j++)
			{
				char myChar = 'a';
				myChar += j;
				String variation = inputWord.substring(0,i) + myChar + inputWord.substring(i,inputWord.length());
				//System.out.println(variation);
				if (addPossibilities)
					possibilities.add(variation);
				boolean contains = words.containsKey(variation);
				if (contains)
				{
					suggestions.add(variation);
				}
			}
		}
	}

	public Set<String> suggestSimilarWords(String inputWord) {
		if (words.containsValue(inputWord) != false)
			return null;
		
		suggestions = new TreeSet<String>();
		possibilities = new TreeSet<String>();

		calculate(inputWord, true);
		
		for (String possibility : possibilities)
		{
			calculate(possibility, false);
		}
		
		return suggestions;
	}	
}
