package client.gui.left;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import client.gui.BatchState;

@SuppressWarnings("serial")
public class StatusColumnCellRenderer extends DefaultTableCellRenderer {
	  @Override
	  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {

	    //Cells are by default rendered as a JLabel.
	    JLabel l = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

	    //Get the status for the current row.
	    TableModel tableModel = (TableModel) table.getModel();
	    if (tableModel.getBatchState().getStatus(row, col) == BatchState.APPROVED) {
	      l.setBackground(Color.WHITE);
	    } else {
	      l.setBackground(Color.RED);
	    }
	    
	    if (isSelected)
	    	l.setBackground(Color.BLUE);
	    
	    l.setText((String)value);

	  //Return the JLabel which renders the cell.
	  return l;

	}
}