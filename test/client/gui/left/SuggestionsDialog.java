package client.gui.left;

import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.gui.left.FormEntryPanel.SharedListSelectionHandler;

@SuppressWarnings("serial")
public class SuggestionsDialog extends JDialog implements ActionListener{

	private JButton cancelButton;
	private JButton useSuggestionButton;
	private Context context;
	private JList<String> sugList;
	private JScrollPane suggestionsScrollPanel;
	
	public interface Context {
		
		public void useSuggestion(String suggestion, int row, int col);
	}
	
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public SuggestionsDialog(Set<String> suggestions, Context c, final int row, final int col)
    {
    	super();
    	
    	context = c;
    	
		setTitle("Suggestions");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
		setAlwaysOnTop(true);
		setModal(true);
		setLocationRelativeTo(null);
		
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
        
        sugList = new JList(suggestions.toArray());
        sugList.getSelectionModel().addListSelectionListener(new SharedListSelectionHandler());
        sugList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        suggestionsScrollPanel = new JScrollPane(sugList);
        suggestionsScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        suggestionsScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        suggestionsScrollPanel.setPreferredSize(new Dimension(64, 128));
        rootPanel.add(suggestionsScrollPanel);
        
        Panel lowerPanel = new Panel();
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("cancel");
        lowerPanel.setLayout(new BoxLayout(lowerPanel, BoxLayout.LINE_AXIS));
//        lowerPanel.add(Box.createHorizontalStrut(128));
        lowerPanel.add(cancelButton);
        useSuggestionButton = new JButton("Use Suggestion");
        useSuggestionButton.addActionListener(new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				context.useSuggestion((String)sugList.getSelectedValue(), row, col);
			}
        });
        useSuggestionButton.setActionCommand("use suggestion");
        useSuggestionButton.setEnabled(false);
        lowerPanel.add(useSuggestionButton);
        rootPanel.add(lowerPanel);
        
        rootPanel.add(Box.createVerticalGlue());
        
        add(rootPanel);
        
        pack();
        
        setVisible(true);
        
        setLocationRelativeTo(null);
//	    	setLocation(this.getLocation().x-this.getSize().width/2,this.getLocation().y-this.getSize().height/2);
    }
    
    class SharedListSelectionHandler implements ListSelectionListener {
	    public void valueChanged(ListSelectionEvent e) {
	    	useSuggestionButton.setEnabled(true);
	    }
	}
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("cancel"))
        {
        	dispose();
        }
    }

    
}
