package client.gui.left;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Set;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.gui.BatchState;
import client.gui.left.TableEntryPanel.PopupListener;


@SuppressWarnings("serial")
public class FormEntryPanel extends JPanel implements BatchState.BatchStateListener, ActionListener, SuggestionsDialog.Context{

	private DefaultListModel<String> defaultListModel;
	private JList<String> records;
	private JPanel entryPanel;
	private ArrayList<JTextField> fields;
	private JScrollPane recordScrollPanel;
	private JScrollPane entryScrollPanel;
	
	private JPopupMenu popup;
	private JMenuItem menuItem;
	
	private int rightClickedRow = 0;
	private int rightClickedCol = 0;
	
	private BatchState batchState;
	
	public FormEntryPanel() {
		
		defaultListModel = new DefaultListModel<String>();
		records = new JList<String>(defaultListModel);
		records.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		entryPanel = new JPanel();
		fields = new ArrayList<JTextField>();
		
		popup = new JPopupMenu();
		menuItem = new JMenuItem("See suggestions");
		menuItem.addActionListener(this);
		popup.add(menuItem);
		
		records.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if (!arg0.getValueIsAdjusting()) {
                	
                }
            }
        });
       
        recordScrollPanel = new JScrollPane(records);
        recordScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        recordScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        entryScrollPanel = new JScrollPane(entryPanel);
        entryScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        entryScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(recordScrollPanel);
        this.add(entryScrollPanel);
	}
	
	public void fillPanel(final BatchState batchState)
	{
		fields = new ArrayList<JTextField>();
		
		int numberOfRecords = batchState.getDbr().getNum_records();
		int numberOfColumns = batchState.getDbr().getNum_fields();
		entryPanel = new JPanel();
		entryPanel.setLayout(new BoxLayout(entryPanel, BoxLayout.PAGE_AXIS));
		
		this.batchState = batchState;
		
		for (int i=0; i<numberOfRecords; i++)
		{
			defaultListModel.add(i, (i+1)+"");
		}
		records.setSelectedIndex(0);
		records.getSelectionModel().addListSelectionListener(new SharedListSelectionHandler());
		
		for (int j=0; j<numberOfColumns; j++)
		{
			JPanel rowPanel = new JPanel();
			rowPanel.setLayout(new BoxLayout(rowPanel, BoxLayout.LINE_AXIS));
			rowPanel.add(new JLabel(batchState.getDbr().getFields().get(j).getTitle()));
			final JTextField rowTextField = new JTextField();
			rowTextField.addFocusListener(new TextFieldListener());
			rowTextField.addActionListener(new ActionListener() {
			    public void actionPerformed(ActionEvent e) {
			    	
			    	JTextField source = (JTextField)e.getSource();
			        String text = source.getText();
			        batchState.setValue(batchState.getSelectedCell().getRecord(), batchState.getSelectedCell().getField(), text, this);
			    }
			});
			rowTextField.addMouseListener(new PopupListener(j));
			rowPanel.add(rowTextField);
			rowPanel.addFocusListener(new TextFieldListener());
			
			fields.add(rowTextField);
			entryPanel.add(rowPanel);
		}
		
		entryScrollPanel = new JScrollPane(entryPanel);
        entryScrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        entryScrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        this.removeAll();
        this.add(recordScrollPanel);
        this.add(entryScrollPanel);
	}
	
	class PopupListener extends MouseAdapter {
		
		int field;
		
		public PopupListener(int field)
		{
			this.field = field;
		}
		
		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }
		
		private void maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	int row = records.getSelectedIndex();
	        	int col = field;
	        	
	        	rightClickedRow = row;
	        	rightClickedCol = col;
	        	
	        	System.out.println("maybeShowPopup.setRow: "+row);
	        	System.out.println("maybeShowPopup.setCol: "+col);
				
	        	if (batchState.getStatus(row, col+1) == BatchState.DENIED)
	        	{
	        		System.out.println("misspelled");
	        		popup.show(e.getComponent(),e.getX(), e.getY());
	        	}
	        }
	    }
	}
	
	public void focusField()
	{
		// apply correct focus
		entryScrollPanel.requestFocus();
		entryPanel.requestFocus();
		int col = batchState.getSelectedCell().getField()-1;
		if (col == -1)
			col = 0;
		fields.get(col).requestFocus();
	}
	
	public void wipe()
	{
		defaultListModel.removeAllElements();
		entryPanel.removeAll();
		repaint();
	}
	
	class TextFieldListener implements FocusListener {

		public void focusGained(FocusEvent e) {
			
			int row = records.getSelectedIndex();
			int col = 0;
			
			for (int k=0; k<batchState.getDbr().getNum_fields(); k++)
			{
				if (fields.get(k).isFocusOwner())
					col = k;
			}
			
//			System.out.println("formEntry textField selecting: " + row + "," + col);
			batchState.setSelectedCell(row, col+1);
		}

		public void focusLost(FocusEvent e) {
//			if (batchState.getSelectedCell().getRecord() != records.getSelectedIndex())
			System.out.println(e.getOppositeComponent() + " ?= " + records);
			if (e.getOppositeComponent().equals(records))
				return;
			
			int field = batchState.getSelectedCell().getField()-1;
	        String text = fields.get(field).getText();
	        batchState.setValue(batchState.getSelectedCell().getRecord(), batchState.getSelectedCell().getField(), text, this);
		}
	}
	
	class SharedListSelectionHandler implements ListSelectionListener {
	    public void valueChanged(ListSelectionEvent e) {
	    	if (batchState.isEnabled() == false)
	    		return;
	    	
	    	int row = records.getSelectedIndex();
	    	int col = batchState.getSelectedCell().getField();
//	    	System.out.println("formEntry jlist selecting: " + row + "," + col);
	        batchState.setSelectedCell(row, col);
	    }
	}
	
	public void valueChanged(int row, int col, String newValue) {
		if (batchState.isEnabled() == false)
    		return;
		
		fields.get(col-1).setText(newValue);
		if (batchState.getStatus(row, col) == BatchState.DENIED)
			fields.get(col-1).setBackground(Color.RED);
		else
			fields.get(col-1).setBackground(Color.WHITE);
			
	}

	public void selectedCellChanged(int row, int col) {
		if (batchState.isEnabled() == false || fields.size() == 0 || row < 0)
    		return;
		
		if (col == 0)
			col = 1;
		
		if (fields.get(col-1).isFocusOwner())
			return;
		
		// load correct values
//		System.out.println("row = "+row);
//		System.out.println("col = "+col);
		for (int i=0; i<fields.size(); i++)
		{
//			System.out.println("i = "+i);
			fields.get(i).setText(batchState.getValue(records.getSelectedIndex(), i+1));
			if (batchState.getStatus(row, i+1) == BatchState.DENIED)
				fields.get(i).setBackground(Color.RED);
			else
				fields.get(i).setBackground(Color.WHITE);
		}
		
		records.setSelectedIndex(row);
		entryScrollPanel.requestFocus();
		entryPanel.requestFocus();
		fields.get(col-1).requestFocus();
	}
	
	@Override
	public void useSuggestion(String suggestion, int row, int col) {
		batchState.setValue(row, col, suggestion, this);
		repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == menuItem)
		{
			Set<String> suggestions = batchState.getSuggestions(rightClickedRow, rightClickedCol+1);
			System.out.println(suggestions);
			
			new SuggestionsDialog(suggestions, this, rightClickedRow, rightClickedCol+1);
		}
	}
}
