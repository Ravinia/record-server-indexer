package client.gui.left;

import javax.swing.table.AbstractTableModel;

import client.gui.BatchState;

@SuppressWarnings("serial")
public class TableModel extends AbstractTableModel {
	
	static private BatchState batchState;
	
	public TableModel(BatchState batchState) {
		TableModel.batchState = batchState;
	}
	
	public BatchState getBatchState()
	{
		return batchState;
	}
	
	@Override
	public int getColumnCount() {
		return batchState.getDbr().getNum_fields()+1;
	}

	@Override
	public String getColumnName(int column) {

		String result = null;
		if (column == 0)
			result = "Record Number";
		if (column >= 1 && column < getColumnCount())
		{
			result = batchState.getDbr().getFields().get(column-1).getTitle();
		}

		return result;
	}

	@Override
	public int getRowCount() {
		if (batchState.getDbr() == null)
		{
			System.out.println("null batchState.getDbr()");
			return 0;
		}
		return batchState.getDbr().getNum_records();
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		if (column > 0)
			return true;
		return false;
	}

	@Override
	public Object getValueAt(int row, int column) {
		if (batchState.isEnabled() == false)
    		return null;

		Object result = null;

		if (row >= 0 && row < getRowCount() && column >= 0 && column < getColumnCount()) {

			result = batchState.getValue(row,column);

		} else {
			throw new IndexOutOfBoundsException();
		}

		return result;
	}

	@Override
	public void setValueAt(Object value, int row, int column) {
		if (batchState.isEnabled() == false)
    		return;

		batchState.setValue(row, column, (String)value, new TableEntryPanel());
		
		this.fireTableCellUpdated(row, column);
	}
}
