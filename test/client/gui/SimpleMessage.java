package client.gui;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class SimpleMessage extends JDialog implements ActionListener
{    
	private JButton okayButton;

    public SimpleMessage(String title, String[] messages)
    {
    	super();
    	
    	okayButton = new JButton();
		okayButton.setText("Okay");
		okayButton.addActionListener(this);
		okayButton.setActionCommand("okay");
		
		setTitle(title);
		setLocationRelativeTo(null);
        
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
        
		setResizable(false);
		setModal(true);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setAlwaysOnTop(true);
		setBounds(getX()+getWidth()/3, getY()+getHeight()/3, getWidth()/2, getHeight()/2);
		
		for (String focus : messages)
			rootPanel.add(new JLabel(focus));
		rootPanel.add(okayButton);
        
        add(rootPanel);
        
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();
        
        if (cmd.equals("okay"))
        {
        	dispose();
        }
    }
}