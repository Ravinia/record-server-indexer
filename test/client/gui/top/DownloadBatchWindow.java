package client.gui.top;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.*;
import shared.model.*;

@SuppressWarnings("serial")
public class DownloadBatchWindow extends JDialog implements ActionListener
{    
	private JButton viewSampleButton;
	private JButton cancelButton;
	private JButton downloadButton;
	@SuppressWarnings("rawtypes")
	private JComboBox projects;
	
	Context context;
	
	public interface Context {
		
		public void downloadBatch(int project_id);
		public void viewSampleImage(String project_title, int project_id);
	}
	
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public DownloadBatchWindow(ArrayList<Project> projectArray, Context c)
    {
    	super();
    	
    	context = c;
		
		setTitle("Download Batch");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
		setAlwaysOnTop(true);
		setModal(true);
		setLocationRelativeTo(null);
        
        ArrayList<String> projectStrings = new ArrayList<String>();
        for (Project focus : projectArray)
        {
        	projectStrings.add(focus.getTitle());
        }
        
        JLabel projectLabel = new JLabel("Project:");
        
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
        
        Panel upperPanel = new Panel();
        upperPanel.setLayout(new BoxLayout(upperPanel, BoxLayout.LINE_AXIS));
        upperPanel.add(projectLabel);
        upperPanel.add(Box.createHorizontalGlue());
        projects = new JComboBox(projectStrings.toArray());
        upperPanel.add(projects);
        viewSampleButton = new JButton("View Sample");
        viewSampleButton.addActionListener(new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				context.viewSampleImage(projects.getSelectedItem().toString(), projects.getSelectedIndex()+1);
			}
        });
        viewSampleButton.setActionCommand("view sample");
        upperPanel.add(viewSampleButton);
        rootPanel.add(upperPanel);
        
        Panel lowerPanel = new Panel();
        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(this);
        cancelButton.setActionCommand("cancel");
        lowerPanel.setLayout(new BoxLayout(lowerPanel, BoxLayout.LINE_AXIS));
        lowerPanel.add(Box.createHorizontalStrut(128));
        lowerPanel.add(cancelButton);
        downloadButton = new JButton("Download");
        downloadButton.addActionListener(new ActionListener() {
        	
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
				context.downloadBatch(projects.getSelectedIndex()+1);
			}
        });
        downloadButton.setActionCommand("download");
        lowerPanel.add(downloadButton);
        rootPanel.add(lowerPanel);
        
        rootPanel.add(Box.createVerticalGlue());
        
        add(rootPanel);
        
        pack();
        
        setVisible(true);
        
        setLocationRelativeTo(null);
//    	setLocation(this.getLocation().x-this.getSize().width/2,this.getLocation().y-this.getSize().height/2);
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("cancel"))
        {
        	dispose();
        }
    }

    
}