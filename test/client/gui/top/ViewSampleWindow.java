package client.gui.top;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial")
public class ViewSampleWindow extends JDialog implements ActionListener
{    
	private JButton closeButton;
	private JLabel picLabel;
	private final int IMAGE_LENGTH = 512;
	private final int IMAGE_WIDTH = 512;
	

    public ViewSampleWindow(String projectTitle, byte[] bytes)
    {
    	setTitle("Sample image from "+projectTitle);
    	setLocationRelativeTo(null);
    	setLocation(this.getLocation().x-IMAGE_LENGTH/2,this.getLocation().y-IMAGE_WIDTH/2);
        setPreferredSize(new Dimension(512,512));
        setResizable(false);
		setAlwaysOnTop(true);
		setModal(true);
    	
    	closeButton = new JButton();
		closeButton.setText("Close");
		closeButton.addActionListener(this);
		closeButton.setActionCommand("close");
		
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
		
		BufferedImage myPicture = null;
		InputStream in = new ByteArrayInputStream(bytes);
		try {
				myPicture = ImageIO.read(in);
				myPicture = resize(myPicture,IMAGE_LENGTH,IMAGE_WIDTH);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		picLabel = new JLabel();
		ImageIcon imageIcon = new ImageIcon(myPicture);
		picLabel.setIcon(imageIcon);
//		picLabel.setPreferredSize(new Dimension(512,512));
		
		rootPanel.add(picLabel);
		rootPanel.add(closeButton);
        add(rootPanel);
        
        pack();
        setVisible(true);
//        setLocationRelativeTo(null);
    }
    
    public static BufferedImage resize(BufferedImage image, int width, int height) {
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = (Graphics2D) bi.createGraphics();
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(image, 0, 0, width, height, null);
        g2d.dispose();
        return bi;
    }
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();
        
        if (cmd.equals("close"))
        {
        	dispose();
        }
    }
}