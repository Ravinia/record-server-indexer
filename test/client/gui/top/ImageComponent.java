package client.gui.top;

import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.awt.event.*;

import javax.imageio.*;
import javax.swing.*;

import client.gui.BatchState;
import client.gui.BatchState.BatchStateListener;
import shared.communication.DownloadBatch_Return;
import shared.model.Field;

import java.util.*;
import java.io.*;

@SuppressWarnings("serial")
public class ImageComponent extends JComponent implements BatchStateListener, MouseWheelListener{

	private static Image NULL_IMAGE = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
	
	final private float MAX_ZOOM = 1.75f;
	final private float MIN_ZOOM = .25f;
	
	private boolean dragging;
	private int w_dragStartX;
	private int w_dragStartY;
	private int w_dragStartOriginX;
	private int w_dragStartOriginY;

	private DrawingImage mainImage;
	private DrawingRect mainRect;
	private BatchState batchState;
	
	
	private ArrayList<DrawingListener> listeners;
	
	public ImageComponent() {
		initDrag();

		listeners = new ArrayList<DrawingListener>();
		this.addMouseWheelListener(this);
		
		this.setBackground(Color.lightGray);
		this.setPreferredSize(new Dimension(700, 700));
		this.setMinimumSize(new Dimension(100, 100));
		this.setMaximumSize(new Dimension(1000, 1000));
		
		this.addMouseListener(mouseAdapter);
		this.addMouseMotionListener(mouseAdapter);
		this.addComponentListener(componentAdapter);
		
		mainImage = new DrawingImage(NULL_IMAGE, new Rectangle2D.Double(350, 50, NULL_IMAGE.getWidth(null), NULL_IMAGE.getHeight(null)));
		mainRect = null;
	}
	
	private void initDrag() {
		dragging = false;
		w_dragStartX = 0;
		w_dragStartY = 0;
		w_dragStartOriginX = 0;
		w_dragStartOriginY = 0;
	}
	
	public Image loadImage(String imageFile) {
		try {
			return ImageIO.read(new File(imageFile));
		}
		catch (IOException e) {
			return NULL_IMAGE;
		}
	}
	
	public void wipe()
	{
		mainImage = new DrawingImage(NULL_IMAGE, new Rectangle2D.Double(350, 50, NULL_IMAGE.getWidth(null), NULL_IMAGE.getHeight(null)));
		mainRect = null;
		repaint();
	}
	
	public Image loadImage(byte[] bytes) {
		try {
			InputStream in = new ByteArrayInputStream(bytes);
			BufferedImage bImageFromConvert = ImageIO.read(in);
			return bImageFromConvert;
		}
		catch (IOException e) {
			return NULL_IMAGE;
		}
	}
	
	public void setImage(Image image)
	{
		mainImage = new DrawingImage(image, new Rectangle2D.Double(350, 50, image.getWidth(null), image.getHeight(null)));
		this.repaint();
	}
	
	public void setScale(double newScale) {
		batchState.setScale(newScale);
		
		if (batchState.getScale() > MAX_ZOOM)
			batchState.setScale(MAX_ZOOM);
		else if (batchState.getScale() < MIN_ZOOM)
			batchState.setScale(MIN_ZOOM);;
		
		this.repaint();
	}
	
	public void incrementScale() {
		batchState.setScale(batchState.getScale()+.1);
		if (batchState.getScale() > MAX_ZOOM)
			batchState.setScale(MAX_ZOOM);
		this.repaint();
	}
	
	public void decrementScale() {
		batchState.setScale(batchState.getScale()-.1);
		if (batchState.getScale() < MIN_ZOOM)
			batchState.setScale(MIN_ZOOM);
		this.repaint();
	}
	
	public void invertImage() {
		if (batchState.isInverted())
			batchState.setInverted(false);
		else
			batchState.setInverted(true);
		RescaleOp op = new RescaleOp(-1.0f, 255f, null);
		BufferedImage negative = op.filter((BufferedImage) mainImage.getImage(), null);
		setImage(negative);
	}
	
	public void toggleHighlights()
	{
		if (batchState.isHighlighted())
			batchState.setHighlighted(false);
		else
			batchState.setHighlighted(true);
		this.repaint();
	}
	
	public void setHighlights(boolean set)
	{
		batchState.setHighlighted(set);
		this.repaint();
	}
	
	public void setOrigin(int w_newOriginX, int w_newOriginY) {
		batchState.setW_originX(w_newOriginX);
		batchState.setW_originX(w_newOriginY);
		this.repaint();
	}
	
	public void addDrawingListener(DrawingListener listener) {
		listeners.add(listener);
	}
	
	private void notifyOriginChanged(int w_newOriginX, int w_newOriginY) {
		for (DrawingListener listener : listeners) {
			listener.originChanged(w_newOriginX, w_newOriginY);
		}
	}
	
	public void fill(BatchState batchState)
	{
		this.batchState = batchState;
	}

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);

		Graphics2D g2 = (Graphics2D)g;
		drawBackground(g2);

//		System.out.println("Scale: " + scale);
		if (batchState != null)
		{
			g2.translate(getWidth()/2, getHeight()/2);
			g2.scale(batchState.getScale(), batchState.getScale());
			g2.translate(-batchState.getW_originX()-getWidth()/2, -batchState.getW_originY()-getHeight()/2);
		}

		mainImage.draw(g2);
		if (batchState != null)
		if (batchState.isHighlighted() && mainRect != null)
			mainRect.draw(g2);
	}
	
	
	
	private void drawBackground(Graphics2D g2) {
		g2.setColor(getBackground());
		g2.fillRect(0,  0, getWidth(), getHeight());
	}

	private MouseAdapter mouseAdapter = new MouseAdapter() {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			
			int d_X = e.getX();
			int d_Y = e.getY();
			
			AffineTransform transform = new AffineTransform();
			transform.translate(getWidth()/2, getHeight()/2);
			transform.scale(batchState.getScale(), batchState.getScale());
			transform.translate(-batchState.getW_originX()-getWidth()/2, -batchState.getW_originY()-getHeight()/2);
			
			Point2D d_Pt = new Point2D.Double(d_X, d_Y);
			Point2D w_Pt = new Point2D.Double();
			try
			{
				transform.inverseTransform(d_Pt, w_Pt);
			}
			catch (NoninvertibleTransformException ex) {
				return;
			}
			int w_X = (int)w_Pt.getX();
			int w_Y = (int)w_Pt.getY();
			
			boolean hitShape = false;
			
			Graphics2D g2 = (Graphics2D)getGraphics();
			if (mainImage.contains(g2, w_X, w_Y)) {
				hitShape = true;
			}
			
			if (hitShape) {
				System.out.println("hit shape: ("+w_X+","+w_Y+")");
				
				DownloadBatch_Return dbr = batchState.getDbr();
				double first_y_coord = mainImage.getBounds(g2).getY() + dbr.getFirst_y_coord();
				double record_height = dbr.getRecord_height();
				ArrayList<Field> fields = dbr.getFields();
				int num_records = dbr.getNum_records();
				
				for (int i=0; i<fields.size(); i++)
				{
					double xcoord = mainImage.getBounds(g2).getX() + fields.get(i).getXcoord();
					double width = fields.get(i).getWidth();
					boolean bingo = false;
					if (w_X > xcoord && w_X < xcoord+width)
						for (int j=0; j<num_records; j++)
						{
							double ycoord = first_y_coord + record_height*j;
							if ( w_Y > ycoord && w_Y < ycoord+record_height)
							{
//								System.out.println("bingo");
								batchState.setSelectedCell(j, i+1);
								bingo = true;
								break;
							}
						}
					if (bingo) break;
				}
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			
			System.out.println("Image component mouse pressed");
			
			int d_X = e.getX();
			int d_Y = e.getY();
			
			AffineTransform transform = new AffineTransform();
			transform.scale(batchState.getScale(), batchState.getScale());
			transform.translate(-batchState.getW_originX(), -batchState.getW_originY());
			
			Point2D d_Pt = new Point2D.Double(d_X, d_Y);
			Point2D w_Pt = new Point2D.Double();
			try
			{
				transform.inverseTransform(d_Pt, w_Pt);
			}
			catch (NoninvertibleTransformException ex) {
				return;
			}
			int w_X = (int)w_Pt.getX();
			int w_Y = (int)w_Pt.getY();
			
			dragging = true;		
			w_dragStartX = w_X;
			w_dragStartY = w_Y;		
			w_dragStartOriginX = batchState.getW_originX();
			w_dragStartOriginY = batchState.getW_originY();
		}

		@Override
		public void mouseDragged(MouseEvent e) {		
			if (dragging) {
				int d_X = e.getX();
				int d_Y = e.getY();

				AffineTransform transform = new AffineTransform();
				transform.scale(batchState.getScale(), batchState.getScale());
				transform.translate(-w_dragStartOriginX,-w_dragStartOriginY);
				
				Point2D d_Pt = new Point2D.Double(d_X, d_Y);
				Point2D w_Pt = new Point2D.Double();
				try
				{
					transform.inverseTransform(d_Pt, w_Pt);
				}
				catch (NoninvertibleTransformException ex) {
					return;
				}
				int w_X = (int)w_Pt.getX();
				int w_Y = (int)w_Pt.getY();
				
				int w_deltaX = w_X - w_dragStartX;
				int w_deltaY = w_Y - w_dragStartY;
				
				batchState.setW_originX(w_dragStartOriginX - w_deltaX);
				batchState.setW_originY(w_dragStartOriginY - w_deltaY);
				
				notifyOriginChanged(batchState.getW_originX(), batchState.getW_originY());
				
				repaint();
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			initDrag();
		}
	};
	
	private ComponentAdapter componentAdapter = new ComponentAdapter() {

		@Override
		public void componentHidden(ComponentEvent e) {
			return;
		}

		@Override
		public void componentMoved(ComponentEvent e) {
			return;
		}

		@Override
		public void componentResized(ComponentEvent e) {
		}

		@Override
		public void componentShown(ComponentEvent e) {
			return;
		}	
	};

	
	/////////////////
	// Drawing Shape
	/////////////////
	
	
	interface DrawingShape {
		boolean contains(Graphics2D g2, double x, double y);
		void draw(Graphics2D g2);
		Rectangle2D getBounds(Graphics2D g2);
	}


	class DrawingRect implements DrawingShape {

		private Rectangle2D rect;
		private Color color;
		
		public DrawingRect(Rectangle2D rect, Color color) {
			this.rect = rect;
			this.color = color;
		}

		@Override
		public boolean contains(Graphics2D g2, double x, double y) {
			return rect.contains(x, y);
		}

		@Override
		public void draw(Graphics2D g2) {
			g2.setColor(color);
			g2.fill(rect);
		}
		
		@Override
		public Rectangle2D getBounds(Graphics2D g2) {
			return rect.getBounds2D();
		}
	}

	class DrawingImage implements DrawingShape {

		private Image image;
		private Rectangle2D rect;
		
		public DrawingImage(Image image, Rectangle2D rect) {
			this.image = image;
			this.rect = rect;
		}
		
		public Image getImage()
		{
			return image;
		}

		@Override
		public boolean contains(Graphics2D g2, double x, double y) {
			return rect.contains(x, y);
		}

		@Override
		public void draw(Graphics2D g2) {
			g2.drawImage(image, (int)rect.getMinX(), (int)rect.getMinY(), (int)rect.getMaxX(), (int)rect.getMaxY(),
							0, 0, image.getWidth(null), image.getHeight(null), null);
		}	
		
		@Override
		public Rectangle2D getBounds(Graphics2D g2) {
			return rect.getBounds2D();
		}
	}
	
	public interface DrawingListener {

		void originChanged(int w_newOriginX, int w_newOriginY);
	}

	@Override
	public void valueChanged(int row, int col, String newValue) {
	}

	@Override
	public void selectedCellChanged(int row, int col) {
		if (col == 0)
			col = 1;
		col--;
		
		Graphics2D g2 = (Graphics2D)getGraphics();
		DownloadBatch_Return dbr = batchState.getDbr();
		double first_y_coord = mainImage.getBounds(g2).getY() + dbr.getFirst_y_coord();
		double record_height = dbr.getRecord_height();
		ArrayList<Field> fields = dbr.getFields();
		
		double xcoord = mainImage.getBounds(g2).getX() + fields.get(col).getXcoord();
		double width = fields.get(col).getWidth();
		double ycoord = first_y_coord + record_height*row;
		mainRect = new DrawingRect(new Rectangle2D.Double(xcoord, ycoord, width, record_height), new Color(.5f,.5f,1.0f,0.25f));
		repaint();
	}
	
	public void mouseWheelMoved(MouseWheelEvent e) {
		int notches = e.getWheelRotation();
       if (notches < 0)
    	   decrementScale();
       else 
    	   incrementScale();
	}
}




