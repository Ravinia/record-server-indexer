package client.gui.top;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

@SuppressWarnings("serial")
public class OperationPanel extends JPanel  implements ActionListener{

	public interface Context {
		void zoomIn();
		void zoomOut();
		void invertImage();
		void toggleHighlights();
		void save();
		void submitBatch();
	}
	
	private JButton zoomInButton;
	private JButton zoomOutButton;
	private JButton invertImageButton;
	private JButton toggleHighlightsButton;
	private JButton saveButton;
	private JButton submitButton;
	
	private Context context;
	
	
	public OperationPanel(Context c) {
		
		super();
		
		context = c;
		
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
		setLocation(0,0);

		zoomInButton = new JButton("Zoom In");
		zoomInButton.addActionListener(this);
		zoomInButton.setActionCommand("zoomIn");
		
		zoomOutButton = new JButton("Zoom Out");
		zoomOutButton.addActionListener(this);
		zoomOutButton.setActionCommand("zoomOut");
		
		invertImageButton = new JButton("Invert Image");
		invertImageButton.addActionListener(this);
		invertImageButton.setActionCommand("invertImage");
		
		toggleHighlightsButton = new JButton("Toggle Highlights");
		toggleHighlightsButton.addActionListener(this);
		toggleHighlightsButton.setActionCommand("toggleHighlights");
		
		saveButton = new JButton("Save");
		saveButton.addActionListener(this);
		saveButton.setActionCommand("save");
		
		submitButton = new JButton("Submit");
		submitButton.addActionListener(this);
		submitButton.setActionCommand("submit");
        
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(zoomInButton);
//        this.add(Box.createHorizontalStrut(32));
        this.add(zoomOutButton);
//        this.add(Box.createHorizontalStrut(32));
        this.add(invertImageButton);
//        this.add(Box.createHorizontalStrut(32));
        this.add(toggleHighlightsButton);
//        this.add(Box.createHorizontalStrut(32));
        this.add(saveButton);
//        this.add(Box.createHorizontalStrut(32));
        this.add(submitButton);
//        this.add(Box.createHorizontalStrut(32));
        
        disableAll();
	}
	
	public void enableAll()
	{
		zoomInButton.setEnabled(true);
		zoomOutButton.setEnabled(true);
		invertImageButton.setEnabled(true);
		toggleHighlightsButton.setEnabled(true);
		saveButton.setEnabled(true);
		submitButton.setEnabled(true);
	}
	
	public void disableAll()
	{
		zoomInButton.setEnabled(false);
		zoomOutButton.setEnabled(false);
		invertImageButton.setEnabled(false);
		toggleHighlightsButton.setEnabled(false);
		saveButton.setEnabled(false);
		submitButton.setEnabled(false);
	}
    
    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("zoomIn"))
        	context.zoomIn();
        
        if(cmd.equals("zoomOut"))
        	context.zoomOut();
        	
        if(cmd.equals("invertImage"))
        	context.invertImage();
        
        if(cmd.equals("toggleHighlights"))
        	context.toggleHighlights();
        
        if(cmd.equals("save"))
        	context.save();
        
        if(cmd.equals("submit"))
        	context.submitBatch();
    }
}
