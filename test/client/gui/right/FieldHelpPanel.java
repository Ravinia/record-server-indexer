package client.gui.right;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import client.communication.ClientCommunicator;
import client.gui.BatchState;

@SuppressWarnings("serial")
public class FieldHelpPanel extends JPanel implements BatchState.BatchStateListener {
	
	private JEditorPane htmlViewer;
	private JLabel testLabel;
	private BatchState batchState;
	private ClientCommunicator cc;
	private String[] helpFiles;
	
	public FieldHelpPanel() {
		
	}
	
	public void fill(BatchState batchState, ClientCommunicator cc)
	{
		this.batchState = batchState;
		this.cc = cc;
		
		testLabel = new JLabel("(0,1)");

		htmlViewer = new JEditorPane();
		htmlViewer.setOpaque(true);
		htmlViewer.setBackground(Color.white);
		htmlViewer.setPreferredSize(new Dimension(500, 600));
		htmlViewer.setEditable(false);
		htmlViewer.setContentType("text/html");
		
		// Collect help files
		int num_fields = batchState.getDbr().getNum_fields();
		helpFiles = new String[num_fields];
		for (int i=0; i<num_fields; i++)
		{
			String pathString = batchState.getDbr().getFields().get(i).getHelpHtml();
			int index = pathString.indexOf("fieldhelp");
			pathString = pathString.substring(index);
			System.out.println(pathString);
			String htmlCode = new String(this.cc.downloadFile(pathString));
			helpFiles[i] = htmlCode;
		}
		
		JScrollPane htmlScrollPane = new JScrollPane(htmlViewer);
		htmlScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		htmlScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        
        this.setLayout(new BorderLayout());
        this.add(testLabel, BorderLayout.NORTH);
        this.add(htmlScrollPane, BorderLayout.CENTER);
	}
	
	public void loadPage(String url)
	{
		try {
			htmlViewer.setPage(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void wipe()
	{
		this.removeAll();
		repaint();
	}
	
	@Override
	public void valueChanged(int row, int col, String newValue) {
		if (batchState.isEnabled() == false)
    		return;
		
		testLabel.setText("("+row+","+col+","+newValue+")");
	}

	@Override
	public void selectedCellChanged(int row, int col) {
		if (batchState.isEnabled() == false)
    		return;
		
		testLabel.setText("("+row+","+col+")");
		
		if (col == 0)
			col = 1;
		if (batchState != null)
		{
			htmlViewer.setText(helpFiles[col-1]);
		}
	}
}
