package client.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.List;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import shared.communication.*;
import client.communication.ClientCommunicator;
import client.gui.BatchState.BatchStateListener;
import client.gui.left.FormEntryPanel;
import client.gui.left.TableEntryPanel;
import client.gui.left.spell.SpellCorrector;
import client.gui.right.FieldHelpPanel;
import client.gui.right.ImageNavigationComponent;
import client.gui.top.DownloadBatchWindow;
import client.gui.top.ImageComponent;
import client.gui.top.OperationPanel;
import client.gui.top.ViewSampleWindow;

@SuppressWarnings("serial")
public class IndexerWindow extends JFrame implements ActionListener, WindowListener
{
	private JMenuItem downloadBatchMenuItem;
    private JMenuItem logoutMenuItem;
    private JMenuItem exitMenuItem;
	
	private OperationPanel operationPanel;
	private ImageComponent imageComponent;
	private TableEntryPanel tableEntryPanel;
	private FormEntryPanel formEntryPanel;
	private FieldHelpPanel fieldHelpPanel;
	
	private ImageNavigationComponent imageNavigationComponent;
	
	private JTabbedPane fieldHelpAndImageNavigationTabbedPane;
	private JTabbedPane tableAndFormTabbedPane;
	
	private JSplitPane splitPane;
	private JSplitPane splitPaneOuter;
	
	private static ClientCommunicator cc;
	private String host;
	private int port;
	
	private static BatchState batchState;
	private DownloadBatch_Return currentBatchRet;
	private ValidateUser_Params savedParams;
	
	private IndexerWindow selfPointer = this;
	
	private boolean thereWasSavedFile = false;
	
    public IndexerWindow(ValidateUser_Params params, String host, int port)
    {	
        super("Indexer");
        
        this.host = host;
        this.port = port;
    	savedParams = params;
    	batchState = attemptToReviveBatchState();
    	
    	if (cc == null)
    		cc = new ClientCommunicator(host, port);
    	
    	// Positioning
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        addWindowListener(this);
        
        // Menu
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu menu = new JMenu("File");
        menu.setMnemonic('f');
        menuBar.add(menu);

        downloadBatchMenuItem = new JMenuItem("Download Batch", KeyEvent.VK_D);
        downloadBatchMenuItem.addActionListener(actionListener);
        menu.add(downloadBatchMenuItem);

        logoutMenuItem = new JMenuItem("Logout", KeyEvent.VK_R);
        logoutMenuItem.addActionListener(actionListener);
        menu.add(logoutMenuItem);
        
        exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_X);
        exitMenuItem.addActionListener(actionListener);
        menu.add(exitMenuItem);
        
        initializePanels();
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
            	operationPanelContext.save();
            }
        });
        
        JScrollPane tableScrollPane = new JScrollPane(tableEntryPanel);
        tableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        tableScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        tableAndFormTabbedPane.addTab("Table Entry", null, tableScrollPane, null);
        
        tableAndFormTabbedPane.addTab("Form Entry", null, formEntryPanel, null);
        
        tableAndFormTabbedPane.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				if (tableAndFormTabbedPane.getSelectedIndex() == 1)
				{
					formEntryPanel.focusField();
				}
			}
        });
        
        // Field help and image navigation tabs
        fieldHelpAndImageNavigationTabbedPane = new JTabbedPane();
        fieldHelpPanel = new FieldHelpPanel();
        fieldHelpAndImageNavigationTabbedPane.addTab("Field Help", null, fieldHelpPanel, null);
        imageNavigationComponent = new ImageNavigationComponent();
        fieldHelpAndImageNavigationTabbedPane.addTab("Image Navigation", null, imageNavigationComponent, null);

        //Create a split pane for the two tabbedPanes
        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tableAndFormTabbedPane, fieldHelpAndImageNavigationTabbedPane);
//        splitPane.setPreferredSize(new Dimension(1440, 300));
        splitPane.setResizeWeight(.33);
        
        //Create a split pane for the image and bottom split pane
        splitPaneOuter = new JSplitPane(JSplitPane.VERTICAL_SPLIT, imageComponent, splitPane);
//        splitPane.setPreferredSize(new Dimension(1440, 700));
        splitPaneOuter.setResizeWeight(.66);
        	
        // Add to the root and pack
        JPanel rootPanel = new JPanel(new BorderLayout());
        rootPanel.add(operationPanel, BorderLayout.NORTH);
        rootPanel.add(splitPaneOuter, BorderLayout.CENTER);
        add(rootPanel);
        
        
        
        // Restoration
        batchState.setListeners(new ArrayList<BatchStateListener>());
		batchState.setScis(new ArrayList<SpellCorrector>());
		batchState.addListener(fieldHelpPanel);
		batchState.addListener(tableEntryPanel);
		batchState.addListener(imageComponent);
		batchState.addListener(formEntryPanel);
		
		if (!thereWasSavedFile)
		{
			setLocationRelativeTo(null);
	        setLocation(0,0);
			splitPane.setDividerLocation(333);
			splitPaneOuter.setDividerLocation(512);
			setExtendedState(JFrame.MAXIMIZED_BOTH); 
		}
		else
		{
			this.setLocation(batchState.getIndexingWindowPosition());
			this.setPreferredSize(batchState.getIndexingWindowDimension());
			splitPane.setDividerLocation(batchState.getHorizontalSplitPosition());
			splitPaneOuter.setDividerLocation(batchState.getVerticalSplitPosition());
			System.out.println("location loaded: "+batchState.getIndexingWindowPosition());
			System.out.println("dimension loaded: "+batchState.getIndexingWindowDimension());
			System.out.println("splitPane loaded: "+batchState.getHorizontalSplitPosition());
			System.out.println("splitPaneOuter loaded: "+batchState.getVerticalSplitPosition());
		}
		
        if (batchState.isEnabled())
		{
			downloadBatchMenuItem.setEnabled(false);
			operationPanel.enableAll();
			
			tableEntryPanel.fillTable(batchState, cc);
			formEntryPanel.fillPanel(batchState);
			
			fieldHelpPanel.fill(batchState, cc);
			
			String url = batchState.getDbr().getImage_url();
			int urlIndex = url.indexOf("images");
			url = url.substring(urlIndex);
			byte[] bytes = cc.downloadFile(url);
			imageComponent.fill(batchState);
//			imageNavigationComponent.fill(batchState);
			
			
			if (batchState.isInverted())
			{			
				RescaleOp op = new RescaleOp(-1.0f, 255f, null);
				Image image = imageComponent.loadImage(bytes);
				BufferedImage negative = op.filter((BufferedImage) image, null);
				imageComponent.setImage(negative);
			}
			else
				imageComponent.setImage(imageComponent.loadImage(bytes));
			
//			imageNavigationComponent.setImage(imageComponent.loadImage(bytes));
			
			batchState.setSelectedCell(batchState.getSelectedCell().record, batchState.getSelectedCell().field);
		}
        
        pack();
        setVisible(true);
        
        tableEntryPanel.getTable().requestFocus();
    }
    
    public BatchState attemptToReviveBatchState()
    {
    	BatchState returnBatch = new BatchState();
    	
    	String UI = savedParams.getName()+savedParams.getPassword().hashCode();
    	File file = new File(UI+".xml");
    	if (!file.exists())
    	{
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else
    	{
    		thereWasSavedFile = true;
	    	XStream xStream = new XStream(new DomDriver());
	    	InputStream inFile = null;
			try {
				inFile = new BufferedInputStream(new FileInputStream(UI+".xml"));
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			BatchState revivedBatch = (BatchState)xStream.fromXML(inFile);
			if (revivedBatch != null)
				returnBatch = revivedBatch;
			try {
				if (inFile != null)
					inFile.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
}
		
		return returnBatch;
    }
    
    public void initializePanels()
    {
        operationPanel = new OperationPanel(operationPanelContext);
        imageComponent = new ImageComponent();
        tableAndFormTabbedPane = new JTabbedPane();
        tableEntryPanel = new TableEntryPanel();
        formEntryPanel = new FormEntryPanel();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("whatever"))
        {
        }
    }
	
	private OperationPanel.Context operationPanelContext = new OperationPanel.Context() {
		
		@Override
		public void zoomIn() {
			imageComponent.incrementScale();
			tableEntryPanel.getTable().requestFocus();
		}

		@Override
		public void zoomOut() {
			imageComponent.decrementScale();
			tableEntryPanel.getTable().requestFocus();
		}

		@Override
		public void invertImage() {
			imageComponent.invertImage();
			tableEntryPanel.getTable().requestFocus();
		}

		@Override
		public void toggleHighlights() {
			imageComponent.toggleHighlights();
			tableEntryPanel.getTable().requestFocus();
		}

		@Override
		public void save() {
			batchState.setIndexingWindowPosition(selfPointer.getLocation());
			batchState.setIndexingWindowDimension(selfPointer.getSize());
			batchState.setHorizontalSplitPosition(splitPane.getDividerLocation());
			batchState.setVerticalSplitPosition(splitPaneOuter.getDividerLocation());
			
			System.out.println("location saved: "+selfPointer.getLocation());
			System.out.println("dimension saved: "+selfPointer.getSize());
			System.out.println("splitPane saved: "+splitPane.getDividerLocation());
			System.out.println("splitPaneOuter saved: "+splitPaneOuter.getDividerLocation());
			
			
			XStream xStream = new XStream(new DomDriver());
			
			OutputStream outFile = null;
			try {
				String UI = savedParams.getName()+savedParams.getPassword().hashCode();
				outFile = new BufferedOutputStream(new FileOutputStream(UI+".xml"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			xStream.toXML(batchState, outFile);
			try {
				if (outFile != null)
					outFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			tableEntryPanel.getTable().requestFocus();
		}

		@Override
		public void submitBatch() {
			// Gather values and submit the batch
//        	public SubmitBatch_Params(String user, String password, int batch, ArrayList<ArrayList<String>> field_values)
        	String user = savedParams.getName();
        	String password = savedParams.getPassword();
        	int batch = batchState.getDbr().getBatch_id();
        	
        	ArrayList<ArrayList<String>> field_values = new ArrayList<ArrayList<String>>();
        	for (int i=0; i<batchState.getDbr().getNum_records(); i++)
        	{
        		ArrayList<String> al = new ArrayList<String>();
        		for (int j=0; j<batchState.getDbr().getNum_fields(); j++)
        		{
        			al.add(batchState.getValue(i, j));
        		}
        		field_values.add(al);
        	}
        			
        	SubmitBatch_Params sbparams = new SubmitBatch_Params(user, password, batch, field_values);
        	cc.submitBatch(sbparams);
        	
        	// Reset interface
        	batchState.wipe();
        	operationPanel.disableAll();
        	downloadBatchMenuItem.setEnabled(true);
        	imageComponent.wipe();
//        	imageNavigationComponent.wipe();
        	fieldHelpPanel.wipe();
        	tableEntryPanel.wipe();
        	formEntryPanel.wipe();
		}
	};
	
	private DownloadBatchWindow.Context downloadBatchWindowListener = new DownloadBatchWindow.Context() {

		@Override
		public void downloadBatch(int project_id) {
			DownloadBatch_Params params = new DownloadBatch_Params(savedParams.getName(),savedParams.getPassword(),project_id);
			currentBatchRet = cc.downloadBatch(params);
			downloadBatchMenuItem.setEnabled(false);
			operationPanel.enableAll();
			
			batchState.updateBatchState(currentBatchRet);
			
			batchState.addListener(fieldHelpPanel);
			batchState.addListener(tableEntryPanel);
			batchState.addListener(imageComponent);
			batchState.addListener(formEntryPanel);
			
			tableEntryPanel.fillTable(batchState, cc);
			formEntryPanel.fillPanel(batchState);
			
			fieldHelpPanel.fill(batchState, cc);
			
			String url = batchState.getDbr().getImage_url();
			int urlIndex = url.indexOf("images");
			url = url.substring(urlIndex);
			byte[] bytes = cc.downloadFile(url);
			imageComponent.fill(batchState);
			imageComponent.setImage(imageComponent.loadImage(bytes));
//			imageNavigationComponent.setImage(imageComponent.loadImage(bytes));
			
			batchState.setEnabled(true);
			batchState.setSelectedCell(0, 1);
			tableEntryPanel.getTable().requestFocus();
		}

		@Override
		public void viewSampleImage(String project_title, int project_id) {
			System.out.println(savedParams.getName());
			System.out.println(savedParams.getPassword());
			System.out.println(project_id);
			GetSampleImage_Params params = new GetSampleImage_Params(savedParams.getName(),savedParams.getPassword(),project_id);
			GetSampleImage_Return ret = cc.getSampleImage(params);
			
			String uri = ret.getImage_url();
			int index = uri.indexOf("images");
			uri = uri.substring(index);
			
			byte[] bytes = cc.downloadFile(uri);
			new ViewSampleWindow(project_title, bytes);
		}
	};
	
	private ActionListener actionListener = new ActionListener() {
    	
	    public void actionPerformed(ActionEvent e) {
	    	
	        if (e.getSource() == downloadBatchMenuItem)
	        {
//	        	System.out.println(savedParams.getName());
//	        	System.out.println(savedParams.getPassword());
	        	GetProjects_Params params = new GetProjects_Params(savedParams.getName(),savedParams.getPassword());
	        	GetProjects_Return ret = cc.getProjects(params);
	        	if (ret == null)
	        		System.out.println("null ret for get projects");
	        	else
	        		new DownloadBatchWindow(ret.getProjects(), downloadBatchWindowListener);
	        }
	        else if (e.getSource() == logoutMenuItem)
	        {
	        	operationPanelContext.save();
	        	dispose();
	        	String[] args = new String[]{host, port+""};
	        	new StartupWindow(args);
	        }
	        else if (e.getSource() == exitMenuItem)
	        {
	        	operationPanelContext.save();
	            System.exit(0);
	        }
	    }
    };

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		operationPanelContext.save();
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}