package client.gui;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import shared.communication.ValidateUser_Params;
import shared.communication.ValidateUser_Return;
import client.communication.ClientCommunicator;

@SuppressWarnings("serial")
public class StartupWindow extends JFrame implements ActionListener
{    
    private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton loginButton;
	private JButton exitButton;
	private String host;
	private int port;
	private static ClientCommunicator cc;

    public StartupWindow(String[] args)
    {
    	super("login");
    	
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setResizable(false);
    	
    	String usage = "Usage: client -Dhost=<HOST NAME> -Dport=<PORT NUMBER>";
    	if (args.length > 2)
    	{
    		System.out.println(usage);
    		dispose();
    		return;
    	}
    	
    	host = "localhost";
    	port = 8881;
    	
    	if (args.length == 2)
    	{
			host = args[0];
			port = Integer.parseInt(args[1]);
    	}
    	else if (args.length == 1)
    	{
    		if (args[0].contains(".") || args[0].equals("localhost"))
	    		host = args[0];
    		else
    			port = Integer.parseInt(args[0]);
    	}
    	
//        setLocationRelativeTo(null);
//        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel usernameLabel = new JLabel("Username:");
		JLabel passwordLabel = new JLabel("Password:");
        
		usernameField = new JTextField(16);
		usernameField.setMaximumSize(new Dimension(128,32));
		usernameField.addActionListener(this);
        
        passwordField = new JPasswordField(16);
        passwordField.setMaximumSize(new Dimension(128,32));
        passwordField.addActionListener(this);
        
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
        
        Panel usernamePanel = new Panel();
        usernamePanel.setLayout(new BoxLayout(usernamePanel, BoxLayout.LINE_AXIS));
        usernamePanel.add(usernameLabel);
        usernamePanel.add(usernameField);
        usernamePanel.add(Box.createHorizontalGlue());
        rootPanel.add(usernamePanel);
        
        Panel passwordPanel = new Panel();
        passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.LINE_AXIS));
        passwordPanel.add(passwordLabel);
        passwordPanel.add(passwordField);
        passwordPanel.add(Box.createHorizontalGlue());
        rootPanel. add(passwordPanel);
        
        Panel buttonPanel = new Panel();
        loginButton = new JButton("Login");
        loginButton.addActionListener(this);
        loginButton.setActionCommand("login");
        exitButton = new JButton("Exit");
        exitButton.addActionListener(this);
        exitButton.setActionCommand("exit");
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createHorizontalStrut(128));
        buttonPanel.add(loginButton);
        buttonPanel.add(Box.createHorizontalGlue());
        rootPanel.add(buttonPanel);
        buttonPanel.add(exitButton);
        
        rootPanel.add(Box.createVerticalGlue());
        
        add(rootPanel);
        
        pack();
        setVisible(true);
        setLocationRelativeTo(null);
    }
    
    public void setUsernameField(String text) {
		usernameField.setText(text);
	}
	
	public String getUsernameField() {
		return usernameField.getText();
	}
	
	public void setPasswordField(String text) {
		passwordField.setText(text);
	}
	
	public String getPasswordField() {
		return passwordField.getText();
	}

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("login"))
        {
        	if (cc == null)
        		cc = new ClientCommunicator(host, port);
    		ValidateUser_Params params = new ValidateUser_Params(getUsernameField(), getPasswordField());
    		ValidateUser_Return ret = cc.validateUser(params);
    		
    		boolean succeeds = true;
    		String errorString = "Uninitialized message";
    		if (ret == null)
    		{
    			succeeds = false;
    			errorString = "Server error";
    		}
    		else if (ret.getSucceeds() == false)
    		{
    			succeeds = false;
    			errorString = "Invalid credentials";
    		}
    		
    		if (succeeds)
    		{
    			String[] messages =
    				{
    					"Welcome, "+ret.getUser_first_name()+" "+ret.getUser_last_name()+".",
    					"You have indexed "+ret.getNum_records()+" records."
    				};
    			SimpleMessage er = new SimpleMessage("Welcome", messages);
    			er.setLocation(this.getLocation().x, this.getLocation().y);
    			
    			dispose();
                new IndexerWindow(params, host, port);
    		}
    		else
    		{
    			String[] messages = {errorString};
    			SimpleMessage er = new SimpleMessage("Error", messages);
    			er.setLocation(this.getLocation().x, this.getLocation().y);
    		}
        }
        
        if (cmd.equals("exit"))
        {
        	dispose();
        	System.exit(0);
        }
    }

    public static void main(final String[] args)
    {
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run()
            {
                new StartupWindow(args).setVisible(true);
            }

        });
    }
}