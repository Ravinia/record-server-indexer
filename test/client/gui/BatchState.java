package client.gui;

import java.awt.Dimension;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import client.gui.left.spell.SpellCorrector;
import shared.communication.DownloadBatch_Return;

public class BatchState {
	
	public interface BatchStateListener {
		
		public void valueChanged(int row, int col, String newValue);
		
		public void selectedCellChanged(int row, int col);
	}
	
	private String[][] values;
	private Cell selectedCell;
	private transient List<BatchStateListener> listeners;
	private DownloadBatch_Return dbr;
	private boolean enabled;
	
	public List<BatchStateListener> getListeners() {
		return listeners;
	}

	public void setListeners(List<BatchStateListener> listeners) {
		this.listeners = listeners;
	}


	// Image Component
	private int w_originX;
	private int w_originY;
	private double scale;
	private boolean highlighted;
	private boolean inverted;
	
	// Table
	public static final boolean APPROVED = true;
	public static final boolean DENIED = false;
	private ArrayList<String> knownDataStrings;
	private transient ArrayList<SpellCorrector> scis;
	private Set<String> suggestions;
	
	// Indexing window
	private Point indexingWindowPosition;
	private Dimension indexingWindowDimension;
	private int horizontalSplitPosition;
	private int verticalSplitPosition;
	
	


	public BatchState()
	{
		values = null;
		selectedCell = null;
		listeners = null;
		dbr = null;
		enabled = false;
		
		w_originX = 0;
		w_originY = 0;
		scale = 1.0;
		highlighted = true;
		inverted = false;
		
		knownDataStrings = new ArrayList<String>();
		scis = new ArrayList<SpellCorrector>();
		
		indexingWindowPosition = new Point(0,0);
		indexingWindowDimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
		horizontalSplitPosition = 700;
		verticalSplitPosition = 512;
	}
	
	public void setScis(ArrayList<SpellCorrector> scis) {
		this.scis = scis;
	}

	// Table ---------------------------------------------------
	public ArrayList<String> getKnownDataStrings()
	{
		return knownDataStrings;
	}
	
	public ArrayList<SpellCorrector> getScis()
	{
		return scis;
	}
	
	public Set<String> getSuggestions(int row, int col)
	{
		getStatus(row, col);
		return suggestions;
	}
	
	public boolean getStatus(int row, int col)
	{
		if (col == 0)
			return APPROVED;
		
		String newValue = values[row][col];
		if (newValue.length() > 0)
		{
			if (scis.get(col-1).getWords().containsKey(newValue.toLowerCase()))
				return APPROVED;
			else
			{
				System.out.println("similar words for: "+newValue);
//				System.out.println("sci words: "+scis.get(col-1).getWords());
				suggestions = scis.get(col-1).suggestSimilarWords(newValue);
				System.out.println(suggestions);
//				System.out.println("adjusted color red: "+row+","+col);
				return DENIED;
			}
		}
		return APPROVED;
	}
	//---------------------------------------------------
	

	public void updateBatchState(DownloadBatch_Return dbr) {
		this.dbr = dbr;
		values = new String[dbr.getNum_records()][dbr.getNum_fields()+1];
		
		for (int i=0; i<values.length; i++)
		{
			for (int j=0; j<values[i].length; j++)
			{
				values[i][j] = "";
			}
			values[i][0] = (i+1)+"";
		}
		
		selectedCell = new Cell(0,1);
		listeners = new ArrayList<BatchStateListener>();
		enabled = true;
	}
	
	public void addListener(BatchStateListener l) {
		listeners.add(l);
	}
	
	public void setValue(int row, int col, String value, Object obj) {
		
		values[row][col] = value;
		
		for (BatchStateListener l : listeners) {
			if (!l.getClass().equals(obj.getClass()))
				l.valueChanged(row, col, value);
		}
	}
	
	public String getValue(int row, int col) {
//		System.out.println("row:"+row+" col:"+col);
//		System.out.println("rows:"+values.length+" cols:"+values[0].length);
		return values[row][col];
	}
	
	public void setSelectedCell(int row, int col) {
		
		selectedCell = new Cell(row, col);
		
		for (BatchStateListener l : listeners) {
			l.selectedCellChanged(row, col);
		}
	}
	
	public Cell getSelectedCell() {
		return selectedCell;
	}
	
	public void wipe()
	{
//		this.dbr = null;
//		values = new String[dbr.getNum_records()][dbr.getNum_fields()+1];
//		
//		for (int i=0; i<values.length; i++)
//		{
//			for (int j=0; j<values[i].length; j++)
//			{
//				values[i][j] = "";
//			}
//			values[i][0] = (i+1)+"";
//		}
//		
//		selectedCell = new Cell(0,1);
//		listeners = new ArrayList<BatchStateListener>();
		enabled = false;
	}
	
	public class Cell {
		int record;
		int field;
		
		public Cell(int row, int col)
		{
			record = row;
			field = col;
		}
		
		public int getRecord()
		{
			return record;
		}
		
		public int getField()
		{
			return field;
		}
	}
	
	public int getW_originX() {
		return w_originX;
	}

	public void setW_originX(int w_originX) {
		this.w_originX = w_originX;
	}

	public int getW_originY() {
		return w_originY;
	}

	public void setW_originY(int w_originY) {
		this.w_originY = w_originY;
	}

	public double getScale() {
		return scale;
	}

	public void setScale(double scale) {
		this.scale = scale;
	}

	public boolean isHighlighted() {
		return highlighted;
	}

	public void setHighlighted(boolean highlight) {
		this.highlighted = highlight;
	}

	public boolean isInverted() {
		return inverted;
	}

	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	}
	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public DownloadBatch_Return getDbr() {
		return dbr;
	}
	
	public void setDbr(DownloadBatch_Return dbr) {
		this.dbr = dbr;
	}
	
	public Point getIndexingWindowPosition() {
		return indexingWindowPosition;
	}


	public void setIndexingWindowPosition(Point indexingWindowPosition) {
		this.indexingWindowPosition = indexingWindowPosition;
	}


	public Dimension getIndexingWindowDimension() {
		return indexingWindowDimension;
	}


	public void setIndexingWindowDimension(Dimension indexingWindowDimension) {
		this.indexingWindowDimension = indexingWindowDimension;
	}


	public int getHorizontalSplitPosition() {
		return horizontalSplitPosition;
	}


	public void setHorizontalSplitPosition(int horizontalSplitPosition) {
		this.horizontalSplitPosition = horizontalSplitPosition;
	}


	public int getVerticalSplitPosition() {
		return verticalSplitPosition;
	}


	public void setVerticalSplitPosition(int verticalSplitPosition) {
		this.verticalSplitPosition = verticalSplitPosition;
	}
}
