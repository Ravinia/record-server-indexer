package main;

public class UnitTestDriver {

	public static void main(String[] args) {

		String[] testClasses = new String[] {
			// Client
			"client.communication.ClientCommunicatorTest",

			// Shared
			"shared.model.CellValueTest",
			"shared.model.FieldTest",
			"shared.model.ImageTest",
			"shared.model.ProjectTest",
			"shared.model.UserTest",

			// Server
			"server.dao.CellValueAccessTest",
			"server.dao.FieldAccessTest",
			"server.dao.ImageAccessTest",
			"server.dao.ProjectAccessTest",
			"server.dao.UserAccessTest",
			"server.facade.FacadeTest"
		};

		org.junit.runner.JUnitCore.main(testClasses);
	}
}
