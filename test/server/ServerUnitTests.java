package server;

public class ServerUnitTests {

	public static void main(String[] args) {
		
		String[] testClasses = new String[] {
				"server.dao.CellValueAccessTest",
				"server.dao.FieldAccessTest",
				"server.dao.ImageAccessTest",
				"server.dao.ProjectAccessTest",
				"server.dao.UserAccessTest",
				"server.facade.FacadeTest"
		};

		org.junit.runner.JUnitCore.main(testClasses);
	}
	
}

