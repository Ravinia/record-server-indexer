package server.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.*;

import server.dao.Database;
import server.dao.DatabaseException;
import server.dao.ProjectAccess;
import shared.model.Project;

public class ProjectAccessTest {
	
	private static final Logger logger = Logger.getLogger("logger");
	private Database db;
	private ProjectAccess projectAccess;
	
	@Before
	public void setUp() throws Exception {

		logger.setLevel(Level.OFF); 
		logger.info("Prepare database for test case");
		db = new Database();
		projectAccess = new ProjectAccess();
		db.wipeDatabase();
	}

	@Test
	public void testInsert() throws DatabaseException, SQLException {
		
		logger.info("Add two projects to the database");
		db.openDatabaseConnection();
		
//		public Project(int id, String title, int recordsPerImage, int firstYCoord, int recordHeight)
		
		Project project1 = new Project(2124, "greatness", 37, 89, 919);
		Project project2 = new Project(2444, "hallbart", 111110, 111, 49);
		
		try {
			projectAccess.insert(project1);
			projectAccess.insert(project2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		ArrayList<Project> list = projectAccess.getAll();
		assertEquals(list.get(0).getId(), project1.getId());
		assertEquals(list.get(0).getTitle(), project1.getTitle());
		assertEquals(list.get(0).getRecordsPerImage(), project1.getRecordsPerImage());
		assertEquals(list.get(0).getFirstYCoord(), project1.getFirstYCoord());
		assertEquals(list.get(0).getRecordHeight(), project1.getRecordHeight());

		assertEquals(list.get(1).getId(), project2.getId());
		assertEquals(list.get(1).getTitle(), project2.getTitle());
		assertEquals(list.get(1).getRecordsPerImage(), project2.getRecordsPerImage());
		assertEquals(list.get(1).getFirstYCoord(), project2.getFirstYCoord());
		assertEquals(list.get(1).getRecordHeight(), project2.getRecordHeight());
		
		db.closeDatabaseConnection(false);
	}
}
