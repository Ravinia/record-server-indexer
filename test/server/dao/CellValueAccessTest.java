package server.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.*;

import server.dao.CellValueAccess;
import server.dao.Database;
import server.dao.DatabaseException;
import shared.model.CellValue;


public class CellValueAccessTest {
	
	private static final Logger logger = Logger.getLogger("logger");
	private Database db;
	private CellValueAccess cellValueAccess;
	
	@Before
	public void setUp() throws Exception {

		logger.setLevel(Level.OFF); 
		logger.info("Prepare database for test case");
		db = new Database();
		cellValueAccess = new CellValueAccess();
		db.wipeDatabase();
	}

	@Test
	public void testInsert() throws DatabaseException, SQLException {
		
		logger.info("Add two cellValues to the database");
		db.openDatabaseConnection();
		
//		public CellValue(int image_id, int project_id, int field_id, String str, int recordNumber)
		
		CellValue cellValue1 = new CellValue(124, 4, 2, "boring", 1);
		CellValue cellValue2 = new CellValue(88, 1,5, "yah", 3);
		
		try {
			cellValueAccess.insert(cellValue1);
			cellValueAccess.insert(cellValue2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ArrayList<CellValue> list = cellValueAccess.getAll();
		assertEquals(list.get(0).getField_id(), cellValue1.getField_id());
		assertEquals(list.get(0).getImage_id(), cellValue1.getImage_id());
		assertEquals(list.get(0).getProject_id(), cellValue1.getProject_id());
		assertEquals(list.get(0).getRecordNumber(), cellValue1.getRecordNumber());
		assertEquals(list.get(0).getStr(), cellValue1.getStr());

		assertEquals(list.get(1).getField_id(), cellValue2.getField_id());
		assertEquals(list.get(1).getImage_id(), cellValue2.getImage_id());
		assertEquals(list.get(1).getProject_id(), cellValue2.getProject_id());
		assertEquals(list.get(1).getRecordNumber(), cellValue2.getRecordNumber());
		assertEquals(list.get(1).getStr(), cellValue2.getStr());
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testUpdate() throws DatabaseException, SQLException {
		
		logger.info("Add two cellValues to the database");
		db.openDatabaseConnection();
		
//		public CellValue(int image_id, int project_id, int field_id, String str, int recordNumber)
		
		CellValue cellValue1 = new CellValue(124, 4, 2, "boring", 1);
		CellValue cellValue2 = new CellValue(88, 1,5, "yah", 3);
		
		try {
			cellValueAccess.insert(cellValue1);
			cellValueAccess.insert(cellValue2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		CellValue cellValue3 = new CellValue(124, 4,2, "no", 1);
		CellValue cellValue4 = new CellValue(88, 1,5, "goocha", 3);
		try {
			cellValueAccess.update(cellValue3);
			cellValueAccess.update(cellValue4);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		ArrayList<CellValue> list = cellValueAccess.getAll();
		assertEquals(list.get(0).getField_id(), cellValue3.getField_id());
		assertEquals(list.get(0).getImage_id(), cellValue3.getImage_id());
		assertEquals(list.get(0).getProject_id(), cellValue3.getProject_id());
		assertEquals(list.get(0).getRecordNumber(), cellValue3.getRecordNumber());
		assertEquals(list.get(0).getStr(), cellValue3.getStr());

		assertEquals(list.get(1).getField_id(), cellValue4.getField_id());
		assertEquals(list.get(1).getImage_id(), cellValue4.getImage_id());
		assertEquals(list.get(1).getProject_id(), cellValue4.getProject_id());
		assertEquals(list.get(1).getRecordNumber(), cellValue4.getRecordNumber());
		assertEquals(list.get(1).getStr(), cellValue4.getStr());
		
		db.closeDatabaseConnection(false);
	}
}
