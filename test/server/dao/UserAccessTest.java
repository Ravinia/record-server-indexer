package server.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.*;

import server.dao.Database;
import server.dao.DatabaseException;
import server.dao.UserAccess;
import shared.model.User;

public class UserAccessTest {
	
	private static final Logger logger = Logger.getLogger("logger");
	private Database db;
	private UserAccess userAccess;
		
	@Before
	public void setUp() throws Exception {

		logger.setLevel(Level.OFF); 
		logger.info("Prepare database for test case");
		db = new Database();
		userAccess = new UserAccess();
		db.wipeDatabase();
	}
	
	@Test
	public void testInsert() throws DatabaseException, SQLException {
		
		logger.info("Add two users to the database");
		db.openDatabaseConnection();
		
//		public User(String userName, String password, String firstName, String lastName,
//		String email, int indexedRecords, String currentImage)
		
		User user1 = new User("joe123", "baseball", "joe", "farstly", "j.f@gmail.com", 2, "waste.png");
		User user2 = new User("abbi101", "hottie", "abbi", "charsley", "abbihotti@icloud.com", 200, "useful.png");
		
		try {
			userAccess.insert(user1);
			userAccess.insert(user2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ArrayList<User> list = userAccess.getAll();
		assertEquals(list.get(0).getUserName(), user1.getUserName());
		assertEquals(list.get(0).getPassword(), user1.getPassword());
		assertEquals(list.get(0).getFirstName(), user1.getFirstName());
		assertEquals(list.get(0).getLastName(), user1.getLastName());
		assertEquals(list.get(0).getEmail(), user1.getEmail());
		assertEquals(list.get(0).getIndexedRecords(), user1.getIndexedRecords());
		assertEquals(list.get(0).getCurrentImage(), user1.getCurrentImage());

		assertEquals(list.get(1).getUserName(), user2.getUserName());
		assertEquals(list.get(1).getPassword(), user2.getPassword());
		assertEquals(list.get(1).getFirstName(), user2.getFirstName());
		assertEquals(list.get(1).getLastName(), user2.getLastName());
		assertEquals(list.get(1).getEmail(), user2.getEmail());
		assertEquals(list.get(1).getIndexedRecords(), user2.getIndexedRecords());
		assertEquals(list.get(1).getCurrentImage(), user2.getCurrentImage());
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testUpdate() throws DatabaseException, SQLException {
		
		logger.info("Add two users to the database");
		db.openDatabaseConnection();
		
//		public User(int image_id, int project_id, int field_id, String str, int recordNumber)
		
		User user1 = new User("joe123", "baseball", "joe", "farstly", "j.f@gmail.com", 2, "waste.png");
		User user2 = new User("abbi101", "hottie", "abbi", "charsley", "abbihotti@icloud.com", 200, "useful.png");
		
		try {
			userAccess.insert(user1);
			userAccess.insert(user2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		User user3 = new User("joe123", "baseball", "aasdfadsf", "asdfasdfadf", "j.f@asdf.com", 21, "dfdf.png");
		User user4 = new User("abbi101", "hottie", "abbasdfi", "asdfadf", "abbidfhotti@idsafacloud.com", 444, "sdd.png");
		try {
			userAccess.update(user3);
			userAccess.update(user4);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		ArrayList<User> list = userAccess.getAll();
		assertEquals(list.get(0).getUserName(), user3.getUserName());
		assertEquals(list.get(0).getPassword(), user3.getPassword());
		assertEquals(list.get(0).getFirstName(), user3.getFirstName());
		assertEquals(list.get(0).getLastName(), user3.getLastName());
		assertEquals(list.get(0).getEmail(), user3.getEmail());
		assertEquals(list.get(0).getIndexedRecords(), user3.getIndexedRecords());
		assertEquals(list.get(0).getCurrentImage(), user3.getCurrentImage());

		assertEquals(list.get(1).getUserName(), user4.getUserName());
		assertEquals(list.get(1).getPassword(), user4.getPassword());
		assertEquals(list.get(1).getFirstName(), user4.getFirstName());
		assertEquals(list.get(1).getLastName(), user4.getLastName());
		assertEquals(list.get(1).getEmail(), user4.getEmail());
		assertEquals(list.get(1).getIndexedRecords(), user4.getIndexedRecords());
		assertEquals(list.get(1).getCurrentImage(), user4.getCurrentImage());
		
		db.closeDatabaseConnection(false);
	}
}
