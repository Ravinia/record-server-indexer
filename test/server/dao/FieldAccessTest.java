package server.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.*;

import server.dao.Database;
import server.dao.DatabaseException;
import server.dao.FieldAccess;
import shared.model.Field;

public class FieldAccessTest {
	
	private static final Logger logger = Logger.getLogger("logger");
	private Database db;
	private FieldAccess fieldAccess;
	@Before
	public void setUp() throws Exception {

		logger.setLevel(Level.OFF); 
		logger.info("Prepare database for test case");
		db = new Database();
		fieldAccess = new FieldAccess();
		db.wipeDatabase();
	}

	@Test
	public void testInsert() throws DatabaseException, SQLException {
		
		logger.info("Add two fields to the database");
		db.openDatabaseConnection();
		
//		public Field(int id, String title, int xcoord, int width, int fieldNumber, int project_id, String knownData, String helpHtml)
		
		Field field1 = new Field(1, "grassness", 15, 100, 1, 14, "not much", "help.com");
		Field field2 = new Field(2, "emptyness", 11, 48, 2, 1, "much", "not-help.gov");
		
		try {
			fieldAccess.insert(field1);
			fieldAccess.insert(field2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		ArrayList<Field> list = fieldAccess.getAll();
//		System.out.println(list.size());
		assertEquals(list.get(0).getId(), field1.getId());
		assertEquals(list.get(0).getTitle(), field1.getTitle());
		assertEquals(list.get(0).getXcoord(), field1.getXcoord());
		assertEquals(list.get(0).getWidth(), field1.getWidth());
		assertEquals(list.get(0).getFieldNumber(), field1.getFieldNumber());
		assertEquals(list.get(0).getProject_id(), field1.getProject_id());
		assertEquals(list.get(0).getKnownData(), field1.getKnownData());
		assertEquals(list.get(0).getHelpHtml(), field1.getHelpHtml());

		assertEquals(list.get(1).getId(), field2.getId());
		assertEquals(list.get(1).getTitle(), field2.getTitle());
		assertEquals(list.get(1).getXcoord(), field2.getXcoord());
		assertEquals(list.get(1).getWidth(), field2.getWidth());
		assertEquals(list.get(1).getFieldNumber(), field2.getFieldNumber());
		assertEquals(list.get(1).getProject_id(), field2.getProject_id());
		assertEquals(list.get(1).getHelpHtml(), field2.getHelpHtml());
		assertEquals(list.get(1).getKnownData(), field2.getKnownData());
		
		db.closeDatabaseConnection(false);
	}
}
