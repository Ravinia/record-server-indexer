package server.dao;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.*;

import server.dao.Database;
import server.dao.DatabaseException;
import server.dao.ImageAccess;
import shared.model.Image;


public class ImageAccessTest {
	
	private static final Logger logger = Logger.getLogger("logger");
	private Database db;
	private ImageAccess imageAccess;
	@Before
	public void setUp() throws Exception {

		logger.setLevel(Level.OFF); 
		logger.info("Prepare database for test case");
		db = new Database();
		imageAccess = new ImageAccess();
		db.wipeDatabase();
	}

	@Test
	public void testInsert() throws DatabaseException, SQLException {
		
		db.openDatabaseConnection();
		
//		public Image(int id, int project_id, String file, boolean isAvailable)
		
		Image image1 = new Image(24, 44, "great.png", true);
		Image image2 = new Image(212, 25, "not.jpeg", false);
		
		try {
			imageAccess.insert(image1);
			imageAccess.insert(image2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		ArrayList<Image> list = imageAccess.getAll();
		assertEquals(list.get(0).getId(), image1.getId());
		assertEquals(list.get(0).getProject_id(), image1.getProject_id());
		assertEquals(list.get(0).getFile(), image1.getFile());
		assertEquals(list.get(0).isAvailable(), image1.isAvailable());

		assertEquals(list.get(1).getId(), image2.getId());
		assertEquals(list.get(1).getProject_id(), image2.getProject_id());
		assertEquals(list.get(1).getFile(), image2.getFile());
		assertEquals(list.get(1).isAvailable(), image2.isAvailable());
		
		db.closeDatabaseConnection(false);
	}

	@Test
	public void testUpdate() throws DatabaseException, SQLException {
		
		db.openDatabaseConnection();
		
		Image image1 = new Image(24, 44, "great.png", true);
		Image image2 = new Image(212, 25, "not.jpeg", false);
		
		try {
			imageAccess.insert(image1);
			imageAccess.insert(image2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Image image3 = new Image(24, 44, "goomba.jpeg", false);
		Image image4 = new Image(212, 25, "lolly.jpeg", false);
		try {
			imageAccess.update(image3);
			imageAccess.update(image4);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ArrayList<Image> list = imageAccess.getAll();
		assertEquals(list.get(0).getId(), image3.getId());
		assertEquals(list.get(0).getProject_id(), image3.getProject_id());
		assertEquals(list.get(0).getFile(), image3.getFile());
		assertEquals(list.get(0).isAvailable(), image3.isAvailable());

		assertEquals(list.get(1).getId(), image4.getId());
		assertEquals(list.get(1).getProject_id(), image4.getProject_id());
		assertEquals(list.get(1).getFile(), image4.getFile());
		assertEquals(list.get(1).isAvailable(), image4.isAvailable());
		
		db.closeDatabaseConnection(false);
	}
}
