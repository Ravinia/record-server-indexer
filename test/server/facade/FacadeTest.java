package server.facade;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;

import server.dao.*;
import shared.communication.*;
import shared.model.*;

public class FacadeTest {
	
	private static final Logger logger = Logger.getLogger("logger");
	private UserAccess userAccess;
	private Database db;
	private Facade facade;
	private CellValueAccess cellValueAccess;
	private FieldAccess fieldAccess;
	private ImageAccess imageAccess;
	private ProjectAccess projectAccess;
	
	@Before
	public void setUp() throws Exception {

		logger.setLevel(Level.OFF); 
		Database.loadDatabaseDriver();
		
		db = new Database();
		db.wipeDatabase();
		
		facade = new Facade();
		userAccess = new UserAccess();
		cellValueAccess = new CellValueAccess();
		fieldAccess = new FieldAccess();
		imageAccess = new ImageAccess();
		projectAccess = new ProjectAccess();
		
		logger.info("Starting");
	}

	@Test
	public void testSearch() throws DatabaseException, SQLException, MalformedURLException {
		
		//search is tested in client communicator test
		
//		db.openDatabaseConnection();
//		
//		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
//		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24, "great.png");
//		
//		try {
//			userAccess.insert(bob);
//			userAccess.insert(amy);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		CellValue cellValue1 = new CellValue(1, 4, 1, "boring", 1);
//		CellValue cellValue2 = new CellValue(2, 1,3, "yah", 3);
//		CellValue cellValue3 = new CellValue(3, 34,2, "wow", 2);
//		CellValue cellValue4 = new CellValue(4, 34,2, "yelp", 2);
//		
//		try {
//			cellValueAccess.insert(cellValue1);
//			cellValueAccess.insert(cellValue2);
//			cellValueAccess.insert(cellValue3);
//			cellValueAccess.insert(cellValue4);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
////		public Field(int id, String title, int xcoord, int width, int fieldNumber, int project_id, String knownData, String helpHtml)
//		Field field1 = new Field(1,"fieldyah", 244, 135, 44, 4, "haha", "great");
//		Field field2 = new Field(2,"fieldyah", 244, 135, 444, 34, "haha", "great");
//		Field field3 = new Field(3,"fieldyah", 244, 135, 1, 2, "haha", "great");
//		
//		try {
//			fieldAccess.insert(field1);
//			fieldAccess.insert(field2);
//			fieldAccess.insert(field3);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		ImageAccess imageAccess = new ImageAccess();
//		Image img1 = new Image(1, 4, "http://students.cs.byu.edu/~cs240ta/winter2014/projects/1.pdf", true);
//		Image img2 = new Image(2, 25, "http://students.cs.byu.edu/~cs240ta/winter2014/projects/2.pdf", false);
//		Image img3 = new Image(3, 22224, "http://students.cs.byu.edu/~cs240ta/winter2014/projects/3.pdf", false);
//		Image img4 = new Image(4, 34, "http://students.cs.byu.edu/~cs240ta/winter2014/projects/4.pdf", false);
//		
//		try {
//			imageAccess.insert(img1);
//			imageAccess.insert(img2);
//			imageAccess.insert(img3);
//			imageAccess.insert(img4);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		//Search_Params(String user, String password, ArrayList<Integer> fields, ArrayList<String> search_values)
//		
//		ArrayList<Integer> fields = new ArrayList<Integer>();
//		fields.add(44);fields.add(444);
//		ArrayList<Integer> fields2 = new ArrayList<Integer>();
//		fields2.add(1);fields2.add(5);
//		ArrayList<String> search_values = new ArrayList<String>();
//		search_values.add("yelp");search_values.add("yah");search_values.add("boring");
//		Search_Params params = new Search_Params("Neo", "Matrix", fields, search_values);
//		Search_Params params2 = new Search_Params("garfield", "Matrix", fields, search_values);
//		Search_Params params3 = new Search_Params("Neo", "Matrix", fields2, search_values);
//		Search_Return ret;
//		Search_Return ret2;
//		Search_Return ret3;
//		try {
//			ret = facade.search(params);
//			ret2 = facade.search(params2);
//			ret3 = facade.search(params3);
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return;
//		}
//		assertEquals(true, ret.getSucceeds());
//		assertEquals(false, ret2.getSucceeds());
//		assertEquals(false, ret3.getSucceeds());
//		
//		//Search result
////		private int batch_id;
////		private URL image_url;
////		private int record_num;
////		private int field_id;
//		
//		ArrayList<Search_Return.SearchResult> results = ret.getSearchResults();
//		assertEquals(2, results.size());
//		
//		assertEquals(cellValue1.getField_id(), results.get(0).getField_id());
//		assertEquals(cellValue1.getRecordNumber(), results.get(0).getRecord_num());
//		assertEquals(cellValue1.getImage_id(), results.get(0).getBatch_id());
//		assertEquals(img1.getFile(), results.get(0).getImage_url());
//		
//		assertEquals(cellValue4.getField_id(), results.get(1).getField_id());
//		assertEquals(cellValue4.getRecordNumber(), results.get(1).getRecord_num());
//		assertEquals(cellValue4.getImage_id(), results.get(1).getBatch_id());
//		assertEquals(img4.getFile(), results.get(1).getImage_url());
//		
//		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testGetFields() throws DatabaseException, SQLException {
		
		db.openDatabaseConnection();
		
		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24, "great.png");
		
		try {
			userAccess.insert(bob);
			userAccess.insert(amy);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Field field1 = new Field(42, "grassness", 15, 100, 55, 12, "not much", "help.com");
		Field field2 = new Field(522, "emptyness", 11, 48, 10, 12, "much", "not-help.gov");
		Field field3 = new Field(15, "woogah", 15, 22, 1, 21, "fake knowledge", "yasuo.com");
		
		try {
			fieldAccess.insert(field1);
			fieldAccess.insert(field2);
			fieldAccess.insert(field3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		GetFields_Params params = new GetFields_Params("Neo", "Matrix", 12);
		GetFields_Params params2 = new GetFields_Params("Neo", "Matrix", 13);
		GetFields_Params params3 = new GetFields_Params("Neo", "asdf", 12);
		GetFields_Return ret;
		GetFields_Return ret2;
		GetFields_Return ret3;
		try {
			ret = facade.getFields(params);
			ret2 = facade.getFields(params2);
			ret3 = facade.getFields(params3);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		assertEquals(true, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(false, ret3.getSucceeds());
		
		ArrayList<Field> fields = ret.getFields();
		
		assertEquals(field1.getId(), fields.get(0).getId());
		assertEquals(field1.getTitle(), fields.get(0).getTitle());
		
		assertEquals(field2.getId(), fields.get(1).getId());
		assertEquals(field2.getTitle(), fields.get(1).getTitle());
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testGetSampleImage() throws SQLException, MalformedURLException {
		
		db.openDatabaseConnection();
		
		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24,
				"http://x.byu.edu:1234/known/first.txt");
		
		try {
			userAccess.insert(bob);
			userAccess.insert(amy);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Image img1 = new Image(24, 44, "images/greek.png", true);
		Image img2 = new Image(212, 25, "images/russian.png", false);
		Image img3 = new Image(133, 2, "images/google.png", true);
		
		try {
			imageAccess.insert(img1);
			imageAccess.insert(img2);
			imageAccess.insert(img3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		GetSampleImage_Params params = new GetSampleImage_Params("Neo", "Matrix", 2);
		GetSampleImage_Return ret;
		try {
			ret = facade.getSampleImage(params);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
		assertEquals(true, ret.getSucceeds());
		assertEquals(ret.getImage_url(), img3.getFile());
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testSubmitBatch() throws SQLException {
		
		db.openDatabaseConnection();
		
		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24, null);
		
		try {
			userAccess.insert(bob);
			userAccess.insert(amy);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Image img1 = new Image(24, 44, "http://x.byu.edu:1234/known/first.txt", true);
		Image img2 = new Image(212, 25, "http://x.byu.edu:1234/known/second.txt", true);
		
		try {
			imageAccess.insert(img1);
			imageAccess.insert(img2);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
//		public Project(int id, String title, int recordsPerImage, int firstYCoord, int recordHeight)
		Project proj1 = new Project(25, "great", 2, 22231, 214);
		
		try {
			projectAccess.insert(proj1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
//		public Field(int id, String title, int xcoord, int width, int fieldNumber, int project_id, String knownData, String helpHtml)
		Field field1 = new Field(1, "haha", 2, 124, 1, 25, null, null);
		Field field2 = new Field(2, "bleek", 2, 124, 2, 25, null, null);
		Field field3 = new Field(3, "greaaat", 2, 124, 3, 25, null, null);
		
		try {
			fieldAccess.insert(field1);
			fieldAccess.insert(field2);
			fieldAccess.insert(field3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		DownloadBatch_Params params0 = new DownloadBatch_Params("Girly", "Hot pockets", 25);
		try {
			DownloadBatch_Return ret0 = facade.downloadBatch(params0);
			assertEquals(true, ret0.getSucceeds());
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		ArrayList<ArrayList<String>> field_values = new ArrayList<ArrayList<String>>();
		field_values.add(new ArrayList<String>(Arrays.asList("uno", "dos", "tres")));
		field_values.add(new ArrayList<String>(Arrays.asList("quatro", "cinco", "sies")));
		//public SubmitBatch_Params(String user, String password, int batch, ArrayList<ArrayList<String>> field_values)
		SubmitBatch_Params params = new SubmitBatch_Params("Girly", "Hot pockets", 212, field_values);
		SubmitBatch_Params params2 = new SubmitBatch_Params("Wanka", "Hot pockets", 212, field_values);
		SubmitBatch_Params params3 = new SubmitBatch_Params("Girly", "Hot pockets", 2, field_values);
		SubmitBatch_Return ret;
		SubmitBatch_Return ret2;
		SubmitBatch_Return ret3;
		try {
			ret = facade.submitBatch(params);
			ret2 = facade.submitBatch(params2);
			ret3 = facade.submitBatch(params3);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
		assertEquals(false, ret3.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(true, ret.getSucceeds());
		ArrayList<User> list = userAccess.getAll();
		User mu = null;
		for (User focus : list)
		{
//			System.out.println(focus.getUserName() + " =? " + params.getUser());
//			System.out.println(focus.getPassword() + " =? " + params.getPassword());
			if (focus.getUserName().equals(params.getUser()) && focus.getPassword().equals(params.getPassword()))
			{
				mu = focus;
			}
		}
		assertEquals(null, mu.getCurrentImage());
		assertEquals(amy.getIndexedRecords()+field_values.size(), mu.getIndexedRecords());
		
//		public CellValue(int image_id, int project_id, int field_id, String str, int recordNumber)
		ArrayList<CellValue> cl = cellValueAccess.getAll();
		assertEquals(cl.get(0).getField_id(), 1);
		assertEquals(cl.get(0).getImage_id(), params.getBatch());
		assertEquals(cl.get(0).getProject_id(), img2.getProject_id());
		assertEquals(cl.get(0).getRecordNumber(), 1);
		assertEquals(cl.get(0).getStr(), field_values.get(0).get(0));
		
		assertEquals(cl.get(1).getField_id(), 2);
		assertEquals(cl.get(1).getImage_id(), params.getBatch());
		assertEquals(cl.get(1).getProject_id(), img2.getProject_id());
		assertEquals(cl.get(1).getRecordNumber(), 1);
		assertEquals(cl.get(1).getStr(), field_values.get(0).get(1));
		
		assertEquals(cl.get(2).getField_id(), 3);
		assertEquals(cl.get(2).getImage_id(), params.getBatch());
		assertEquals(cl.get(2).getProject_id(), img2.getProject_id());
		assertEquals(cl.get(2).getRecordNumber(), 1);
		assertEquals(cl.get(2).getStr(), field_values.get(0).get(2));
		
		//-----------------------------------------------------
		
		assertEquals(cl.get(3).getField_id(), 1);
		assertEquals(cl.get(3).getImage_id(), params.getBatch());
		assertEquals(cl.get(3).getProject_id(), img2.getProject_id());
		assertEquals(cl.get(3).getRecordNumber(), 2);
		assertEquals(cl.get(3).getStr(), field_values.get(1).get(0));
		
		assertEquals(cl.get(4).getField_id(), 2);
		assertEquals(cl.get(4).getImage_id(), params.getBatch());
		assertEquals(cl.get(4).getProject_id(), img2.getProject_id());
		assertEquals(cl.get(4).getRecordNumber(), 2);
		assertEquals(cl.get(4).getStr(), field_values.get(1).get(1));
		
		assertEquals(cl.get(5).getField_id(), 3);
		assertEquals(cl.get(5).getImage_id(), params.getBatch());
		assertEquals(cl.get(5).getProject_id(), img2.getProject_id());
		assertEquals(cl.get(5).getRecordNumber(), 2);
		assertEquals(cl.get(5).getStr(), field_values.get(1).get(2));
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testDownloadBatch() throws SQLException, MalformedURLException{
		
		db.openDatabaseConnection();
		
		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24,
				"http://x.byu.edu:1234/known/first.txt");
		User george = new User("Great", "Gatsby", "Holy", "Cow", "wowness@icloud.com", 4, null);
		
		try {
			userAccess.insert(bob);
			userAccess.insert(amy);
			userAccess.insert(george);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
//		public Project(int id, String title, int recordsPerImage, int firstYCoord, int recordHeight)
		Project proj1 = new Project(44, "greatness", 11, 991, 1818181);
		Project proj2 = new Project(25, "asdfasdf", 124, 1244, 533);
		Project proj3 = new Project(1, "dsf", 1112, 4, 2);
		
		try {
			projectAccess.insert(proj1);
			projectAccess.insert(proj2);
			projectAccess.insert(proj3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
//		public Field(int id, String title, int xcoord, int width,
//		int fieldNumber, int project_id, String knownData, String helpHtml)
		Field field1 = new Field(1, "haha", 4, 1991, 4, 2, "nothing", "http://x.byu.edu:1234/known/first.txt");
		Field field2 = new Field(44, "sfasd", 2, 234, 10, 2, "something", "http://x.byu.edu:1234/known/second.txt");
		Field field3 = new Field(112, "googly", 22, 255, 6, 3, "everything", "http://x.byu.edu:1234/known/third.txt");
		
		try {
			fieldAccess.insert(field1);
			fieldAccess.insert(field2);
			fieldAccess.insert(field3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Image img1 = new Image(24, 44, "http://x.byu.edu:1234/known/first.txt", false);
		Image img2 = new Image(212, 25, "http://x.byu.edu:1234/known/second.txt", true);
		Image img3 = new Image(4422, 1, "http://x.byu.edu:1234/known/third.txt", true);
		
		try {
			imageAccess.insert(img1);
			imageAccess.insert(img2);
			imageAccess.insert(img3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//public DownloadBatch_Params(String user, String password, int project)
		DownloadBatch_Params params = new DownloadBatch_Params("Girly", "Hot pockets", 25);
		DownloadBatch_Params params2 = new DownloadBatch_Params("Neo", "Matrix", 44);
		DownloadBatch_Params params3 = new DownloadBatch_Params("Neo", "Matrix", 54);
		DownloadBatch_Params params4 = new DownloadBatch_Params("Neo", "garbage", 25);
		DownloadBatch_Params params5 = new DownloadBatch_Params("Neo", "Matrix", 25);
		DownloadBatch_Params params6 = new DownloadBatch_Params("Neo", "Matrix", 1);
		DownloadBatch_Params params7 = new DownloadBatch_Params("Great", "Gatsby", 1);
		DownloadBatch_Return ret;
		DownloadBatch_Return ret2;
		DownloadBatch_Return ret3;
		DownloadBatch_Return ret4;
		DownloadBatch_Return ret5;
		DownloadBatch_Return ret6;
		DownloadBatch_Return ret7;
		try {
			ret = facade.downloadBatch(params);
			ret2 = facade.downloadBatch(params2);
			ret3 = facade.downloadBatch(params3);
			ret4 = facade.downloadBatch(params4);
			ret5 = facade.downloadBatch(params5);
			ret6 = facade.downloadBatch(params6);
			ret7 = facade.downloadBatch(params7);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
//		private boolean succeeds;
//		private int batch_id;
//		private int project_id;
//		private URL image_url;
		
//		private int first_y_coord;
//		private int record_height;
//		private int num_records;
//		private int num_fields;
//		private ArrayList<Field> fields;
		
		assertEquals(false, ret.getSucceeds());
		assertEquals(false, ret2.getSucceeds());
		assertEquals(false, ret3.getSucceeds());
		assertEquals(false, ret4.getSucceeds());
		
		assertEquals(true, ret5.getSucceeds());
		assertEquals(img2.getId(), ret5.getBatch_id());
		assertEquals(img2.getFile(), ret5.getImage_url());
		assertEquals(img2.getProject_id(), ret5.getProject_id());
		assertEquals(proj2.getFirstYCoord(), ret5.getFirst_y_coord());
		assertEquals(proj2.getRecordHeight(), ret5.getRecord_height());
		assertEquals(proj2.getRecordsPerImage(), ret5.getNum_records());
		int supposed_field_count = 0;
		ArrayList<Field> supposed_fields = new ArrayList<Field>();
		for (Field focus : fieldAccess.getAll())
		{
			if (focus.getProject_id() == ret5.getProject_id())
			{
				supposed_field_count++;
				supposed_fields.add(focus);
			}
		}
		assertEquals(supposed_field_count, ret5.getNum_fields());
		assertEquals(supposed_fields, ret5.getFields());
		
		assertEquals(false, ret6.getSucceeds());
		
		assertEquals(true, ret7.getSucceeds());
		assertEquals(img3.getId(), ret7.getBatch_id());
		assertEquals(img3.getFile(), ret7.getImage_url());
		assertEquals(img3.getProject_id(), ret7.getProject_id());
		assertEquals(proj3.getFirstYCoord(), ret7.getFirst_y_coord());
		assertEquals(proj3.getRecordHeight(), ret7.getRecord_height());
		assertEquals(proj3.getRecordsPerImage(), ret7.getNum_records());
		supposed_field_count = 0;
		supposed_fields = new ArrayList<Field>();
		for (Field focus : fieldAccess.getAll())
		{
			if (focus.getProject_id() == ret7.getProject_id())
			{
				supposed_field_count++;
				supposed_fields.add(focus);
			}
		}
		assertEquals(supposed_field_count, ret7.getNum_fields());
		assertEquals(supposed_fields, ret7.getFields());
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testGetProjects() throws DatabaseException, SQLException {
		
		db.openDatabaseConnection();
		
		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24, "great.png");
		
		try {
			userAccess.insert(bob);
			userAccess.insert(amy);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Project proj1 = new Project(2124, "greatness", 37, 89, 919);
		Project proj2 = new Project(2444, "hallbart", 111110, 111, 49);
		Project proj3 = new Project(7474, "waffle", 424, 42, 55);
		
		try {
			projectAccess.insert(proj1);
			projectAccess.insert(proj2);
			projectAccess.insert(proj3);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		GetProjects_Params params = new GetProjects_Params("Neo", "Matrix");
		GetProjects_Return ret;
		try {
			ret = facade.getProjects(params);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		assertEquals(true, ret.getSucceeds());
		
		ArrayList<Project> projects = ret.getProjects();
		
		assertEquals(proj1.getId(), projects.get(0).getId());
		assertEquals(proj1.getTitle(), projects.get(0).getTitle());
		
		assertEquals(proj2.getId(), projects.get(1).getId());
		assertEquals(proj2.getTitle(), projects.get(1).getTitle());
		
		assertEquals(proj3.getId(), projects.get(2).getId());
		assertEquals(proj3.getTitle(), projects.get(2).getTitle());
		
		db.closeDatabaseConnection(false);
	}
	
	@Test
	public void testValidateUser() throws DatabaseException, SQLException {
		
		db.openDatabaseConnection();
		
//		public User(String userName, String password, String firstName, String lastName,
//				String email, int indexedRecords, String currentImage)
		
		User bob = new User("Neo", "Matrix", "Mr", "Anderson", "neo.matrix@gmail.com", 0, null);
		User amy = new User("Girly", "Hot pockets", "Sally", "Gretal", "hotty@gmail.com", 24, "great.png");
		
		try {
			userAccess.insert(bob);
			userAccess.insert(amy);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ValidateUser_Params params = new ValidateUser_Params("Neo", "Matrix");
		ValidateUser_Return ret;
		try {
			ret = facade.validateUser(params);
		} catch (SQLException e) {
			e.printStackTrace();
			// If it gets to this point, the function is doing what is supposed to be doing... throwing an Exception
			return;
		}
		assertEquals(true, ret.getSucceeds());
		
		params.setPassword("Galaxy");
		try {
			ret = facade.validateUser(params);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		assertEquals(false, ret.getSucceeds());
		
		params.setName("Girly");
		params.setPassword("Hot pockets");
		try {
			ret = facade.validateUser(params);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		assertEquals(true, ret.getSucceeds());
		
		params.setName("mexican");
		params.setPassword("Hot pockets");
		try {
			ret = facade.validateUser(params);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		assertEquals(false, ret.getSucceeds());
	}
}
