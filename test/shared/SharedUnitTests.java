package shared;

public class SharedUnitTests {

	public static void main(String[] args) {
		
		String[] testClasses = new String[] {
				"shared.model.CellValueTest",
				"shared.model.FieldTest",
				"shared.model.ImageTest",
				"shared.model.ProjectTest",
				"shared.model.UserTest"
		};

		org.junit.runner.JUnitCore.main(testClasses);
	}
	
}

