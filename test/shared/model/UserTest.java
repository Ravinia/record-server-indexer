package shared.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserTest {

	private User defaultUser;
	private User valuesUser;
	
	@Before
	public void setUp() throws Exception {
		
		defaultUser = new User();
		valuesUser = new User("deathknight101", "irule", "fred",
				"jones", "fred.jones@gmail.com", 101,"myImage.png");

	}

	@After
	public void tearDown() throws Exception {
		
		defaultUser = null;
		valuesUser = null;
	}

	@Test
	public void testDefaultConstructor() {
		
		assertEquals(null, defaultUser.getUserName());
		assertEquals(null, defaultUser.getPassword());
		assertEquals(null, defaultUser.getFirstName());
		assertEquals(null, defaultUser.getLastName());
		assertEquals(null, defaultUser.getEmail());
		assertEquals(-1, defaultUser.getIndexedRecords());
		assertEquals(null, defaultUser.getCurrentImage());
	}

	@Test
	public void testValuesConstructor() {
		
		assertEquals("deathknight101", valuesUser.getUserName());
		assertEquals("irule", valuesUser.getPassword());
		assertEquals("fred", valuesUser.getFirstName());
		assertEquals("jones", valuesUser.getLastName());
		assertEquals("fred.jones@gmail.com", valuesUser.getEmail());
		assertEquals(101, valuesUser.getIndexedRecords());
		assertEquals("myImage.png", valuesUser.getCurrentImage());
	}
	
	@Test
	public void testSetters() {
		
		defaultUser.setUserName("Rav");
		assertEquals("Rav", defaultUser.getUserName());
		
		defaultUser.setPassword("secure");
		assertEquals("secure", defaultUser.getPassword());
		
		defaultUser.setFirstName("Hayden");
		assertEquals("Hayden", defaultUser.getFirstName());
		
		defaultUser.setLastName("Jarbus");
		assertEquals("Jarbus", defaultUser.getLastName());
		
		defaultUser.setEmail("bob@white.org");
		assertEquals("bob@white.org", defaultUser.getEmail());

		defaultUser.setIndexedRecords(25);
		assertEquals(25, defaultUser.getIndexedRecords());	
		
		defaultUser.setCurrentImage("theCurrentImage.png");
		assertEquals("theCurrentImage.png", defaultUser.getCurrentImage());	
	}

}
