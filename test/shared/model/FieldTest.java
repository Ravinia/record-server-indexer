package shared.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FieldTest {

	private Field defaultField;
	private Field valuesField;

//	private int id;
//	private String title;
//	private int xcoord;
//	private int width;
//	private String helpHtml;
//	private String knownData;
//	private int fieldNumber;
//	private int project_id;
	
	@Before
	public void setUp() throws Exception {
		
		defaultField = new Field();
		valuesField = new Field(123, "my title", 55, 1124, 9911, 124, "myKnown", "myHelp");

	}

	@After
	public void tearDown() throws Exception {
		
		defaultField = null;
		valuesField = null;
	}

	@Test
	public void testDefaultConstructor() {
		
		assertEquals(-1, defaultField.getId());
		assertEquals(null, defaultField.getTitle());
		assertEquals(-1, defaultField.getXcoord());
		assertEquals(-1, defaultField.getWidth());
		assertEquals(-1, defaultField.getFieldNumber());
		assertEquals(-1, defaultField.getProject_id());
		assertEquals(null, defaultField.getHelpHtml());
		assertEquals(null, defaultField.getKnownData());
	}

	@Test
	public void testValuesConstructor() {
		
		assertEquals(123, valuesField.getId());
		assertEquals("my title", valuesField.getTitle());
		assertEquals(55, valuesField.getXcoord());
		assertEquals(1124, valuesField.getWidth());
		assertEquals(9911, valuesField.getFieldNumber());
		assertEquals(124, valuesField.getProject_id());
		assertEquals("myHelp", valuesField.getHelpHtml());
		assertEquals("myKnown", valuesField.getKnownData());
	}
	
	@Test
	public void testSetters() {
		
		defaultField.setId(123);
		assertEquals(123, defaultField.getId());
		
		defaultField.setTitle("myT");
		assertEquals("myT", defaultField.getTitle());
		
		defaultField.setXcoord(44);
		assertEquals(44, defaultField.getXcoord());	
		
		defaultField.setWidth(5111);
		assertEquals(5111, defaultField.getWidth());
		
		defaultField.setHelpHtml("hi");
		assertEquals("hi", defaultField.getHelpHtml());
		
		defaultField.setKnownData("known");
		assertEquals("known", defaultField.getKnownData());
		
		defaultField.setFieldNumber(5111);
		assertEquals(5111, defaultField.getFieldNumber());
		
		defaultField.setProject_id(2343);
		assertEquals(2343, defaultField.getProject_id());

	}

}
