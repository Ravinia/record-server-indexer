package shared.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ProjectTest {

	private Project defaultProject;
	private Project valuesProject;
	
	@Before
	public void setUp() throws Exception {
		
		defaultProject = new Project();
		valuesProject = new Project(123312, "greatness", 24, 2, 72);

	}

	@After
	public void tearDown() throws Exception {
		
		defaultProject = null;
		valuesProject = null;
	}

	@Test
	public void testDefaultConstructor() {
		
		assertEquals(-1, defaultProject.getId());
		assertEquals(null, defaultProject.getTitle());
		assertEquals(-1, defaultProject.getRecordsPerImage());
		assertEquals(-1, defaultProject.getFirstYCoord());
		assertEquals(-1, defaultProject.getRecordHeight());
	}

	@Test
	public void testValuesConstructor() {
		
		assertEquals(123312, valuesProject.getId());
		assertEquals("greatness", valuesProject.getTitle());
		assertEquals(24, valuesProject.getRecordsPerImage());
		assertEquals(2, valuesProject.getFirstYCoord());
		assertEquals(72, valuesProject.getRecordHeight());
	}
	
	@Test
	public void testSetters() {
		
		defaultProject.setId(123);
		assertEquals(123, defaultProject.getId());
		
		defaultProject.setTitle("ls;dkf");
		assertEquals("ls;dkf", defaultProject.getTitle());
		
		defaultProject.setRecordsPerImage(44);
		assertEquals(44, defaultProject.getRecordsPerImage());
		
		defaultProject.setFirstYCoord(5522);
		assertEquals(5522, defaultProject.getFirstYCoord());
		
		defaultProject.setRecordHeight(5111);
		assertEquals(5111, defaultProject.getRecordHeight());
	}

}
