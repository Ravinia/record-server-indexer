package shared.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ImageTest {

	private Image defaultImage;
	private Image valuesImage;
	
//	private int id;
//	private int project_id;
//	private String file;
//	private boolean isAvailable;
	
	@Before
	public void setUp() throws Exception {
		
		defaultImage = new Image();
		valuesImage = new Image(124, 838, "myFile", true);

	}

	@After
	public void tearDown() throws Exception {
		
		defaultImage = null;
		valuesImage = null;
	}

	@Test
	public void testDefaultConstructor() {
		
		assertEquals(-1, defaultImage.getId());
		assertEquals(-1, defaultImage.getProject_id());
		assertEquals(null, defaultImage.getFile());
		assertEquals(false, defaultImage.isAvailable());
	}

	@Test
	public void testValuesConstructor() {
		
		assertEquals(124, valuesImage.getId());
		assertEquals(838, valuesImage.getProject_id());
		assertEquals("myFile", valuesImage.getFile());
		assertEquals(true, valuesImage.isAvailable());
	}
	
	@Test
	public void testSetters() {
		
		defaultImage.setId(5125);
		assertEquals(5125, defaultImage.getId());
		
		defaultImage.setProject_id(54);
		assertEquals(54, defaultImage.getProject_id());
		
		defaultImage.setFile("fileness.gile");
		assertEquals("fileness.gile", defaultImage.getFile());
		
		defaultImage.setAvailable(true);
		assertEquals(true, defaultImage.isAvailable());
	}

}
