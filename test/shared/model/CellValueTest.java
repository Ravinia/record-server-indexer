package shared.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CellValueTest {

	private CellValue defaultCellValue;
	private CellValue valuesCellValue;

//	private int image_id;
//	private int project_id;
//	private int field_id;
//	private String str;
//	private int recordNumber;
	
	@Before
	public void setUp() throws Exception {
		
		defaultCellValue = new CellValue();
		valuesCellValue = new CellValue(123312, 234, 243, "yeah", 44);

	}

	@After
	public void tearDown() throws Exception {
		
		defaultCellValue = null;
		valuesCellValue = null;
	}

	@Test
	public void testDefaultConstructor() {
		
		assertEquals(-1, defaultCellValue.getImage_id());
		assertEquals(-1, defaultCellValue.getProject_id());
		assertEquals(-1, defaultCellValue.getField_id());
		assertEquals(null, defaultCellValue.getStr());
		assertEquals(-1, defaultCellValue.getRecordNumber());
	}

	@Test
	public void testValuesConstructor() {
		
		assertEquals(123312, valuesCellValue.getImage_id());
		assertEquals(234, valuesCellValue.getProject_id());
		assertEquals(243, valuesCellValue.getField_id());
		assertEquals("yeah", valuesCellValue.getStr());
		assertEquals(44, valuesCellValue.getRecordNumber());
	}
	
	@Test
	public void testSetters() {
		
		defaultCellValue.setImage_id(123);
		assertEquals(123, defaultCellValue.getImage_id());
		
		defaultCellValue.setProject_id(33);
		assertEquals(33, defaultCellValue.getProject_id());
		
		defaultCellValue.setField_id(44);
		assertEquals(44, defaultCellValue.getField_id());
		
		defaultCellValue.setStr("hi");
		assertEquals("hi", defaultCellValue.getStr());
		
		defaultCellValue.setRecordNumber(5111);
		assertEquals(5111, defaultCellValue.getRecordNumber());
	}

}
