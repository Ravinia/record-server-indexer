package searchgui;

import java.awt.*;

import shared.communication.Search_Params;
import shared.communication.Search_Return;
import shared.communication.ValidateUser_Params;
import shared.communication.Search_Return.SearchResult;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import client.communication.ClientCommunicator;


@SuppressWarnings("serial")
public class SearchPanel extends JPanel  implements ActionListener{

	public interface Context {
		
//		public void performedSearch(Search_Return sr);
		
		public void selectionChanged(String fileName);
	}

	private DefaultListModel<String> defaultListModel;
	private JList<String> searchList;
	private ArrayList<SearchResult> searchResults;
	
	private JButton loginButton;
	
	ClientCommunicator cc;
	
	JTextField fieldIDField;
	JTextField valuesField;
	
	String host;
	int port;
	
	private Context context;
	ValidateUser_Params paramsSaved;
	
	public SearchPanel(Context c, ValidateUser_Params params, String host, int port) {
		
		super();
		
        this.host = host;
        this.port = port;
		paramsSaved = params;
		context = c;
		cc = new ClientCommunicator(host, port);
		
		JLabel sectionLabel = new JLabel("Search:");
		JLabel fieldIDLabel = new JLabel("Field ID's:");
		JLabel valuesLabel = new JLabel("Values:");
		JLabel searchResultsLabel = new JLabel("Search Results:");
		
		fieldIDField = new JTextField(16);
		fieldIDField.setMaximumSize(new Dimension(128,32));
		fieldIDField.addActionListener(actionListener);
		
		valuesField = new JTextField(16);
		valuesField.setMaximumSize(new Dimension(128,32));
		valuesField.addActionListener(actionListener);
		
		defaultListModel = new DefaultListModel<String>();
		searchList = new JList<String>(defaultListModel);
		searchList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if (!arg0.getValueIsAdjusting())
                {
                	String value = searchList.getSelectedValue();
                	if (value != null)
                	{
	                	int index = value.indexOf("images");
	                	value = value.substring(index);
	                	context.selectionChanged(value);
                	}
                }
            }
        });
       
        JScrollPane favScrollPane = new JScrollPane(searchList);
        favScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        favScrollPane.setPreferredSize(new Dimension(250, 300));
        
        JPanel fieldsPanel = new JPanel();
        fieldsPanel.setLayout(new BoxLayout(fieldsPanel, BoxLayout.LINE_AXIS));
        fieldsPanel.add(fieldIDLabel);
        fieldsPanel.add(fieldIDField);
        
        JPanel valuesPanel = new JPanel();
        valuesPanel.setLayout(new BoxLayout(valuesPanel, BoxLayout.LINE_AXIS));
        valuesPanel.add(valuesLabel);
        valuesPanel.add(valuesField);
        
        Panel buttonPanel = new Panel();
        loginButton = new JButton("Search");
        loginButton.addActionListener(this);
        loginButton.setActionCommand("search");
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createHorizontalStrut(128));
        buttonPanel.add(loginButton);
        buttonPanel.add(Box.createHorizontalGlue());
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.add(sectionLabel);
        this.add(Box.createVerticalStrut(32));
        this.add(fieldsPanel);
        this.add(valuesPanel);
        this.add(buttonPanel);
        this.add(Box.createVerticalStrut(32));
        this.add(searchResultsLabel);
        this.add(favScrollPane);
        
	}
	
	public void updateHostAndPort(String host, int port)
	{
		this.host = host;
        this.port = port;
        
        cc = new ClientCommunicator(host, port);
	}
    
    private ActionListener actionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (context != null) {
//				context.onUrlChanged(getUrl());
			}
		}   	
    };
    
    public ArrayList<SearchResult> getSearchResults() {
		return searchResults;
	}
    
    public void wipe()
	{
    	setSearchResults(new ArrayList<SearchResult>());
    	fieldIDField.setText("");
    	valuesField.setText("");
	}

	public void setSearchResults(ArrayList<SearchResult> searchResults) {
		this.searchResults = searchResults;
		defaultListModel.removeAllElements();
		for (int i=0; i<searchResults.size(); i++)
		{
			defaultListModel.add(i, searchResults.get(i).getImage_url());
			System.out.println(searchResults.get(i).getImage_url());
		}
	}

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("search"))
        {	
        	ArrayList<Integer> fields = new ArrayList<Integer>();
        	ArrayList<String> values = new ArrayList<String>();
    		
    		Scanner scanner = new Scanner(fieldIDField.getText());
    		Scanner scannerFields = scanner.useDelimiter(",");
    		while(scannerFields.hasNext())
    			fields.add(Integer.parseInt(scannerFields.next()));
    		scanner.close();
    		
    		Scanner scanner2 = new Scanner(valuesField.getText());
    		Scanner scannerSearchValues = scanner2.useDelimiter(",");
    		while(scannerSearchValues.hasNext())
    			values.add(scannerSearchValues.next());
    		scanner2.close();
        	
    		Search_Params params = new Search_Params(paramsSaved.getName(), paramsSaved.getPassword(), fields, values);
    		Search_Return sr = cc.search(params);
    		
    		setSearchResults(sr.getSearchResults());
        }
    }
}
