package searchgui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


@SuppressWarnings("serial")
public class TechPanel extends JPanel {

	public interface Context {
		
		public void hostChanged(String host, int port);
		public void portChanged(String host, int port);
	}

	
	private Context context;
	private JTextField hostField;
	
	public String getHostField() {
		return hostField.getText();
	}

	public void setHostField(String hostField) {
		this.hostField.setText(hostField);
	}

	public String getPortField() {
		return portField.getText();
	}
	
	public void setPortField(String portField) {
		this.portField.setText(portField);
	}

	private JTextField portField;
	
	public TechPanel(Context c) {
		
		super();
		
		context = c;
		
		JLabel hostLabel = new JLabel("Host name:");
		JLabel portLabel = new JLabel("TCP port:");
        
		hostField = new JTextField(16);
		hostField.setMaximumSize(new Dimension(128,32));
        hostField.addActionListener(hostListener);
        
        portField = new JTextField(16);
        portField.setMaximumSize(new Dimension(128,32));
        portField.addActionListener(portListener);
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        Panel techPanel = new Panel();
        techPanel.setLayout(new BoxLayout(techPanel, BoxLayout.LINE_AXIS));
        techPanel.add(hostLabel);
        techPanel.add(hostField);
        techPanel.add(Box.createHorizontalStrut(32));
        techPanel.add(portLabel);
        techPanel.add(portField);
        techPanel.add(Box.createHorizontalGlue());
        this.add(techPanel);
	}

	
	private ActionListener hostListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (context != null) {
				context.hostChanged(hostField.getText(), Integer.parseInt(portField.getText()));
			}
		}   	
    };
    
    private ActionListener portListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (context != null) {
				context.portChanged(hostField.getText(), Integer.parseInt(portField.getText()));
			}
		}   	
    };
	
}
