package searchgui;

import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import shared.communication.*;
import client.communication.ClientCommunicator;

@SuppressWarnings("serial")
public class SearchWindow extends JFrame implements ActionListener
{
//	private JButton loginButton;
	TechPanel techPanel;
	ProjectsPanel projectsPanel;
	FieldsPanel fieldsPanel;
	SearchPanel searchPanel;
	ImagePanel imagePanel;
	
	ClientCommunicator cc;
	String host;
	int port;
	
	ValidateUser_Params savedParams;
	
	public SearchWindow()
	{
		
	}

    public SearchWindow(ValidateUser_Params params, String host, int port)
    {	
        super("Search Window");
        
        this.host = host;
        this.port = port;
    	savedParams = params;
    	
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
        
        techPanel = new TechPanel(techPanelContext);
        rootPanel.add(techPanel);
        
        JPanel longPanel = new JPanel();
        longPanel.setLayout(new BoxLayout(longPanel, BoxLayout.LINE_AXIS));
        
        projectsPanel = new ProjectsPanel(projectsPanelContext);
        fieldsPanel = new FieldsPanel(fieldsPanelContext);
        searchPanel = new SearchPanel(searchPanelContext, params, host, port);
        imagePanel = new ImagePanel();
        initializePanels(params, host, port);
		
		longPanel.add(projectsPanel);
		longPanel.add(fieldsPanel);
		longPanel.add(searchPanel);
		longPanel.add(imagePanel);
		
		rootPanel.add(longPanel);
        
        add(rootPanel);
        
        pack();
        setVisible(true);
    }
    
    public void initializePanels(ValidateUser_Params params, String host, int port)
    {   
    	cc = new ClientCommunicator(host, port);
    	
        // Write to techPanel
        techPanel.setHostField(host);
        techPanel.setPortField(port+"");
        
		// Add projects
		GetProjects_Params params2 = new GetProjects_Params(params.getName(), params.getPassword());
		GetProjects_Return ret = cc.getProjects(params2);
		
		if (ret != null)
			projectsPanel.setProjects(ret.getProjects());
    }
    
    public void resetPanels(ValidateUser_Params params, String host, int port)
    {
    	projectsPanel.wipe();
    	fieldsPanel.wipe();
    	searchPanel.wipe();
    	searchPanel.updateHostAndPort(host, port);
    	imagePanel.wipe();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        String cmd = e.getActionCommand();

        if(cmd.equals("search"))
        {
        	ClientCommunicator cc = new ClientCommunicator(host, port);
    		ValidateUser_Params params = new ValidateUser_Params(savedParams.getName(), savedParams.getPassword());
    		ValidateUser_Return ret = cc.validateUser(params);
    		
    		if (ret.getSucceeds() == true)
    		{
    			dispose();
                new SearchWindow();
    		}
    		else
    		{
    			JDialog dialog = new JDialog(new JFrame(), "Error", false);
    			dialog.setBounds(getX()+getWidth()/3, getY()+getHeight()/3, getWidth()/2, getHeight()/2);
    			JLabel errorMessage = new JLabel("Invalid Credentials");
    			dialog.add(errorMessage);
    			dialog.setVisible(true);
    		}
        }
    }
    
    private ProjectsPanel.Context projectsPanelContext = new ProjectsPanel.Context() {
		
		@Override
		public void selectionChanged(int projectID) {
			 GetFields_Params params = new GetFields_Params(savedParams.getName(), savedParams.getPassword(), projectID);
			 fieldsPanel.setFields(cc.getFields(params).getFields());
		}
	};
	
	private TechPanel.Context techPanelContext = new TechPanel.Context() {

		@Override
		public void hostChanged(String hostUpdated, int portUpdated) {
			port = portUpdated;
			host = hostUpdated;
			resetPanels(savedParams, host, port);
			initializePanels(savedParams, host, port);
			System.out.println("hostChanged");
		}
		
		@Override
		public void portChanged(String hostUpdated, int portUpdated) {
			port = portUpdated;
			host = hostUpdated;
			resetPanels(savedParams, host, port);
			initializePanels(savedParams, host, port);
			System.out.println("portChanged");
		}
	};
	
	private FieldsPanel.Context fieldsPanelContext = new FieldsPanel.Context() {

		@Override
		public void selectionChanged(String url) {
//			htmlPanel.setUrl(url);
		}
	};
	
	private SearchPanel.Context searchPanelContext = new SearchPanel.Context() {

		@Override
		public void selectionChanged(String fileName) {
			byte[] bytes = cc.downloadFile(fileName);
			imagePanel.updateFile(bytes);
		}

	};
}