package searchgui;

import java.awt.*;

import shared.model.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;


@SuppressWarnings("serial")
public class ImagePanel extends JPanel {

	JLabel picLabel;
	
	public ImagePanel() {
		
		super();

		JLabel tableLabel = new JLabel("Image:");
		
		File file = new File("initialImage.jpg");
		BufferedImage myPicture = null;
		try {
			if (file != null)
				myPicture = ImageIO.read(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (myPicture == null)
			picLabel = new JLabel();
		else
			picLabel = new JLabel(new ImageIcon(myPicture));
		
		JScrollPane favScrollPane = new JScrollPane(picLabel);
		favScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		favScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        favScrollPane.setPreferredSize(new Dimension(600, 600));
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(tableLabel);
        add(favScrollPane);
	}
	
	public void wipe()
	{
		File file = new File("initialImage.jpg");
		BufferedImage myPicture = null;
		try {
			if (file != null)
				myPicture = ImageIO.read(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (myPicture == null)
			picLabel.setIcon(new ImageIcon());
		else
			picLabel.setIcon(new ImageIcon(myPicture));
	}
	
	void updateFile(byte[] bytes)
	{
//		picLabel.removeAll();
		BufferedImage myPicture = null;
		InputStream in = new ByteArrayInputStream(bytes);
		try {
				myPicture = ImageIO.read(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		picLabel.setIcon(new ImageIcon(myPicture));
	}
}
