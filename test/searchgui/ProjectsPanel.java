package searchgui;

import java.awt.*;

import shared.model.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


@SuppressWarnings("serial")
public class ProjectsPanel extends JPanel {

	public interface Context {
		
		public void selectionChanged(int projectID);
	}

	private Context context;
	
	private DefaultListModel<String> defaultListModel;
	private JList<String> projectList;
	private ArrayList<Project> projects;
	
	public ProjectsPanel(Context c) {
		
		super();
		
		context = c;
		
		JLabel tableLabel = new JLabel("Projects:");
		
		defaultListModel = new DefaultListModel<String>();
		projectList = new JList<String>(defaultListModel);
		
		projectList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if (!arg0.getValueIsAdjusting()) {
                	context.selectionChanged(projectList.getSelectedIndex()+1);
                }
            }
        });
       
        JScrollPane favScrollPane = new JScrollPane(projectList);
        favScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        favScrollPane.setPreferredSize(new Dimension(250, 600));
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.add(tableLabel);
        this.add(favScrollPane);
        
	}
	
	public void wipe()
	{
		setProjects(new ArrayList<Project>());
	}
	
	public ArrayList<Project> getProjects() {
		return projects;
	}

	public void setProjects(ArrayList<Project> projects) {
		this.projects = projects;
		defaultListModel.removeAllElements();
		for (int i=0; i<projects.size(); i++)
		{
			defaultListModel.add(i, "("+projects.get(i).getId()+")" + projects.get(i).getTitle());
			System.out.println(projects.get(i).getTitle());
		}
	}
}
