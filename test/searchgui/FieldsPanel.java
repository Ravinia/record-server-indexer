package searchgui;

import java.awt.*;

import shared.model.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.*;


@SuppressWarnings("serial")
public class FieldsPanel extends JPanel {

	public interface Context {
		
		public void selectionChanged(String url);
	}

	private DefaultListModel<String> defaultListModel;
	private JList<String> fieldList;
	private ArrayList<Field> fields;
	
	public FieldsPanel(Context c) {
		
		super();
		
		JLabel tableLabel = new JLabel("Fields:");
		
		defaultListModel = new DefaultListModel<String>();
		fieldList = new JList<String>(defaultListModel);
		fieldList.addMouseListener(mouseAdapter);
       
        JScrollPane favScrollPane = new JScrollPane(fieldList);
        favScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        favScrollPane.setPreferredSize(new Dimension(250, 600));
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        this.add(tableLabel);
        this.add(favScrollPane);
        
	}
	
	public ArrayList<Field> getFields() {
		return fields;
	}
	
	public void wipe()
	{
		setFields(new ArrayList<Field>());
	}

	public void setFields(ArrayList<Field> fields) {
		this.fields = fields;
//		String strings[] = new String[fields.size()];
		defaultListModel.removeAllElements();
		for (int i=0; i<fields.size(); i++)
		{
//			strings[i] = fields.get(i).getTitle();
			defaultListModel.add(i, "("+fields.get(i).getId()+")" + fields.get(i).getTitle());
			System.out.println(fields.get(i).getTitle());
		}
//		fieldList = new JList<String>(strings);
	}

	private MouseAdapter mouseAdapter = new MouseAdapter() {

		@Override
		public void mouseReleased(MouseEvent e) {
			if (e.isPopupTrigger()) {
				if (e.getSource() == fieldList) {
//					favPopupMenu.show(e.getComponent(),
//		                       			e.getX(), e.getY());
				}
			}
		} 	
    };
}
