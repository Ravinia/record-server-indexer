package searchgui;

import java.awt.Dimension;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.communication.*;
import shared.communication.*;


@SuppressWarnings("serial")
public class ValidateUserPanel extends JPanel {

public interface Context {
		
		public void onUsernameChanged(String url);
	}
	
//	private Context context;
	private JTextField usernameField;
	private JTextField passwordField;
	private JButton loginButton;
	
	public ValidateUserPanel(Context c) {
		
		super();
		
//		context = c;
		
        JLabel usernameLabel = new JLabel("Username:");
		JLabel passwordLabel = new JLabel("Password:");
        
		usernameField = new JTextField(16);
		usernameField.setMaximumSize(new Dimension(128,32));
		usernameField.addActionListener(actionListener);
        
        passwordField = new JTextField(16);
        passwordField.setMaximumSize(new Dimension(128,32));
        passwordField.addActionListener(actionListener);
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        
        Panel usernamePanel = new Panel();
        usernamePanel.setLayout(new BoxLayout(usernamePanel, BoxLayout.LINE_AXIS));
        usernamePanel.add(usernameLabel);
        usernamePanel.add(usernameField);
        usernamePanel.add(Box.createHorizontalGlue());
        this.add(usernamePanel);
        
        Panel passwordPanel = new Panel();
        passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.LINE_AXIS));
        passwordPanel.add(passwordLabel);
        passwordPanel.add(passwordField);
        passwordPanel.add(Box.createHorizontalGlue());
        this.add(passwordPanel);
        
        Panel buttonPanel = new Panel();
        loginButton = new JButton("Login");
        loginButton.addActionListener(actionListener);
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        buttonPanel.add(Box.createHorizontalStrut(128));
        buttonPanel.add(loginButton);
        buttonPanel.add(Box.createHorizontalGlue());
        this.add(buttonPanel);
        
        this.add(Box.createVerticalGlue());
	}
	
	public void setUsernameField(String text) {
		usernameField.setText(text);
	}
	
	public String getUsernameField() {
		return usernameField.getText();
	}
	
	public void setPasswordField(String text) {
		passwordField.setText(text);
	}
	
	public String getPasswordField() {
		return passwordField.getText();
	}
	
	public void login()
	{
		ClientCommunicator cc = new ClientCommunicator("localhost", 8881);
		ValidateUser_Params params = new ValidateUser_Params(getUsernameField(), getPasswordField());
		ValidateUser_Return ret = cc.validateUser(params);
		
		if (ret.getSucceeds() == true)
		{
			
		}
	}
	
	private ActionListener actionListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == loginButton) {
	    		login();
	    	}
		}   	
    };
}
