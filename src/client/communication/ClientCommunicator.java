package client.communication;


import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import client.ClientException;
import shared.communication.*;
import shared.communication.Search_Return.SearchResult;
import shared.model.Field;
import sun.misc.IOUtils;

public class ClientCommunicator {
	private String serverHost;
	private int serverPort;
	private static Logger logger;
	
	public ClientCommunicator(String serverHost, int serverPort) {
		Level logLevel = Level.ALL;
		
		logger = Logger.getLogger("clientmanager"); 
		logger.setLevel(logLevel);
		logger.setUseParentHandlers(false);
		
		Handler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(logLevel);
		consoleHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(consoleHandler);
		
		this.serverHost = serverHost;
		this.serverPort = serverPort;
	}

	/**
	* Validates user credentials
	*/
	public ValidateUser_Return validateUser(ValidateUser_Params params){
		logger.info("Validating user");
		
		Object ret = null;
		try {
			ret = doPost("/ValidateUser", params);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		if (ret == null)
			return null;
		return (ValidateUser_Return) ret;
	}

	/**
	* Returns information about all of the available projects
	*/
	public GetProjects_Return getProjects(GetProjects_Params params){
		logger.info("Getting projects");
		
		Object ret = null;
		try {
			ret = doPost("/GetProjects", params);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		if (ret == null)
			return null;
		return (GetProjects_Return) ret;
	}

	/**
	* Returns a sample image for the specified project
	*/
	public GetSampleImage_Return getSampleImage(GetSampleImage_Params params){
		logger.info("Getting sample image");
		
		Object ret = null;
		try {
			ret = doPost("/GetSampleImage", params);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		if (ret == null)
			return null;
		
		GetSampleImage_Return real = (GetSampleImage_Return) ret;
		real.setImage_url(makeLocalUrlFromPath(real.getImage_url()));
		return real;
	}

	/**
	* Downloads a batch for the user to index
	*/
	public DownloadBatch_Return downloadBatch(DownloadBatch_Params params){
		logger.info("Downloading batch");
		
		Object ret = null;
		try {
			ret = doPost("/DownloadBatch", params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret == null)
			return null;
		
		DownloadBatch_Return real = (DownloadBatch_Return) ret;
		real.setImage_url(makeLocalUrlFromPath(real.getImage_url()));
		ArrayList<Field> fields = real.getFields();
		if (fields != null)
		{
			for (Field field : fields)
			{
				field.setHelpHtml(makeLocalUrlFromPath(field.getHelpHtml()));
				if (!field.getKnownData().equals("null"))
					field.setKnownData(makeLocalUrlFromPath(field.getKnownData()));
			}
		}
		return real;
	}

	/**
	* Submits the indexed record field values for a batch to the Server
	*/
	public SubmitBatch_Return submitBatch(SubmitBatch_Params params){
		logger.info("Submitting batch");
		
		Object ret = null;
		try {
			ret = doPost("/SubmitBatch", params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret == null)
			return null;
		return (SubmitBatch_Return) ret;
	}

	/**
	* Returns information about all of the fields for the specified project
	* If no project is specified, returns information about all of the fields for all projects in the system
	*/
	public GetFields_Return getFields(GetFields_Params params){
		logger.info("Getting fields");
		
		Object ret = null;
		try {
			ret = doPost("/GetFields", params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret == null)
			return null;
		return (GetFields_Return) ret;
	}

	/**
	* Searches the indexed records for the specified strings
	*/
	public Search_Return search(Search_Params params){
		logger.info("Searching");
		
		Object ret = null;
		try {
			ret = doPost("/Search", params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret == null)
			return null;
		
		Search_Return real = (Search_Return) ret;
		ArrayList<SearchResult> results = real.getSearchResults();
		if (results != null)
		{
			for (SearchResult sr : results)
			{
				sr.setImage_url(makeLocalUrlFromPath(sr.getImage_url()));
			}
		}
		return real;
	}

	/**
	* Downloads a file for the user
	*/
	public byte[] downloadFile(String urlPath){
		logger.info("Downloading file");
		
		byte[] ret = null;
		try {
			ret = doGet(urlPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (ret == null)
			return null;
		return ret;
	}
	
	public String makeLocalUrlFromPath(String path)
	{
		String scheme = "http";
		String domain = serverHost;
		String port = serverPort+"";
		
		String urlString = scheme + "://" + domain + ":" + port + "/database/" + path;
		return urlString;
	}
	
	private byte[] doGet(String urlPath) throws ClientException, IOException
	{
		//		 Make HTTP GET request to the specified URL, 
		//		 and return the object returned by the server
		
		String urlString = makeLocalUrlFromPath(urlPath);
		System.out.println(urlString);
		
		try
		{
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			// Set HTTP request headers, if necessary
			// connection.addRequestProperty("Accept", "text/html");
			connection.connect();
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK)
			{
				// Get HTTP response headers, if necessary
				InputStream responseBody = connection.getInputStream();
				
				//return IOUtils.toByteArray(is);
				
				ByteArrayOutputStream buffer = new ByteArrayOutputStream();

				int nRead;
				byte[] data = new byte[16384];

				while ((nRead = responseBody.read(data, 0, data.length)) != -1) {
				  buffer.write(data, 0, nRead);
				}
				
				buffer.flush();
				
				return buffer.toByteArray();
			} 
			else
			{
			System.out.println("IO ERROR");
			}
		}
		catch (IOException e)
		{
			System.out.println("IO ERROR");
		}
		return null;
	}
	
	private Object doPost(String urlPath, Object postData) throws ClientException, IOException {
		logger.info("doPost");
		
		// Make HTTP POST request to the specified URL, 
		// passing in the specified postData object
		
		Object ret = null;
		
//		* scheme - http
//		* domain - www.espn.com
//		* port-80
//		* path - /basketball/nba/index.html
//		* query_string - ?team=dallas&order=name
//		* fragment_id - #Roster
		
		// http://localhost:8881/ValidateUser
		
		String scheme = "http";
		String domain = serverHost;
		String port = serverPort+"";
		String path = urlPath;
		
		String urlString = scheme + "://" + domain + ":" + port + path;
		
		logger.info(urlString);
		
		URL url = new URL(urlString);
 
		try
		{
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true); // We want to send request body
			
			// Set HTTP request headers, if necessary
			//connection.addRequestProperty("Accept", "text/html");
			connection.connect();
			OutputStream requestBody = connection.getOutputStream();
			OutputStream outFile = new BufferedOutputStream(requestBody);
			
			// Write request body to OutputStream ...
			XStream xStream = new XStream(new DomDriver());
			xStream.toXML(postData, outFile);
			outFile.close();
			requestBody.close();
			
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				// Get HTTP response headers, if necessary
				InputStream responseBody = connection.getInputStream();
				
				// Read response body from InputStream ...
				InputStream inFile = new BufferedInputStream(responseBody);
				ret = xStream.fromXML(inFile);
				inFile.close();
				
				logger.info("finished with xstream");
			}
			else
			{
				// SERVER RETURNED AN HTTP ERROR
				System.out.println("SERVER RETURNED AN HTTP ERROR");
				return null;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		if (ret != null)
			logger.info("returning: " + (ret).toString());
		else
			logger.info("returning: " + "null");
		
		return ret;
	}
}
