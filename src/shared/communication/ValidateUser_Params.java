package shared.communication;

/**
* The encapsulated parameters of ClientCommunicator.ValidateUser
* Namely, the user's name and the user's password
*/
public class ValidateUser_Params {
	private String name;
	private String password;
	
	@Override
	public String toString()
	{
		String ret = "";
		ret += name+"\n"+password+"\n";
		return ret;
	}
	
	public ValidateUser_Params()
	{
		setName(null);
		setPassword(null);
	}
	
	public ValidateUser_Params(String name, String password)
	{
		setName(name);
		setPassword(password);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
