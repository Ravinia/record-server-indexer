package shared.communication;

import java.util.ArrayList;

import shared.model.Field;

/**
* The encapsulated return value of ClientCommunicator.GetFields
* A successful return yields the field info
* An unsuccessful return yields a "failed" signal
*/
public class GetFields_Return {
	private boolean succeeds;
	private ArrayList<Field> fields;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
			for (int i=0; i<fields.size(); i++)
			{
				ret += fields.get(i).getProject_id()+"\n"+fields.get(i).getId()+"\n"+fields.get(i).getTitle()+"\n";
			}
		else
			ret += "FAILED\n";
		return ret;
	}
	
	public GetFields_Return()
	{
		setSucceeds(false);
		setFields(null);
	}
	
	public GetFields_Return(boolean succeeds, ArrayList<Field> fields)
	{
		setSucceeds(succeeds);
		setFields(fields);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
	public ArrayList<Field> getFields() {
		return fields;
	}
	public void setFields(ArrayList<Field> fields) {
		this.fields = fields;
	}
}
