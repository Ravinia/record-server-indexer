package shared.communication;

import java.util.ArrayList;

/**
* The encapsulated return value of ClientCommunicator.Search
* A successful return yields the search result
* An unsuccessful return yields a "failed" signal
*/
public class Search_Return {
	private boolean succeeds;
	private ArrayList<SearchResult> searchResults;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
			for (int i=0; i<searchResults.size(); i++)
			{
				ret += searchResults.get(i).getBatch_id()+"\n"+searchResults.get(i).getImage_url()+
						"\n"+searchResults.get(i).getRecord_num()+"\n"+searchResults.get(i).getField_id()+"\n";
			}
		else
			ret += "FAILED\n";
		return ret;
	}

	public Search_Return()
	{
		setSucceeds(false);
		setSearchResults(null);
	}
	
	public Search_Return(boolean succeeds, ArrayList<SearchResult> searchResults)
	{
		setSucceeds(succeeds);
		setSearchResults(searchResults);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
	public ArrayList<SearchResult> getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(ArrayList<SearchResult> searchResults) {
		this.searchResults = searchResults;
	}

	public class SearchResult
	{
		private int batch_id;
		private String image_url;
		private int record_num;
		private int field_id;

		public SearchResult()
		{
			setBatch_id(-1);
			setImage_url(null);
			setRecord_num(-1);
			setField_id(-1);
		}
		
		public SearchResult(int batch_id, String image_url, int record_num, int field_id)
		{
			setBatch_id(batch_id);
			setImage_url(image_url);
			setRecord_num(record_num);
			setField_id(field_id);
		}
		
		public int getBatch_id() {
			return batch_id;
		}
		public void setBatch_id(int batch_id) {
			this.batch_id = batch_id;
		}
		public String getImage_url() {
			return image_url;
		}
		public void setImage_url(String image_url) {
			this.image_url = image_url;
		}
		public int getRecord_num() {
			return record_num;
		}
		public void setRecord_num(int record_num) {
			this.record_num = record_num;
		}
		public int getField_id() {
			return field_id;
		}
		public void setField_id(int field_id) {
			this.field_id = field_id;
		}
	}
}
