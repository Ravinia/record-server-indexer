package shared.communication;

/**
* The encapsulated return value of ClientCommunicator.SubmitBatch
* If the operation succeeds, a "true" signal is returned
* If the operation fails, a "failed" signal is returned
*/
public class SubmitBatch_Return {
	private boolean succeeds;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
			ret += "TRUE\n";
		else
			ret += "FAILED\n";
		return ret;
	}
	
	public SubmitBatch_Return()
	{
		setSucceeds(false);
	}
	
	public SubmitBatch_Return(boolean succeeds)
	{
		setSucceeds(succeeds);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
}
