package shared.communication;

import java.util.ArrayList;

/**
* The encapsulated parameters of ClientCommunicator.SubmitBatch
* Namely, the user's name, the user's password, the batch ID, and the field values
*/
public class SubmitBatch_Params {
	private String user;
	private String password;
	private int batch;
	private ArrayList<ArrayList<String>> field_values;
	
	@Override
	public String toString()
	{
		String ret = "";
		ret += user+"\n"+password+"\n"+batch+"\n";
		for (int i=0; i<field_values.size();i++)
		{
			if (i > 0) ret += ";";
			for (int j=0; j<field_values.get(i).size();j++)
			{
				if (j > 0) ret += ",";
				ret += field_values.get(i).get(j);
			}
		}
		return ret;
	}
	
	public SubmitBatch_Params()
	{
		setUser(null);
		setPassword(null);
		setBatch(-1);
		setField_values(null);
	}
	
	public SubmitBatch_Params(String user, String password, int batch, ArrayList<ArrayList<String>> field_values)
	{
		setUser(user);
		setPassword(password);
		setBatch(batch);
		setField_values(field_values);
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getBatch() {
		return batch;
	}
	public void setBatch(int batch) {
		this.batch = batch;
	}
	public ArrayList<ArrayList<String>> getField_values() {
		return field_values;
	}
	public void setField_values(ArrayList<ArrayList<String>> field_values) {
		this.field_values = field_values;
	}
}
