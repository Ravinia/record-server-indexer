package shared.communication;

/**
* The encapsulated parameters of ClientCommunicator.DownloadBatch
* Namely, the user's name, the user's password, and the project ID
*/
public class DownloadBatch_Params {
	private String user;
	private String password;
	private int project;
	
	@Override
	public String toString()
	{
		String ret = "";

		ret += user+"\n"+password+"\n"+project+"\n";
		return ret;
	}
	
	public DownloadBatch_Params()
	{
		setUser(null);
		setPassword(null);
		setProject(-1);
	}
	
	public DownloadBatch_Params(String user, String password, int project)
	{
		setUser(user);
		setPassword(password);
		setProject(project);
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getProject() {
		return project;
	}
	public void setProject(int project) {
		this.project = project;
	}
}
