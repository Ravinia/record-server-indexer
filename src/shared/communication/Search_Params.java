package shared.communication;

import java.util.ArrayList;

/**
* The encapsulated parameters of ClientCommunicator.Search
* Namely, the user's name, the user's password, a comma-separated list of fields to be searched
* and a comma-separated list of strings to search for
*/
public class Search_Params {
	private String user;
	private String password;
	private ArrayList<Integer> fields;
	private ArrayList<String> search_values;
	
	@Override
	public String toString()
	{
		String ret = "";
		ret += user+"\n"+password+"\n";
		for (int i=0; i<fields.size();i++)
		{
			if (i > 0) ret += ",";
			ret += fields.get(i);
		}
		ret += "\n";
		for (int i=0; i<search_values.size();i++)
		{
			if (i > 0) ret += ",";
			ret += search_values.get(i);
		}
		return ret;
	}
	
	public Search_Params()
	{
		setUser(null);
		setPassword(null);
		setFields(null);
		setSearch_values(null);
	}
	
	public Search_Params(String user, String password, ArrayList<Integer> fields, ArrayList<String> search_values)
	{
		setUser(user);
		setPassword(password);
		setFields(fields);
		setSearch_values(search_values);
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public ArrayList<Integer> getFields() {
		return fields;
	}
	public void setFields(ArrayList<Integer> fields) {
		this.fields = fields;
	}
	public ArrayList<String> getSearch_values() {
		return search_values;
	}
	public void setSearch_values(ArrayList<String> search_values) {
		this.search_values = search_values;
	}
}
