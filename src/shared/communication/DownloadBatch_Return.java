package shared.communication;

import java.util.ArrayList;

import shared.model.Field;

/**
* The encapsulated return value of ClientCommunicator.DownloadBatch
* A successful return yields a batch
* An unsuccessful return yields a "failed" signal
*/
public class DownloadBatch_Return {
	private boolean succeeds;
	private int batch_id;
	private int project_id;
	private String image_url;
	private int first_y_coord;
	private int record_height;
	private int num_records;
	private int num_fields;
	private ArrayList<Field> fields;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
		{
			ret += batch_id+"\n"+project_id+"\n"+image_url+"\n"+first_y_coord+"\n"+record_height+"\n"+num_records+"\n"+num_fields+"\n";
			for (int i=0; i<fields.size(); i++)
			{
				ret += fields.get(i).getId()+"\n"+fields.get(i).getFieldNumber()+"\n"+fields.get(i).getTitle()+
						"\n"+fields.get(i).getHelpHtml()+"\n"+fields.get(i).getXcoord()+"\n"+fields.get(i).getWidth()+"\n";
//				System.out.println("knowndata: "+fields.get(i).getKnownData());
				if (!fields.get(i).getKnownData().equals("null"))
					ret += fields.get(i).getKnownData()+"\n";
			}
		}
		else
			ret += "FAILED\n";
		return ret;
	}
	
	public DownloadBatch_Return()
	{
		setSucceeds(false);
		setBatch_id(-1);
		setProject_id(-1);
		setImage_url(null);
		setFirst_y_coord(-1);
		setRecord_height(-1);
		setNum_records(-1);
		setNum_fields(-1);
		setFields(null);
	}
	
	public DownloadBatch_Return(boolean succeeds, int batch_id,
	int project_id, String image_url, int first_y_coord, int record_height,
	int num_records, int num_fields, ArrayList<Field> fields)
	{
		setSucceeds(succeeds);
		setBatch_id(batch_id);
		setProject_id(project_id);
		setImage_url(image_url);
		setFirst_y_coord(first_y_coord);
		setRecord_height(record_height);
		setNum_records(num_records);
		setNum_fields(num_fields);
		setFields(fields);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
	public int getBatch_id() {
		return batch_id;
	}
	public void setBatch_id(int batch_id) {
		this.batch_id = batch_id;
	}
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public int getFirst_y_coord() {
		return first_y_coord;
	}
	public void setFirst_y_coord(int first_y_coord) {
		this.first_y_coord = first_y_coord;
	}
	public int getRecord_height() {
		return record_height;
	}
	public void setRecord_height(int record_height) {
		this.record_height = record_height;
	}
	public int getNum_records() {
		return num_records;
	}
	public void setNum_records(int num_records) {
		this.num_records = num_records;
	}
	public int getNum_fields() {
		return num_fields;
	}
	public void setNum_fields(int num_fields) {
		this.num_fields = num_fields;
	}
	public ArrayList<Field> getFields() {
		return fields;
	}
	public void setFields(ArrayList<Field> fields) {
		this.fields = fields;
	}
}
