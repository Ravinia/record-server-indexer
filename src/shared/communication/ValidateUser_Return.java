package shared.communication;

/**
* The encapsulated parameters of ClientCommunicator.ValidateUser
* If the credentials are valid, a "true" signal is returned
* If the credentials are invalid, a "false" signal is returned
* If the operation fails, a "failed" signal is returned
*/
public class ValidateUser_Return {
	private boolean succeeds;
	private String user_first_name;
	private String user_last_name;
	private int num_records;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
			ret += "TRUE\n"+user_first_name+"\n"+user_last_name+"\n"+num_records+"\n";
		else
			ret += "FALSE\n";
		return ret;
	}
	
	public ValidateUser_Return()
	{
		setSucceeds(false);
		setUser_first_name(null);
		setUser_last_name(null);
		setNum_records(-1);
	}
	
	public ValidateUser_Return(boolean succeeds, String user_first_name, String user_last_name, int num_records)
	{
		setSucceeds(succeeds);
		setUser_first_name(user_first_name);
		setUser_last_name(user_last_name);
		setNum_records(num_records);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
	public String getUser_first_name() {
		return user_first_name;
	}
	public void setUser_first_name(String user_first_name) {
		this.user_first_name = user_first_name;
	}
	public String getUser_last_name() {
		return user_last_name;
	}
	public void setUser_last_name(String user_last_name) {
		this.user_last_name = user_last_name;
	}
	public int getNum_records() {
		return num_records;
	}
	public void setNum_records(int num_records) {
		this.num_records = num_records;
	}
}
