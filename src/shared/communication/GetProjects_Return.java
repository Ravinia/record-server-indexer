package shared.communication;

import java.util.ArrayList;

import shared.model.Project;

/**
* The encapsulated return value of ClientCommunicator.GetProjects
* A successful return yields the project info
* An unsuccessful return yields a "failed" signal
*/
public class GetProjects_Return {
	private boolean succeeds;
	private ArrayList<Project> projects;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
			for (int i=0; i<projects.size(); i++)
			{
				ret += projects.get(i).getId()+"\n"+projects.get(i).getTitle()+"\n";
			}
		else
			ret += "FAILED\n";
		return ret;
	}
	
	public GetProjects_Return()
	{
		setSucceeds(false);
		setProjects(null);
	}
	
	public GetProjects_Return(boolean succeeds, ArrayList<Project> projects)
	{
		setSucceeds(succeeds);
		setProjects(projects);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
	public ArrayList<Project> getProjects() {
		return projects;
	}
	public void setProjects(ArrayList<Project> projects) {
		this.projects = projects;
	}
}
