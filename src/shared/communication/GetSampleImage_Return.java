package shared.communication;

/**
* The encapsulated return value of ClientCommunicator.GetSampleImage
* A successful return yields the image URL info
* An unsuccessful return yields a "failed" signal
*/
public class GetSampleImage_Return {
	private boolean succeeds;
	private String image_url;
	
	@Override
	public String toString()
	{
		String ret = "";
		if (succeeds)
			ret += image_url+"\n"; 
		else
			ret += "FAILED\n";
		return ret;
	}
	
	public GetSampleImage_Return()
	{
		setSucceeds(false);
		setImage_url(null);
	}
	
	public GetSampleImage_Return(boolean succeeds, String image_url)
	{
		setSucceeds(succeeds);
		setImage_url(image_url);
	}
	
	public boolean getSucceeds() {
		return succeeds;
	}
	public void setSucceeds(boolean succeeds) {
		this.succeeds = succeeds;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
}
