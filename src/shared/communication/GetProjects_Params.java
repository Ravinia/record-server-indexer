package shared.communication;

/**
* The encapsulated parameters of ClientCommunicator.GetProjects
* Namely, the user's name and the user's password
*/
public class GetProjects_Params {
	private String user;
	private String password;
	
	@Override
	public String toString()
	{
		String ret = "";
		ret += user+"\n"+password+"\n";
		return ret;
	}
	
	public GetProjects_Params()
	{
		setUser(null);
		setPassword(null);
	}
	
	public GetProjects_Params(String user, String password)
	{
		setUser(user);
		setPassword(password);
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
