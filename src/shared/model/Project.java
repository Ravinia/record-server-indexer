package shared.model;

/** 
* Each Project object contains the information about a single project.
*/
public class Project {
	private int id;
	private String title;
	private int recordsPerImage;
	private int firstYCoord;
	private int recordHeight;
	
	public Project()
	{
		setId(-1);
		setTitle(null);
		setRecordsPerImage(-1);
		setFirstYCoord(-1);
		setRecordHeight(-1);
	}
	
	public Project(int id, String title, int recordsPerImage, int firstYCoord, int recordHeight)
	{
		setId(id);
		setTitle(title);
		setRecordsPerImage(recordsPerImage);
		setFirstYCoord(firstYCoord);
		setRecordHeight(recordHeight);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getRecordsPerImage() {
		return recordsPerImage;
	}
	public void setRecordsPerImage(int recordsPerImage) {
		this.recordsPerImage = recordsPerImage;
	}
	public int getFirstYCoord() {
		return firstYCoord;
	}
	public void setFirstYCoord(int firstYCoord) {
		this.firstYCoord = firstYCoord;
	}
	public int getRecordHeight() {
		return recordHeight;
	}
	public void setRecordHeight(int recordHeight) {
		this.recordHeight = recordHeight;
	}
}
