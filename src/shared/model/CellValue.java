package shared.model;

/** 
* Each CellValue object contains the information for a cell value of the system.
*/
public class CellValue {
	private int image_id;
	private int project_id;
	private int field_id;
	private String str;
	private int recordNumber;
	
	public CellValue()
	{
		setImage_id(-1);
		setProject_id(-1);
		setField_id(-1);
		setStr(null);
		setRecordNumber(-1);
	}
	
	public CellValue(int image_id, int project_id, int field_id, String str, int recordNumber)
	{
		setImage_id(image_id);
		setProject_id(project_id);
		setField_id(field_id);
		setStr(str);
		setRecordNumber(recordNumber);
	}
	
	public int getImage_id() {
		return image_id;
	}
	public void setImage_id(int image_id) {
		this.image_id = image_id;
	}
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public int getField_id() {
		return field_id;
	}
	public void setField_id(int field_id) {
		this.field_id = field_id;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public int getRecordNumber() {
		return recordNumber;
	}
	public void setRecordNumber(int recordNumber) {
		this.recordNumber = recordNumber;
	}
	
}
