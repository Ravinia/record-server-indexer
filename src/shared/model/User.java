package shared.model;

/** 
* Each User object contains the information for a single user of the system.
*/
public class User {
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private int indexedRecords;
	private String currentImage;
	
	public User()
	{
		setUserName(null);
		setPassword(null);
		setFirstName(null);
		setLastName(null);
		setEmail(null);
		setIndexedRecords(-1);
		setCurrentImage(null);
	}
	
	public User(String userName, String password, String firstName, String lastName,
			String email, int indexedRecords, String currentImage)
	{
		setUserName(userName);
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setEmail(email);
		setIndexedRecords(indexedRecords);
		setCurrentImage(currentImage);
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getIndexedRecords() {
		return indexedRecords;
	}
	public void setIndexedRecords(int indexedRecords) {
		this.indexedRecords = indexedRecords;
	}
	public String getCurrentImage() {
		return currentImage;
	}
	public void setCurrentImage(String currentImage) {
		this.currentImage = currentImage;
	}
}
