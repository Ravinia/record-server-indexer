package shared.model;

/** 
* Each Image object contains information about one of the project's images.
*/
public class Image {
	private int id;
	private int project_id;
	private String file;
	private boolean isAvailable;
	
	public Image()
	{
		setId(-1);
		setProject_id(-1);
		setFile(null);
		setAvailable(false);
	}
	
	public Image(int id, int project_id, String file, boolean isAvailable)
	{
		setId(id);
		setProject_id(project_id);
		setFile(file);
		setAvailable(isAvailable);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public boolean isAvailable() {
		return isAvailable;
	}
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
}
