package shared.model;

/** 
* Each Field object contains the information about one of the project's fields.
*/
public class Field {
	private int id;
	private String title;
	private int xcoord;
	private int width;
	private int fieldNumber;
	private int project_id;
	private String helpHtml;
	private String knownData;
	
	public Field()
	{
		setId(-1);
		setTitle(null);
		setXcoord(-1);
		setWidth(-1);
		setFieldNumber(-1);
		setProject_id(-1);
		setHelpHtml(null);
		setKnownData(null);
	}
	
	public Field(int id, String title, int xcoord, int width, int fieldNumber, int project_id, String knownData, String helpHtml)
	{
		setId(id);
		setTitle(title);
		setXcoord(xcoord);
		setWidth(width);
		setHelpHtml(helpHtml);
		setKnownData(knownData);
		setFieldNumber(fieldNumber);
		setProject_id(project_id);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getXcoord() {
		return xcoord;
	}
	public void setXcoord(int xcoord) {
		this.xcoord = xcoord;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public String getHelpHtml() {
		return helpHtml;
	}
	public void setHelpHtml(String helpHtml) {
		this.helpHtml = helpHtml;
	}
	public String getKnownData() {
		return knownData;
	}
	public void setKnownData(String knownData) {
		this.knownData = knownData;
	}
	public int getFieldNumber() {
		return fieldNumber;
	}
	public void setFieldNumber(int fieldNumber) {
		this.fieldNumber = fieldNumber;
	}
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
}
