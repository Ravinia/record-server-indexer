package importer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.parsers.*;

import org.w3c.dom.*;

import server.dao.*;
import shared.model.CellValue;
import shared.model.Field;
import shared.model.Image;
import shared.model.Project;
import shared.model.User;

public class DataImporter {

	private Database db;
	static int current_field_id = 1;
	static int image_id = 1;
	
	public DataImporter(){
		
		// Load database driver	
		Database.loadDatabaseDriver();
		
		db = new Database();
		try {
			db.wipeDatabase();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		db.openDatabaseConnection();
	}
	
	public void wrapUp()
	{
		try {
			db.closeDatabaseConnection(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Document initializeDoc(Path folderName)
	{
		current_field_id = 1;
		DocumentBuilder builder;
		try {
			builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return null;
		}

		Path folder = folderName;
		File[] files = folder.toFile().listFiles();
		
		File file = null;

		Path parent = folder.getParent();
		Path relative;
		if (parent != null)
			relative = parent.relativize(folder);
		else
			relative = folder;
//		System.out.println("folder: " + folder);
//		System.out.println("parent: " + parent);
//		System.out.println("relative: " + relative);
		
		String expectedFileName = relative.getFileName() + ".xml";
//		System.out.println("expectedFileName: " + expectedFileName);
		for (File focus : files)
		{
//			System.out.println("Trying: " + focus.getName());
			if (focus.isFile())
				if (focus.getName().equals(expectedFileName))
					file = focus;
		}
		if (file == null)
		{
			System.out.println("ERROR: proper xml file not found");
			return null;
		}
		
		Document doc;
		try {
			doc = (Document) builder.parse(file);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return doc;
	}
	
	public boolean importUsers(Document doc)
	{
		UserAccess userAccess = new UserAccess();
		NodeList userList = doc.getElementsByTagName("user");
		for (int i = 0; i < userList.getLength(); i++)
		{	
			Element userElem = (Element)userList.item(i);
			
			Element usernameElem = (Element)userElem.getElementsByTagName("username").item(0);
			Element passwordElem = (Element)userElem.getElementsByTagName("password").item(0);
			Element firstnameElem = (Element)userElem.getElementsByTagName("firstname").item(0);
			Element lastnameElem = (Element)userElem.getElementsByTagName("lastname").item(0);
			Element emailElem = (Element)userElem.getElementsByTagName("email").item(0);
			Element indexedrecordsElem = (Element)userElem.getElementsByTagName("indexedrecords").item(0);
			
			String username = usernameElem.getTextContent();
			String password = passwordElem.getTextContent();
			String firstname = firstnameElem.getTextContent();
			String lastname = lastnameElem.getTextContent();
			String email = emailElem.getTextContent();
			int indexedrecords = Integer.parseInt(indexedrecordsElem.getTextContent());
			
			User user = new User(username, password, firstname, lastname, email, indexedrecords, null);
			try {
				userAccess.insert(user);
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	public boolean importProjects(Document doc)
	{
		ProjectAccess projectAccess = new ProjectAccess();
		NodeList projectList = doc.getElementsByTagName("project");
		for (int i = 0; i < projectList.getLength(); i++)
		{	
			Element projectElem = (Element)projectList.item(i);
			Element titleElem = (Element)projectElem.getElementsByTagName("title").item(0);
			Element recordsPerImageElem = (Element)projectElem.getElementsByTagName("recordsperimage").item(0);
			Element firstYCoordElem = (Element)projectElem.getElementsByTagName("firstycoord").item(0);
			Element recordHeightElem = (Element)projectElem.getElementsByTagName("recordheight").item(0);
			
			int id = i+1;
			String title = titleElem.getTextContent();
			int recordsPerImage = Integer.parseInt(recordsPerImageElem.getTextContent());
			int firstYCoord = Integer.parseInt(firstYCoordElem.getTextContent());
			int recordHeight = Integer.parseInt(recordHeightElem.getTextContent());
			
			Project project = new Project(id, title, recordsPerImage, firstYCoord, recordHeight);
			if (!importFields(projectElem.getElementsByTagName("fields"), id))
				return false;
			if (!importImages(projectElem.getElementsByTagName("images"), id))
				return false;
			try {
				projectAccess.insert(project);
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	public boolean importFields(NodeList fields, int project_id)
	{
		FieldAccess fieldAccess = new FieldAccess();
		NodeList fieldList = ((Element)fields.item(0)).getElementsByTagName("field");
		for (int i = 0; i < fieldList.getLength(); i++)
		{	
			Element fieldElem = (Element)fieldList.item(i);
			Element titleElem = (Element)fieldElem.getElementsByTagName("title").item(0);
			Element xcoordElem = (Element)fieldElem.getElementsByTagName("xcoord").item(0);
			Element widthElem = (Element)fieldElem.getElementsByTagName("width").item(0);
			Element helpHtmlElem = (Element)fieldElem.getElementsByTagName("helphtml").item(0);
			
			String knownData;
			NodeList knownDataList = fieldElem.getElementsByTagName("knowndata");
			if (knownDataList.getLength() == 0)
			{
				knownData = "null";
			}
			else
			{
				Element knownDataElem = (Element)knownDataList.item(0);
				knownData = knownDataElem.getTextContent();
			}
			
			int number = i+1;
			String title = titleElem.getTextContent();
			int xcoord = Integer.parseInt(xcoordElem.getTextContent());
			int width = Integer.parseInt(widthElem.getTextContent());
			String helpHtml = helpHtmlElem.getTextContent();
			
//			public Field(int id, String title, int xcoord, int width,
//			int fieldNumber, int project_id, String knownData, String helpHtml)
			
//			System.out.println(current_field_number);
//			System.out.println(((Element)fieldElem.getElementsByTagName("images")));
			Field field = new Field(current_field_id, title, xcoord, width, number, project_id, knownData, helpHtml);
			try {
				fieldAccess.insert(field);
				current_field_id++;
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	public boolean importImages(NodeList images, int project_id)
	{
		ImageAccess imageAccess = new ImageAccess();
		NodeList imageList = ((Element)images.item(0)).getElementsByTagName("image");
		for (int i = 0; i < imageList.getLength(); i++)
		{	
			Element imageElem = (Element)imageList.item(i);
			Element titleElem = (Element)imageElem.getElementsByTagName("file").item(0);
			
			
			String file = titleElem.getTextContent();
			
//			public Image(int id, int project_id, String file, boolean isAvailable)
			
			Image image = new Image(image_id, project_id, file, true);
			if (!importRecords(imageElem.getElementsByTagName("records"), project_id, image_id))
				return false;
			try {
				imageAccess.insert(image);
			} catch (SQLException e) {
				e.printStackTrace();
				return false;
			}
			image_id++;
		}
		return true;
	}
	
	public boolean importRecords(NodeList records, int project_id, int image_id)
	{
		if ((Element)records.item(0) == null)
			return true;
		
		CellValueAccess cellValueAccess = new CellValueAccess();
		NodeList recordList = ((Element)records.item(0)).getElementsByTagName("record");
		for (int i = 0; i < recordList.getLength(); i++)
		{	
			Element recordElem = (Element)recordList.item(i);
			NodeList values = recordElem.getElementsByTagName("values");
			
			NodeList valueList = ((Element)values.item(0)).getElementsByTagName("value");
			for (int j = 0; j < valueList.getLength(); j++)
			{
				ResultSet rs = null;
				PreparedStatement stmt = null;
				
				int field_id = -1;
				try {
					String sql = "select id from field where field_number = ? and project_id = ?";
					stmt = Database.connection.prepareStatement(sql);
					stmt.setInt(1, j+1);
					stmt.setInt(2, project_id);
				    rs = stmt.executeQuery();
				    
				    if (rs.next())
				    	field_id = rs.getInt(1);
				    
				    if (rs != null) rs.close();
				    if (stmt != null) stmt.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String str = ((Element)valueList.item(j)).getTextContent();
				
				//	CellValue(int image_id, int project_id, int field_id, String str, int recordNumber)
				
				CellValue cellValue = new CellValue(image_id, project_id, field_id, str, i+1);
				try {
					cellValueAccess.insert(cellValue);
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
		return true;
	}

	public void importData(Path folderName)
	{
		Document doc = initializeDoc(folderName);
		
		if (importUsers(doc) && importProjects(doc))
			System.out.println("Data has been imported successfully");
	}
	
	public void importFiles(Path folderName) throws IOException
	{
		// Delete data
		Path oldDataFolder = Paths.get("database");
		File[] oldFiles = oldDataFolder.toFile().listFiles();
		Path fieldHelp = null;
		Path images = null;
		Path knownData = null;
		for (File focus : oldFiles)
		{
			if (focus.isDirectory())
			{
//				System.out.println("Deleting from: " + focus.getName());
				if (focus.getName().equals("fieldhelp"))
					fieldHelp = focus.toPath();
				else if (focus.getName().equals("images"))
					images = focus.toPath();
				else if (focus.getName().equals("knowndata"))
					knownData = focus.toPath();
				
				if (focus.getName().equals("fieldhelp") || focus.getName().equals("images")
						|| focus.getName().equals("knowndata"))
				{
					File[] innerFiles = focus.listFiles();
					for (File focus2 : innerFiles)
					{
						focus2.delete();
					}
				}
			}
		}
		
		// Copy data
		Path newDataFolder = folderName;
		File[] newFiles = newDataFolder.toFile().listFiles();
//		System.out.println(fieldHelp);
//		System.out.println(images);
//		System.out.println(knownData);
		for (File focus : newFiles)
		{
			if (focus.isDirectory() && (focus.getName().equals("fieldhelp") || focus.getName().equals("images")
					|| focus.getName().equals("knowndata")))
			{
//				System.out.println(focus.getName());
				File[] innerFiles = focus.listFiles();
				for (File focus2 : innerFiles)
				{
//					System.out.println(focus2.getName());
					if (focus.getName().equals("fieldhelp"))
						Files.copy(focus2.toPath(), fieldHelp.resolve(focus2.getName()));
					else if (focus.getName().equals("images"))
						Files.copy(focus2.toPath(), images.resolve(focus2.getName()));
					else if (focus.getName().equals("knowndata"))
						Files.copy(focus2.toPath(), knownData.resolve(focus2.getName()));
				}
			}
		}
	}
	
	public static void main(String[] args)
	{
		if (args.length != 1)
		{
			System.out.println("Usage: accepts a single command-line argument,"
					+ "which is the path (absolute or relative) to the XML file for the imported data set.");
			return;
		}
		
		File xmlFile = new File(args[0]);
		Path folder = xmlFile.getParentFile().toPath();
		
		DataImporter dataImporter = new DataImporter();
		dataImporter.importData(folder);
		try {
			dataImporter.importFiles(folder);
		} catch (IOException e) {
			System.out.println("ERROR: File import failure");
			e.printStackTrace();
		}
		dataImporter.wrapUp();
	}
	
}
