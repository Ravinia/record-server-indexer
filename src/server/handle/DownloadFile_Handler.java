package server.handle;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;


public class DownloadFile_Handler implements HttpHandler
{

//	1) Extracts the requested URL from the HTTP GET request (i.e., from the HttpExchange)
//	2) Maps the Path portion of the requested URL to the path of the requested file in the Server's file system.
//	3) Opens the requested file, and passes back the contents (i.e., bytes)
//	of the file back to the client through the HTTP response body (i.e., through the HttpExchange)
	
	private static Logger logger;
	public DownloadFile_Handler()
	{
		logger = Logger.getLogger("servermanager"); 
	}
	

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		
		logger.info("handling download file");
	    
		String pathString = exchange.getRequestURI().getPath().toString().substring(1);
	    logger.info("path: "+pathString);
	    Path path = Paths.get(pathString);

	    byte[] bytes = Files.readAllBytes(path);
	    
	    logger.info("bytes: "+bytes);
	    
	    Headers resHeaders = exchange.getResponseHeaders();
	    resHeaders.add ( "Content-Type", "text/plain" );
	    exchange.sendResponseHeaders ( 200, bytes.length );
	
	    OutputStream os = exchange.getResponseBody();
	    os.write (bytes);
	    os.close();
	
	    logger.info( "Completed!" );
	}
}
