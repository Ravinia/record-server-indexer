package server.handle;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.logging.Logger;

import server.dao.Database;
import server.facade.Facade;
import shared.communication.ValidateUser_Params;
import shared.communication.ValidateUser_Return;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class ValidateUser_Handler implements HttpHandler{
	
	private static Logger logger;
	public ValidateUser_Handler()
	{
		logger = Logger.getLogger("servermanager"); 
	}
	

	@Override
	public void handle(HttpExchange exchange) throws IOException {
		
		logger.info("handling validateUser");
	    
	    InputStream s = exchange.getRequestBody();
	
		// Deserialize
	    XStream xStream = new XStream(new DomDriver());
		InputStream inFile = new BufferedInputStream(s);
		
		ValidateUser_Params params = (ValidateUser_Params)xStream.fromXML(inFile);
		inFile.close();
		
		logger.info("getting the return value: ");
		
		logger.info("params: " + params.toString());
		// Get return value
		Database db = new Database();
		db.openDatabaseConnection();
		Facade facade = new Facade();
		ValidateUser_Return ret = null;
		try {
			ret = facade.validateUser(params);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			db.closeDatabaseConnection(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if (ret != null)
			logger.info("ret: " + ret.toString());
		
		// Serialize
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		OutputStream outFile = new BufferedOutputStream(bos);  
		xStream.toXML(ret, outFile);
		outFile.close();
		byte[] bytes = bos.toByteArray();
	
	    // Set response headers explicitly
	    Headers resHeaders = exchange.getResponseHeaders ();
	    resHeaders.add ( "Content-Type", "text/plain" );
	    exchange.sendResponseHeaders ( 200, bytes.length );
	
	    OutputStream os = exchange.getResponseBody();
	    os.write (bytes);
	    os.close();
	
	    System.out.println ( "Completed!" );
	}
}
