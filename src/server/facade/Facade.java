package server.facade;

import java.net.MalformedURLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeSet;

import server.dao.CellValueAccess;
import server.dao.Database;
import server.dao.ImageAccess;
import server.dao.UserAccess;
import shared.communication.*;
import shared.model.CellValue;
import shared.model.Field;
import shared.model.Image;
import shared.model.Project;
import shared.model.User;

/** 
* Performs operations involving the database as instructed by the client communicator.
*/
public class Facade {
	
	/**
	* Validates user credentials
	*/
	public ValidateUser_Return validateUser(ValidateUser_Params params) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ValidateUser_Return ret = null;
		try
		{
			ret = new ValidateUser_Return();
			String sql = "select * from user where username = ? and password = ?";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setString(1, params.getName());
			stmt.setString(2, params.getPassword());
		    rs = stmt.executeQuery();
		    
		    if (!rs.next())
		    	ret.setSucceeds(false);
		    else
		    {
			    ret.setUser_first_name(rs.getString(3));
				ret.setUser_last_name(rs.getString(4));
				ret.setNum_records(rs.getInt(6));
				
				ret.setSucceeds(true);
		    }
		}
		finally
		{
		    if (rs != null) rs.close();
		    if (stmt != null) stmt.close();
		}
		
		return ret;
	}
	
	/**
	* Returns information about all of the available projects
	*/
	public GetProjects_Return getProjects(GetProjects_Params params) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		GetProjects_Return ret = new GetProjects_Return();
		
		ValidateUser_Params vparams = new ValidateUser_Params(params.getUser(), params.getPassword());
		if (validateUser(vparams).getSucceeds())
		{
			try
			{
				String sql = "select id, title from project";
				
//				if (Database.connection == null)
//					System.out.println("its null");
				
				stmt = Database.connection.prepareStatement(sql);
			    rs = stmt.executeQuery();
			    ArrayList<Project> list = new ArrayList<Project>();
			    
			    while (rs.next())
			    {
			    	Project project = new Project();
			    	project.setId(rs.getInt(1));
			    	project.setTitle(rs.getString(2));
				    list.add(project);
			    }
			    ret.setProjects(list);
			    ret.setSucceeds(true);
			}
			finally
			{
			    if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
			}
		}
		
		return ret;
	}

	/**
	* Returns a sample image for the specified project
	*/
	public GetSampleImage_Return getSampleImage(GetSampleImage_Params params) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		GetSampleImage_Return ret = new GetSampleImage_Return();
		
		ValidateUser_Params vparams = new ValidateUser_Params();
		vparams.setName(params.getUser());
		vparams.setPassword(params.getPassword());
		if (validateUser(vparams).getSucceeds())
		{
			try
			{
				String sql = "select file from image where project_id = ?";
				stmt = Database.connection.prepareStatement(sql);
			    stmt.setInt(1, params.getProject());
			    rs = stmt.executeQuery();
			    
			    if (rs.next())
			    {	
			    	ret.setImage_url(rs.getString(1));
			    	ret.setSucceeds(true);
		    	}
			}
			finally
			{
			    if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
			}
		}
		
		return ret;
	}

	/**
	* Downloads a batch for the user to index
	*/
	public DownloadBatch_Return downloadBatch(DownloadBatch_Params params) throws SQLException, MalformedURLException
	{		
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		ResultSet userRS = null;
		DownloadBatch_Return ret = new DownloadBatch_Return();
		
		ValidateUser_Params vparams = new ValidateUser_Params();
		vparams.setName(params.getUser());
		vparams.setPassword(params.getPassword());
		if (validateUser(vparams).getSucceeds())
		{
			try
			{
				// Make sure the user doesn't already have an image
				String sql = "select * from user where username = ? and password = ?";
				stmt = Database.connection.prepareStatement(sql);
				stmt.setString(1, params.getUser());
				stmt.setString(2, params.getPassword());
			    userRS = stmt.executeQuery();
			    
			    boolean valid = false;
			    userRS.next();
		    	if (userRS.getString(7) == null)
		    	{
		    		valid = true;
		    	}
			    
			    if (valid)
			    {
			    	// check the image's availability
				    if (stmt != null) stmt.close();
			    	sql = "select * from image where is_available = ? and project_id = ?";
					stmt = Database.connection.prepareStatement(sql);
					stmt.setBoolean(1, true);
					stmt.setInt(2, params.getProject());
				    rs = stmt.executeQuery();
				    
//				    System.out.println("at least valid");
				    if (rs.next())
				    {
				    	ret.setBatch_id(rs.getInt(1));
				    	ret.setProject_id(params.getProject());
				    	ret.setImage_url(rs.getString(3));
				    	
				    	// Get info about project
				    	sql = "select * from project where id = ?";
						stmt = Database.connection.prepareStatement(sql);
						stmt.setInt(1, params.getProject());
					    rs2 = stmt.executeQuery();
					    if (rs2.next())
					    {
						    ret.setFirst_y_coord(rs2.getInt(4));
						    ret.setRecord_height(rs2.getInt(5));
						    ret.setNum_records(rs2.getInt(3));
					    }
							
					    if (rs2 != null) rs2.close();
					    if (stmt != null) stmt.close();
						sql = "select * from field where project_id = ?";
						stmt = Database.connection.prepareStatement(sql);
						stmt.setInt(1, params.getProject());
						rs2 = stmt.executeQuery();

						ArrayList<Field> fields = new ArrayList<Field>();
						while (rs2.next())
						{
							Field field = new Field();
							
							field.setId(rs2.getInt(1));
							field.setTitle(rs2.getString(2));
							field.setXcoord(rs2.getInt(3));
							field.setWidth(rs2.getInt(4));
							field.setFieldNumber(rs2.getInt(5));
							field.setProject_id(rs.getInt(2)); //supposed to be rs not rs2
							
							field.setKnownData(rs2.getString(7));
							
							field.setHelpHtml(rs2.getString(8));
							fields.add(field);
							
//							System.out.println("adding field: " + field.getTitle());
						}
						ret.setFields(fields);
						ret.setNum_fields(fields.size());
						
						//public Image(int id, int project_id, String file, boolean isAvailable)
						// Make the image not available
						Image updatedImage = new Image(ret.getBatch_id(), ret.getProject_id(), ret.getImage_url().toString()
								, false);
						ImageAccess imageAccess = new ImageAccess();
						imageAccess.update(updatedImage);
						
						//public User(String userName, String password, String firstName, String lastName,
						//String email, int indexedRecords, String currentImage)
						// Assign the image to the user
						if (userRS != null) userRS.close();
					    if (stmt != null) stmt.close();
						sql = "select * from user where username = ? and password = ?";
						stmt = Database.connection.prepareStatement(sql);
						stmt.setString(1, params.getUser());
						stmt.setString(2, params.getPassword());
					    userRS = stmt.executeQuery();
						User updatedUser = new User(userRS.getString(1), userRS.getString(2), userRS.getString(3),
						userRS.getString(4), userRS.getString(5), userRS.getInt(6), ret.getImage_url().toString());
						UserAccess userAccess = new UserAccess();
						userAccess.update(updatedUser);

				    	ret.setSucceeds(true);
				    }
				    else
				    {
				    	System.out.println("no more batches from that projects");
				    }
			    }
			    else
			    {
			    	System.out.println("user already has batch: " + userRS.getString(7));
			    }
			}
			finally
			{
				if (rs != null) rs.close();
				if (rs2 != null) rs2.close();
				if (userRS != null) userRS.close();
			    if (stmt != null) stmt.close();
			}
		}
		
		return ret;
	}

	/**
	* Submits the indexed record field values for a batch to the Server
	*/
	public SubmitBatch_Return submitBatch(SubmitBatch_Params params) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		SubmitBatch_Return ret = new SubmitBatch_Return();
		
		UserAccess userAccess = new UserAccess();
		ValidateUser_Params vparams = new ValidateUser_Params();
		vparams.setName(params.getUser());
		vparams.setPassword(params.getPassword());
		if (validateUser(vparams).getSucceeds())
		{
			try
			{
				
				String sql = "select file, project_id from image where id = ?";
				stmt = Database.connection.prepareStatement(sql);
				stmt.setInt(1, params.getBatch());
				rs = stmt.executeQuery();
				String imageFile = "";
				int project_id = -1;
				if (!rs.next())
				{
					System.out.println("SUBMIT ERROR: bad image id");
					return ret;
				}
				else
				{
					imageFile = rs.getString(1);
					project_id = rs.getInt(2);
				}
				
				if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
				sql = "select recordsperimage from project where id = ?";
				stmt = Database.connection.prepareStatement(sql);
			    stmt.setInt(1, project_id);
			    rs = stmt.executeQuery();
			    if (!rs.next())
			    	return ret;
			    // See if there are the right number of records
			    if (Integer.parseInt(rs.getString(1)) != params.getField_values().size())
			    {
			    	System.out.println("SUBMIT ERROR: wrong number of records");
			    	System.out.println("expected: "+rs.getString(1)+"actual:"+params.getField_values().size());
			    	return ret;
			    }
			    
			    if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
				sql = "select id from field where project_id = ?";
				stmt = Database.connection.prepareStatement(sql);
			    stmt.setInt(1, project_id);
			    rs = stmt.executeQuery();
			    int fields = 0;
			    while(rs.next())
			    	fields++;
			    // See if there are the right number of items in each record
			    for (int i=0; i<params.getField_values().size(); i++)
			    {
			    	if (params.getField_values().get(i).size() != fields)
			    	{
			    		System.out.println("SUBMIT ERROR: wrong number of items in record " + i);
			    		System.out.println(params.getField_values().get(i).size() + " != " + fields);
			    		return ret;
			    	}
			    }
			    
				if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
				sql = "select * from user where username = ?";
				stmt = Database.connection.prepareStatement(sql);
			    stmt.setString(1, params.getUser());
			    rs = stmt.executeQuery();
			    rs.next();
			    // See if user owns the submitted batch
			    if (!rs.getString(7).equals(imageFile))
			    {
			    	System.out.println(rs.getString(7) + " != " + imageFile);
			    	System.out.println("SUBMIT ERROR: user doesn't own the submitted batch");
			    	return ret;
			    }
			    
			    // Update user
			    User user = new User();
			    user.setUserName(rs.getString(1));
			    user.setPassword(rs.getString(2));
			    user.setFirstName(rs.getString(3));
			    user.setLastName(rs.getString(4));
			    user.setEmail(rs.getString(5));
			    user.setIndexedRecords(rs.getInt(6) + params.getField_values().size()); // Increment indexed records
			    user.setCurrentImage(null); // Un-assign the current image
			    
			    if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
			    userAccess.update(user);
			    
			    // Submit batch
			    CellValueAccess cellValueAccess = new CellValueAccess();
			    for (int i=0; i<params.getField_values().size(); i++)
			    {
					for (int j=0; j<params.getField_values().get(i).size(); j++)
					{
						// Create the cell to insert
						CellValue cellValue = new CellValue();
						cellValue.setImage_id(params.getBatch());
						cellValue.setField_id(j+1);
						cellValue.setStr(params.getField_values().get(i).get(j));
						
						cellValue.setProject_id(project_id);
						
						// Retrieve the appropriate record number
						if (rs != null) rs.close();
					    if (stmt != null) stmt.close();
						sql = "select record_number from cellvalue where field_id = ? and image_id = ? and project_id = ?";
						stmt = Database.connection.prepareStatement(sql);
						stmt.setInt(1, cellValue.getField_id());
						stmt.setInt(2, cellValue.getImage_id());
						stmt.setInt(3, project_id);
						rs = stmt.executeQuery();
						TreeSet<Integer> indexes = new TreeSet<Integer>();
						while (rs.next())
						{
//							System.out.println("there was one: " + (rs.getInt(1)+1));
							indexes.add(rs.getInt(1)+1);
						}
						if (indexes.size() == 0)
							cellValue.setRecordNumber(1);
						else
						{
							cellValue.setRecordNumber(indexes.last());
//							System.out.println(indexes.last());
						}
						
						if (rs != null) rs.close();
					    if (stmt != null) stmt.close();
						cellValueAccess.insert(cellValue);
//						System.out.println("inserting: " + cellValue.getStr());
					}
			    }
			    
				ret.setSucceeds(true);
			}
			finally
			{
			    if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
			}
		}
		return ret;
	}

	/**
	* Returns information about all of the fields for the specified project
	* If no project is specified, returns information about all of the fields for all projects in the system
	*/
	public GetFields_Return getFields(GetFields_Params params) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		GetFields_Return ret = new GetFields_Return();
		
		ValidateUser_Params vparams = new ValidateUser_Params();
		vparams.setName(params.getUser());
		vparams.setPassword(params.getPassword());
		if (validateUser(vparams).getSucceeds())
		{
			try
			{
//				if (db == null)
//					db = new Database();
				
				String sql;
				if (params.getProject() == -1)
				{
					sql = "select id, title, project_id from field";
					stmt = Database.connection.prepareStatement(sql);
				}
				else
				{
					sql = "select id, title, project_id from field where project_id = ?";
					stmt = Database.connection.prepareStatement(sql);
					stmt.setInt(1, params.getProject());
				}
				rs = stmt.executeQuery();
			    ArrayList<Field> fields = new ArrayList<Field>();
			    
			    while (rs.next())
			    {			    	
			    	Field field = new Field();
			    	field.setId(rs.getInt(1));
			    	field.setTitle(rs.getString(2));
			    	field.setProject_id(rs.getInt(3));
			    	fields.add(field);
			    }
			    ret.setFields(fields);
			    
			    if (fields.size() > 0)
			    	ret.setSucceeds(true);
			}
			finally
			{
			    if (rs != null) rs.close();
			    if (stmt != null) stmt.close();
			}
		}
		
		return ret;
	}

	/**
	* Searches the indexed records for the specified strings
	*/
	public Search_Return search(Search_Params params) throws SQLException, MalformedURLException
	{
		System.out.println("SEARCHING THIS FAR");
		
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		// Validate user
		PreparedStatement stmt = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		Search_Return ret = new Search_Return();
		
		ValidateUser_Params vparams = new ValidateUser_Params();
		vparams.setName(params.getUser());
		vparams.setPassword(params.getPassword());
		if (validateUser(vparams).getSucceeds())
		{
			System.out.println("USER VALIDATED");
			try
			{
				ArrayList<Search_Return.SearchResult> sr = new ArrayList<Search_Return.SearchResult>();
				
				for (Integer i : params.getFields())
				{
//				    System.out.println("CONTINUTING TO PERFORM SEARCH");   
					for (String s : params.getSearch_values())
					{   
						// perform the search
					    if (rs != null) rs.close();
					    if (stmt != null) stmt.close();
//					    System.out.println("field_id="+i+" str="+s.toLowerCase());
					    String sql = "select * from cellvalue where field_id = ? and str = ?";
						stmt = Database.connection.prepareStatement(sql);
						stmt.setInt(1, i);
						stmt.setString(2, s.toLowerCase());
					    rs = stmt.executeQuery();
					    
					    while (rs.next())
					    {
					    	System.out.println("A RESULT");
					    	// get the  image url and add it to the sr array
					    	Search_Return.SearchResult searchResult = ret.new SearchResult();
					    	searchResult.setBatch_id(rs.getInt(1));
					    	
					    	sql = "select * from image where id = ? and project_id = ?";
							stmt = Database.connection.prepareStatement(sql);
							stmt.setInt(1, searchResult.getBatch_id());
							stmt.setInt(2, rs.getInt(2));
						    rs2 = stmt.executeQuery();
//						    System.out.println("here");
						    if (rs2.next())
						    {
						    	System.out.println("SETTING IMAGE URL TO: "+rs2.getString(3));
//						    	System.out.println("within");
						    	searchResult.setImage_url(rs2.getString(3));
						    	
						    	searchResult.setRecord_num(rs.getInt(5));
						    	searchResult.setField_id(i);
//							    	rs.getInt(3) - used to be ^
						    	
						    	sr.add(searchResult);
						    }
					    }
					}
				}
			    ret.setSearchResults(sr);
			    
			    if (sr.size() > 0)
			    {
			    	ret.setSucceeds(true);
			    	System.out.println("PUSHING RESULT(S)");
			    }
			}
			finally
			{
				if (rs != null) rs.close();
				if (rs2 != null) rs2.close();
			    if (stmt != null) stmt.close();
			}
		}
		
		return ret;
	}
	
//	/**
//	* Downloads a file for the user
//	*/
//	public DownloadFile_Return DownloadFile(DownloadFile_Params params){
//		return null;
//	}
}
