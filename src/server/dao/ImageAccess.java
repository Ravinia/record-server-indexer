package server.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import shared.model.Image;

/** 
* Handles any image access within the database.
*/
public class ImageAccess{
	
	public void insert(Image image) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "insert into image " +
			"(id, project_id, file, is_available) values (?, ?, ?, ?)";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setInt(1, image.getId());
			stmt.setInt(2, image.getProject_id());
			stmt.setString(3, image.getFile());
			stmt.setBoolean(4, image.isAvailable());
			stmt.executeUpdate();
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	/*
	 * Updates a image within the database
	 */
	public void update(Image image) throws SQLException
	{	
		PreparedStatement stmt = null;
		try
		{
			String sql = "update image " +
			"set file = ?, is_available = ? where id = ? and project_id = ?";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setString(1, image.getFile());
			stmt.setBoolean(2, image.isAvailable());
			stmt.setInt(3, image.getId());
			stmt.setInt(4, image.getProject_id());
			stmt.executeUpdate();
		} 
		finally {
			if (stmt != null)
				stmt.close(); 
		}
	}
	
	public ArrayList<Image> getAll() throws SQLException
	{	
		ArrayList<Image> list = new ArrayList<Image>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			String sql = "select * from image";
			stmt = Database.connection.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				Image image = new Image();
				image.setId(rs.getInt(1));
				image.setProject_id(rs.getInt(2));
				image.setFile(rs.getString(3));
				image.setAvailable(rs.getBoolean(4));
				list.add(image);
			}
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
		return list;
	}
}
