package server.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import shared.model.Project;

/** 
* Handles any project access within the database.
*/
public class ProjectAccess{

	public void insert(Project project) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "insert into project " +
			"(id, title, recordsperimage, firstycoord, recordheight) values (?, ?, ?, ?, ?)";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setInt(1, project.getId());
			stmt.setString(2, project.getTitle());
			stmt.setInt(3, project.getRecordsPerImage());
			stmt.setInt(4, project.getFirstYCoord());
			stmt.setInt(5, project.getRecordHeight());
			stmt.executeUpdate();
		}
		finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	public ArrayList<Project> getAll() throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		ArrayList<Project> list = new ArrayList<Project>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			String sql = "select * from project";
			stmt = Database.connection.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				Project project = new Project();
				project.setId(rs.getInt(1));
				project.setTitle(rs.getString(2));
				project.setRecordsPerImage(rs.getInt(3));
				project.setFirstYCoord(rs.getInt(4));
				project.setRecordHeight(rs.getInt(5));
				list.add(project);
			}
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
		return list;
	}
}
