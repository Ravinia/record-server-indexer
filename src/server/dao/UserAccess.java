package server.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import shared.model.User;

/** 
* Handles any user access within the database.
*/
public class UserAccess{

	public void insert(User user) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "insert into user " +
			"(username, password, firstname, lastname, email, indexedrecords, current_image) values (?, ?, ?, ?, ?, ?, ?)";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setString(1, user.getUserName());
			stmt.setString(2, user.getPassword());
			stmt.setString(3, user.getFirstName());
			stmt.setString(4, user.getLastName());
			stmt.setString(5, user.getEmail());
			stmt.setInt(6, user.getIndexedRecords());
			stmt.setString(7, user.getCurrentImage());
			stmt.executeUpdate();
		} 
		finally {
			if (stmt != null)
				stmt.close(); 
		}
	}
	
	/*
	 * Updates a user within the database
	 */
	public void update(User user) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "update user " +
			"set firstname = ?, lastname = ?, email = ?, indexedrecords = ?, current_image = ?" +
			"where username = ? and password = ?";
			stmt = Database.connection.prepareStatement(sql);
			
			stmt.setString(1, user.getFirstName());
			stmt.setString(2, user.getLastName());
			stmt.setString(3, user.getEmail());
			stmt.setInt(4, user.getIndexedRecords());
			stmt.setString(5, user.getCurrentImage());
			stmt.setString(6, user.getUserName());
			stmt.setString(7, user.getPassword());
			stmt.executeUpdate();
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	public ArrayList<User> getAll() throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		ArrayList<User> list = new ArrayList<User>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			String sql = "select * from user";
			stmt = Database.connection.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				User user = new User();
				user.setUserName(rs.getString(1));
				user.setPassword(rs.getString(2));
				user.setFirstName(rs.getString(3));
				user.setLastName(rs.getString(4));
				user.setEmail(rs.getString(5));
				user.setIndexedRecords(rs.getInt(6));
				user.setCurrentImage(rs.getString(7));
				list.add(user);
			}
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
		return list;
	}
}
