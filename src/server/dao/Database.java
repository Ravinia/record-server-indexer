package server.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/*
 *  This class has a static connection object, there can only be one
 */
public class Database {
	public static Connection connection;
	//make connection static so there is only one of them
	
	public static void loadDatabaseDriver()
	{
		try
		{
			final String driver = "org.sqlite.JDBC";
			Class.forName(driver);
		} 
		catch(ClassNotFoundException e)
		{
			// ERROR! Could not load database driver
			e.printStackTrace();
		}
	}
	
	public void wipeDatabase() throws SQLException
	{
		Database.loadDatabaseDriver();
		openDatabaseConnection();
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			String sql = "delete from user";
			stmt = Database.connection.prepareStatement(sql);
		    stmt.execute();
		    stmt.close();
		    
		    sql = "delete from cellvalue";
			stmt = Database.connection.prepareStatement(sql);
			stmt.execute();
			stmt.close();
		    
		    sql = "delete from field";
			stmt = Database.connection.prepareStatement(sql);
			stmt.execute();
			stmt.close();
		    
			sql = "delete from image";
			stmt = Database.connection.prepareStatement(sql);
			stmt.execute();
			stmt.close();
		    
			sql = "delete from project";
			stmt = Database.connection.prepareStatement(sql);
		    stmt.execute();
		    stmt.close();
		    
			closeDatabaseConnection(true);
		}
		finally
		{
		    if (rs != null) rs.close();
		    if (stmt != null) stmt.close();
		    if (Database.connection != null)
		    	closeDatabaseConnection(false);
		}
	}
	
	public void openDatabaseConnection()
	{		
//		System.out.println("Attempting to open a connection");
		
		if (connection != null)
		{
			System.out.println("Attempted to open a second connection");
			return;
		}
		
		String dbName = "database" + File.separator + "indexer_server.sqlite";
		String connectionURL = "jdbc:sqlite:" + dbName;
		connection = null;
		
		try
		{
			// Open a database connection
			connection = DriverManager.getConnection(connectionURL);
			// Start a transaction
			connection.setAutoCommit(false);
		}
		catch (SQLException e)
		{
			System.out.println("Database opening failure");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		    System.exit(0);
		}
//		System.out.println("Opened database successfully");
	}
	
	public void closeDatabaseConnection(boolean succeeds) throws SQLException
	{
//		System.out.println("Attempting to close a connection");

		if (connection == null)
		{
			System.out.println("Attempted to close null connection");
			return;
		}
		
		try
		{
			if (succeeds /*ALL DATABASE OPERATIONS SUCCEEDED*/)
			{
				connection.commit();
			}
			else 
			{
		         connection.rollback();
			}
		}
		catch (SQLException e)
		{
			// ERROR
			e.printStackTrace();
		}
		finally
		{
		     connection.close();
		}
		connection = null;
		
//		System.out.println("Closed database successfully");
	}
	
}
