package server.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import shared.model.Field;

/** 
* Handles any field access within the database.
*/
public class FieldAccess{

	public void insert(Field field) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "insert into field " +
			"(id, title, xcoord, width, field_number, project_id, knowndata, helphtml) values (?, ?, ?, ?, ?, ?, ?, ?)";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setInt(1, field.getId());
			stmt.setString(2, field.getTitle());
			stmt.setInt(3, field.getXcoord());
			stmt.setInt(4, field.getWidth());
			stmt.setInt(5, field.getFieldNumber());
			stmt.setInt(6, field.getProject_id());
			stmt.setString(7, field.getKnownData());
			stmt.setString(8, field.getHelpHtml());
			stmt.executeUpdate();
		}
		finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	public ArrayList<Field> getAll() throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		ArrayList<Field> list = new ArrayList<Field>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			String sql = "select * from field";
			stmt = Database.connection.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				Field field = new Field();
				field.setId(rs.getInt(1));
				field.setTitle(rs.getString(2));
				field.setXcoord(rs.getInt(3));
				field.setWidth(rs.getInt(4));
				field.setFieldNumber(rs.getInt(5));
				field.setProject_id(rs.getInt(6));
				field.setKnownData(rs.getString(7));
				field.setHelpHtml(rs.getString(8));
				list.add(field);
			}
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
		return list;
	}
}
