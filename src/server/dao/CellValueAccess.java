package server.dao;

import shared.model.CellValue;

import java.sql.*;
import java.util.ArrayList;
import java.sql.ResultSet;

/** 
* Handles any cell value access within the database.
*/
public class CellValueAccess
{	
	public void insert(CellValue cellValue) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "insert into cellvalue " +
			"(image_id, project_id, field_id, str, record_number) values (?, ?, ?, ?, ?)";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setInt(1, cellValue.getImage_id());
			stmt.setInt(2, cellValue.getProject_id());
			stmt.setInt(3, cellValue.getField_id());
			stmt.setString(4, cellValue.getStr().toLowerCase());
			stmt.setInt(5, cellValue.getRecordNumber());
			stmt.executeUpdate();
		}
		finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	public void update(CellValue cellValue) throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return;
		}
		
		PreparedStatement stmt = null;
		try
		{
			String sql = "update cellvalue " +
			"set str = ? " +
			"where image_id = ? and project_id = ? and field_id = ? and record_number = ?";
			stmt = Database.connection.prepareStatement(sql);
			stmt.setString(1, cellValue.getStr().toLowerCase());
			stmt.setInt(2, cellValue.getImage_id());
			stmt.setInt(3, cellValue.getProject_id());
			stmt.setInt(4, cellValue.getField_id());
			stmt.setInt(5, cellValue.getRecordNumber());
			stmt.executeUpdate();
	
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
	}
	
	public ArrayList<CellValue> getAll() throws SQLException
	{
		if (Database.connection == null)
		{
			System.out.println("No connection to database");
			return null;
		}
		
		ArrayList<CellValue> list = new ArrayList<CellValue>();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try
		{
			String sql = "select * from cellvalue";
			stmt = Database.connection.prepareStatement(sql);
			rs = stmt.executeQuery();
			
			while (rs.next())
			{
				CellValue cellValue = new CellValue();
				cellValue.setImage_id(rs.getInt(1));
				cellValue.setProject_id(rs.getInt(2));
				cellValue.setField_id(rs.getInt(3));
				cellValue.setStr(rs.getString(4));
				cellValue.setRecordNumber(rs.getInt(5));
				list.add(cellValue);
			}
		}  
		finally {
			if (stmt != null)
				stmt.close();
		}
		return list;
	}
	
//	public void delete(CellValue cellValue) throws SQLException
//	{
//		if (db == null)
//			db = new Database();
//		db.openDatabaseConnection();
//		
//		PreparedStatement stmt = null;
//		try
//		{
//			String sql = "delete from cellvalue " +
//			"where image_id = ? and project_id = ? and field_id = ? and str = ? and record_number = ?";
//			stmt = Database.connection.prepareStatement(sql);
//			stmt.setInt(1, cellValue.getImage_id());
//			stmt.setInt(2, cellValue.getProject_id());
//			stmt.setInt(3, cellValue.getField_id());
//			stmt.setString(4, cellValue.getStr());
//			stmt.setInt(5, cellValue.getRecordNumber());
//			stmt.executeUpdate();
//			
//			db.closeDatabaseConnection(true);
//		}  
//		finally {
//			if (stmt != null)
//				stmt.close();
//			if (Database.connection != null)
//				db.closeDatabaseConnection(false);
//		}
//	}
}
