package server;

import java.io.*;
import java.net.*;
import java.util.logging.*;

import com.sun.net.httpserver.*;

import server.dao.Database;
import server.handle.*;

public class Server {

	private static final int MAX_WAITING_CONNECTIONS = 10;
	private static Logger logger;
	private int port;
	private HttpServer server;
	
	public Server(int port) {
		this.port = port;
	}
	
	static {
		try {
			initLog();
		}
		catch (IOException e) {
			System.out.println("Could not initialize log: " + e.getMessage());
		}
	}
	
	private static void initLog() throws IOException {
		
		Level logLevel = Level.ALL;
		
		logger = Logger.getLogger("servermanager"); 
		logger.setLevel(logLevel);
		logger.setUseParentHandlers(false);
		
		Handler consoleHandler = new ConsoleHandler();
		consoleHandler.setLevel(logLevel);
		consoleHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(consoleHandler);

		FileHandler fileHandler = new FileHandler("log.txt", false);
		fileHandler.setLevel(logLevel);
		fileHandler.setFormatter(new SimpleFormatter());
		logger.addHandler(fileHandler);
	}
	
	private void run() {
		
		logger.info("Initializing Database");
		
		Database.loadDatabaseDriver();		

		logger.info("Initializing HTTP Server");
		
		try {
			server = HttpServer.create(new InetSocketAddress(port), MAX_WAITING_CONNECTIONS);
		} 
		catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);			
			return;
		}

		server.setExecutor(null); // use the default executor
		
		server.createContext("/ValidateUser", new ValidateUser_Handler());
		server.createContext("/GetProjects", new GetProjects_Handler());
		server.createContext("/GetSampleImage", new GetSampleImage_Handler());
		server.createContext("/DownloadBatch", new DownloadBatch_Handler());
		server.createContext("/SubmitBatch", new SubmitBatch_Handler());
		server.createContext("/GetFields", new GetFields_Handler());
		server.createContext("/Search", new Search_Handler());
		server.createContext("/database", new DownloadFile_Handler());
		
		logger.info("Starting HTTP Server");
		logger.info("port: " + port);

		server.start();
	}
	
	public static void main(String[] args) {
		
		int port = 8881;
		
		if (args.length == 1) {
			try {
				port = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException e) {
				// Ignore
				e.printStackTrace();
			}
		}
		
		if (port >= 0 && port <= 65535) {
			Server server = new Server(port);
			server.run();
		}
		else {
			System.out.println("USAGE: java Server <port>");
		}
	}
	

}
